<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 23.02.2018
 * Time: 11:53
 */

namespace B2B\Classes\Constants;

/**
 * @SWG\Definition(
 *   definition="CoatingTypeInterface",
 *   type="string",
 *   enum={"iron","galvanic","powder"},
 *   default="powder"
 * )
 */
interface CoatingTypeInterface
{
  const POWDER = 'powder';
  const IRON = 'iron';
  const GALVANIC = 'galvanic';
}