<?php

namespace B2B\Models;

use B2B\Classes\Constants\WarehouseItemTypeInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * @SWG\Definition(
 *   definition="WarehouseItem",
 *   type="object"
 *  )
 */
class WarehouseItem extends Model
{
  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $nid;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelName;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $productId;

  /**
   * @SWG\Property()
   * @var string
   */
  public $sku;

  /**
   * @SWG\Property()
   * @var string
   */
  public $configurationName;

  /**
   * @SWG\Property(
   *   format="float"
   * )
   * @var number
   */
  public $quantity;

  /**
   * @SWG\Property(
   *   format="float"
   * )
   * @var number
   */
  public $price;

  /**
   * @SWG\Property(
   *   format="float"
   * )
   * @var number
   */
  public $priceRrp;

  /**
   * @SWG\Property()
   * @var boolean
   *
   */
  public $sale;

  /**
   * @SWG\Property()
   * @var Image
   */
  public $image;

  /**
   * @SWG\Property()
   * @var ProductConfiguration
   */
  public $config;

  /**
   * Set image
   *
   * @param Image $image
   */
  public function setImage(Image $image)
  {
    $this->image = $image;
  }

  /**
   * Get image
   *
   * @return Image
   */
  public function getImage(): Image
  {
    return $this->image;
  }

  /**
   * Set config
   *
   * @param ProductConfiguration $config
   */
  public function setConfig(ProductConfiguration $config)
  {
    $this->config = $config;
  }

  /**
   * Get config
   *
   * @return ProductConfiguration
   */
  public function getConfig(): ProductConfiguration
  {
    return $this->config;
  }

  /**
   * Get models list of warehouse items
   * @param string $type
   *   Warehouse item type
   * @param Request $request
   *
   * @return StockModel[]
   */
  public function getModelsList(string $type, Request $request): array
  {
    $table = $this->_getTableName($type, true);
    $statement = "SELECT * FROM ".$table." ORDER BY modelName ASC";

    return parent::getItems($statement, $request);
  }

  /**
   * Get warehouse items list from Drupal
   *
   * @param string $type
   *   Warehouse item type
   * @param Request $request
   *
   * @return WarehouseItem[]
   */
  public function getList(string $type, Request $request): array
  {
    $table = $this->_getTableName($type);
    $statement = "SELECT * FROM $table";

    return parent::getItems($statement, $request, self::class);
  }

  /**
   * Get warehouse item details
   *
   * @param int $nid
   *   Node identifier
   *
   * @return WarehouseItem
   */
  public function get(int $nid): WarehouseItem
  {
    $statement = "SELECT * FROM api_all_stock WHERE productId='$nid'";

    return parent::getItem($statement, self::class);
  }

  /**
   * Get table name for warehouse item type
   *
   * @param string $productType
   *   Product type
   * @param bool $models
   *   Grouped by models
   *
   * @return string
   */
  protected function _getTableName(string $productType, bool $models = false): string
  {
    switch ($productType) {
      case WarehouseItemTypeInterface::ALL:
        $table = "api_all_stock";
        break;
      case WarehouseItemTypeInterface::ACCESSORIES:
        $table = "api_accessories_stock";
        break;
      case WarehouseItemTypeInterface::HEATERS:
        $table = "api_heaters_stock";
        break;
      case WarehouseItemTypeInterface::RADIATORS:
        $table = "api_radiators_stock";
        break;
      default:
        $table = "api_all_stock";
    }

    return $table.($models ? '_models' : '');
  }

}
