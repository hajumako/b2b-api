<?php
/**
 * @file
 * Contains class AccessoryController code implementation.
 */

namespace B2B\Controllers;

use B2B\Models\Accessory;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class AccessoryController extends Controller
{
  protected $accessoryModel;

  public function __construct(Container $container, Accessory $accessoryModel)
  {
    $this->accessoryModel = $accessoryModel;
    parent::__construct($container);
  }

  /**
   * Get accessories list
   *
   * @param string $lang
   *   Language
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function listItems(string $lang, Request $request, Response $response): Response
  {
    $items = $this->accessoryModel->getList($lang, $request);
    $response
      ->getBody()
      ->write(json_encode($items, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
    $this->total = $this->accessoryModel->getTotal();

    return parent::addHeaders($lang, $request, $response);
  }

  /**
   * Get accessory details
   *
   * @param string $lang
   *   Language
   * @param int $nid
   *   Node id
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function itemDetails(string $lang, int $nid, Request $request, Response $response): Response
  {
    $item = $this->accessoryModel->get($nid, $lang);
    $response
      ->getBody()
      ->write(json_encode($item, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }
}
