<?php

namespace B2B\Models;

/** @SWG\Definition(
 *   definition="Token",
 *   type="object",
 *   required={"token"}
 *  )
 */
class Token
{
  /**
   * @SWG\Property()
   * @var string
   */
  public $token;
}
