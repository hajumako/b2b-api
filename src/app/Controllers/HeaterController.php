<?php
/**
 * @file
 * Contains class HeaterController code implementation.
 */

namespace B2B\Controllers;

use B2B\Models\Heater;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class HeaterController extends Controller
{
  protected $heaterModel;

  public function __construct(Container $container, Heater $heaterModel)
  {
    $this->heaterModel = $heaterModel;
    parent::__construct($container);
  }

  /**
   * Get heaters list
   *
   * @param string $lang
   *   Language
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function listItems(string $lang, Request $request, Response $response): Response
  {
    $items = $this->heaterModel->getList($lang, $request);
    $response
      ->getBody()
      ->write(json_encode($items, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
    $this->total = $this->heaterModel->getTotal();

    return parent::addHeaders($lang, $request, $response);
  }

  /**
   * Get heater details
   *
   * @param string $lang
   *   Language
   * @param int $nid
   *   Node id
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function itemDetails(string $lang, int $nid, Request $request, Response $response): Response
  {
    $item = $this->heaterModel->get($nid, $lang);
    $response
      ->getBody()
      ->write(json_encode($item, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }
}
