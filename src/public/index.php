<?php

namespace B2B;

/**
 * remove warnings from ArrayOfDTOutletContactInfo.php
 * ln 138
 * for reset() when DTOutletContactInfo == null
 */
error_reporting(E_ALL ^ E_WARNING);
mb_internal_encoding('UTF-8');

require '../../vendor/autoload.php';


use B2B\Classes\ErrorHandler;
use B2B\Classes\Exceptions\ForbiddenException;
use B2B\Classes\Exceptions\MethodNotAllowedException;
use B2B\Classes\Exceptions\UnauthorizedException;
use B2B\Classes\ResponseCode;
use B2B\Classes\SwaggerNotationGenerator;
use B2B\Controllers\TokenController;
use B2B\Models\ErrorModel;
use Closure;
use DI\Bridge\Slim\App;
use DI\Container;
use DI\ContainerBuilder;
use Monolog\Logger as MLogger;
use Monolog\Handler\FingersCrossedHandler;
use Monolog\Handler\StreamHandler;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Middleware\JwtAuthentication;
use Wsdl2PhpGenerator\Config;
use Wsdl2PhpGenerator\Generator;

$app = new class() extends App
{
  protected function configureContainer(ContainerBuilder $builder)
  {
    $builder->addDefinitions(__DIR__.'/../../env.php');
    $exceptionCatcher = function (Container $c) {
      return new ErrorHandler($c, new ErrorModel());
    };
    $definitions = [
      'logger' => function (ContainerInterface $c) {
        $logger = new MLogger('b2b');
        $debugConfig = $c->get('debug_config');
        $stream = new StreamHandler($debugConfig['log_path'], $debugConfig['debug_level']);
        $fingersCrossed = new FingersCrossedHandler($stream, $debugConfig['debug_level']);
        $logger->pushHandler($fingersCrossed);

        return $logger;
      },
      'phpErrorHandler' => $exceptionCatcher,
      'errorHandler' => $exceptionCatcher,
    ];
    $builder->addDefinitions($definitions);
  }
};

$container = $app->getContainer();

// Set Swagger annotation variables.
define("API_HOST", $container->get('api_host'));
define("API_SCHEMA", $container->get('api_schema'));
define("API_DOC_URL", $container->get('api_host').'/docs');

//Middleware

$app->add(
  new JwtAuthentication(
    [
      'secure' => false,
      'secret' => $container->get('token_settings')['salt'],
      'cookie' => $container->get('token_settings')['name'],
      'path' => '/',
      'passthrough' => array_merge($container->get('jwt_passthrough_urls'), $container->get('tested_urls')),
      'callback' => function (Request $request, Response $response, array $arguments) use ($container) {
        $container->set('decoded_token', $arguments['decoded']);
      },
      'error' => function (Request $request, Response $response, array $arguments) use ($container) {
        throw new UnauthorizedException('missing JWT token');
      },
    ]
  )
);

$app->add(
  function (Request $request, Response $response, Closure $next) use ($container) {
    $origin = $request->getHeader('origin');

    //block access from domains other than allowed
    if (!(
      ($container->get('debug_config')['debug_mode'] && $container->get('debug_config')['allow_all_origins']) ||
      (count($origin) && in_array($origin[0], $container->get('allowed_origins'))) ||
      (in_array($request->getUri()->getPath(), $container->get('jwt_passthrough_urls'))) ||
      (in_array($request->getUri()->getPath(), $container->get('tested_urls')))
    )) {
      //forbidden request - origin not allowed
      throw new ForbiddenException('origin not allowed');
    }

    //process request
    $response = $next($request, $response);

    //reissue token if expires in ['token_settings']['reissue'] time
    if ($container->has('decoded_token')) {
      $diff = $container->get('decoded_token')->exp - time();
      if ($diff < ($container->get('token_settings')['reissue']) && $diff > 0) {
        $tokenController = new TokenController($container);
        $response = $tokenController->issueTokenResponse(
          $container->get('decoded_token')->uid,
          $container->get('decoded_token')->nam,
          $request,
          $response
        );
      }
    }

    $response->getBody()->rewind();
    $body = $response->getBody()->getContents();

    if (
      ($container->get('debug_config')['debug_mode'] && $container->get('debug_config')['allow_all_origins']) ||
      (count($origin) && in_array($origin[0], $container->get('allowed_origins')))
    ) {
      //origin allowed
      if (
        !count($origin) &&
        $container->get('debug_config')['debug_mode'] &&
        $container->get('debug_config')['allow_all_origins']) {
        //origin empty but debug mode on
        $origin = ['*'];
      }

      if (in_array($request->getUri()->getPath(), ['/docs'])) {
        //requesting docs
        if (json_decode($body)) {

          //swaggering successful
          return $response
            ->withHeader('Content-Type', 'application/json')
            ->withHeader('Access-Control-Allow-Origin', $origin[0])
            ->withStatus(ResponseCode::OK);
        } else {

          //swaggering returned errors
          return $response
            ->withHeader('Content-Type', 'text/html;charset=UTF-8')
            ->withHeader('Access-Control-Allow-Origin', $origin[0])
            ->withStatus(ResponseCode::ERROR);
        }
      } else {
        if ($response->getStatusCode() === ResponseCode::METHOD_NOT_ALLOWED) {
          //method not allowed
          throw new MethodNotAllowedException('unknown endpoint: '.$request->getUri()->getPath());
        }

        return $response
          ->withHeader('Content-Type', json_decode($body) ? 'application/json' : 'text/html;charset=UTF-8')
          ->withHeader('Access-Control-Allow-Credentials', 'true')
          ->withHeader('Access-Control-Allow-Origin', $origin[0])
          ->withHeader(
            'Access-Control-Allow-Headers',
            'X-Requested-With, Content-Type, Accept, Origin, Authorization, Webp-Supported, Image-Type'
          )
          ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
          ->withHeader('Access-Control-Expose-Headers', 'X-CSRF-Token, X-Next, X-Total, X-Token');
      }
    } elseif (
      in_array($request->getUri()->getPath(), $container->get('jwt_passthrough_urls')) &&
      $request->getMethod() === 'GET'
    ) {
      //url allowed without jwt
      if (json_decode($body)) {

        //response has valid json
        return $response
          ->withHeader('Content-Type', 'application/json')
          ->withHeader('Access-Control-Allow-Origin', '*')
          ->withStatus(ResponseCode::OK);
      } else {

        //response is not json (i.e. php error with HTML)
        return $response
          ->withHeader('Content-Type', 'text/html;charset=UTF-8')
          ->withHeader('Access-Control-Allow-Origin', '*')
          ->withStatus(ResponseCode::OK);
      }
    } elseif (in_array($request->getUri()->getPath(), $container->get('tested_urls'))) {
      //url exposed for tests
      if ($response->getStatusCode() === ResponseCode::METHOD_NOT_ALLOWED) {
        //method not allowed
        throw new MethodNotAllowedException('unknown endpoint: '.$request->getUri()->getPath());
      } elseif (json_decode($body)) {

        //response has valid json
        return $response
          ->withHeader('Content-Type', 'application/json')
          ->withHeader('Access-Control-Allow-Origin', '*')
          ->withStatus(ResponseCode::OK);
      } else {

        //response is not json (i.e. php error with HTML)
        return $response
          ->withHeader('Access-Control-Allow-Origin', '*')
          ->withStatus(ResponseCode::OK);
      }
    } else {
      //forbidden request - origin not allowed
      throw new ForbiddenException('origin not allowed');
    }
  }
);

//Routes

$app->options(
  '/{routes:.+}',
  function ($request, $response) {
    //$response = $response->withHeader('X-CSRF-Token', $request->getAttribute('csrf_value'));
    return $response;
  }
);

$app->get(
  '/docs',
  function (Request $request, Response $response) {
    $swagger = \Swagger\scan(__DIR__.'/../');
    $response
      ->getBody()
      ->write($swagger);

    return $response;
  }
);

$app->get(
  '/wsdl',
  function (Request $request, Response $response) use ($container) {
    $axWebservices = $container->get('ax_webservices');
    $swaggeredClasses = array();
    foreach ($axWebservices as $name => $axWebservice) {
      $path = $container->get('root_dir').'/src/app/Wsdl/'.$name.'/';
      $swaggerGenerator = new SwaggerNotationGenerator($path, false, $swaggeredClasses);
      $swaggerGenerator->removeDirectory();
      $generator = new Generator();
      $generator->generate(
        new Config(
          array(
            'inputFile' => $axWebservice['host'],
            'outputDir' => $path,
            'namespaceName' => 'B2B\\Wsdl\\'.$name,
            'soapClientOptions' => array_merge(
              array(
                'login' => $axWebservice['user'],
                'password' => $axWebservice['pass'],
              ),
              $axWebservice['options']
            ),
          )
        )
      );
      $swaggerGenerator->generate();
      $response
        ->getBody()
        ->write($swaggerGenerator->getLog());
      $swaggeredClasses = $swaggerGenerator->getGenerated();
    }

    return $response
      ->withHeader('Content-Type', 'text/html;charset=UTF-8');
  }
);

/** @SWG\Post(
 *    path="/login",
 *    description="Login function",
 *    summary="Used to login into b2b service",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/credentials"),
 *    @SWG\Response(
 *      response=200,
 *      description="login response",
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/login', ['\B2B\Controllers\UserController', 'login']);

/** @SWG\Get(
 *    path="/logout",
 *    description="Logout function",
 *    summary="Used to logout from b2b service",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="login response",
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/logout', ['\B2B\Controllers\UserController', 'logout']);

include_once '../app/Routes/account.php';

include_once '../app/Routes/order.php';

include_once '../app/Routes/products.php';

include_once '../app/Routes/warehouse.php';

include_once '../app/Routes/payment.php';

include_once '../app/Routes/configurator.php';

include_once '../app/Routes/colors.php';

include_once '../app/Routes/parser.php';

include_once '../app/Routes/images.php';

/** @SWG\Post(
 *    path="/price-list/generate",
 *    description="Method to get price list PDF stream",
 *    summary="Method to get price list PDF stream",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="Price list response",
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/price-list/generate', ['\B2B\Controllers\PriceListController', 'generate']);

/** @SWG\Get(
 *    path="/countries/list",
 *    description="Get list of country regions.",
 *    summary="Get list of country regions.",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="countries response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/LogisticsAddressCountryRegionContract")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/countries/list', ['\B2B\Controllers\UserController', 'getCountries']);

$app->run();
