<?php

namespace B2B\Models;


use Psr\Http\Message\ServerRequestInterface as Request;

/** @SWG\Definition(
 *    definition="NewRadiator",
 *    type="object",
 *    required={"modelName"}
 *  )
 */
class Radiator extends Product
{
  const TABLE = "api_radiators";

  public $nid;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelName;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelNameLoc;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelAX;

  /**
   * @SWG\Property()
   * @var string
   */
  public $language;

  /**
   * @SWG\Property()
   * @var string
   */
  public $lead;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $created;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $changed;

  /**
   * @SWG\Property()
   * @var string
   */
  public $description;

  /**
   * @SWG\Property()
   * @var string
   */
  public $connector;

  /**
   * @SWG\Property()
   * @var string
   */
  public $customConnector;

  /**
   * @SWG\Property()
   * @var string
   */
  public $maxTemperature;

  /**
   * @SWG\Property()
   * @var string
   */
  public $pressure;

  /**
   * @SWG\Property()
   * @var string
   */
  public $warranty;

  /**
   * @SWG\Property(
   *   property="gallery"
   * )
   * @var string
   */

  /**
   * @SWG\Property()
   * @var string
   */
  public $adv;

  /**
   * @SWG\Property()
   * @var string
   */
  public $equipment;

  public function __set($name, $value)
  {
    if ($name === 'gallery') {
      //TODO $this->container->get('img_host') throws error - container is null when __set is being called
      $this->gallery = str_replace('public://', 'https://www.termaheat.pl/sites/default/files/', $value);
    }
  }

  /**
   * Get radiators list
   *
   * @param string $lang
   *   Language
   * @param Request $request
   *
   * @return Radiator[]
   */
  public function getList(string $lang, Request $request): array
  {
    $statement = "SELECT * FROM ".self::TABLE." WHERE language='$lang'";

    return parent::getItems($statement, $request, self::class);
  }

  /**
   * Get radiator details
   *
   * @param int $nid
   *   Node identifier
   * @param string $lang
   *   Language
   *
   * @return Radiator
   */
  public function get(int $nid, string $lang): Radiator
  {
    $statement = "CALL api_get_radiator_details($nid, '$lang')";

    return parent::getItem($statement, self::class);
  }
}

/**
 * @SWG\Definition(
 *   definition="Radiator",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(ref="#/definitions/NewRadiator"),
 *       @SWG\Schema(
 *           required={"nid"},
 *           @SWG\Property(property="nid", format="int64", type="integer")
 *       )
 *   }
 * )
 */
