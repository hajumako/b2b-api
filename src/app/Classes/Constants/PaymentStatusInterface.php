<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 30.08.2017
 * Time: 12:27
 */

namespace B2B\Classes\Constants;

/**
 * @SWG\Definition(
 *   definition="PaymentStatusInterface",
 *   type="string",
 *   enum={"started","pending","cancelled","success","error"},
 *   default="started"
 * )
 */
interface PaymentStatusInterface
{
  const STARTED = 'started';
  const PENDING = 'pending';
  const CANCELLED = 'cancelled';
  const SUCCESS = 'success';
  const ERROR = 'error';
}
