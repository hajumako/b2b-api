<?php

namespace B2B\Models;

use B2B\Wsdl\DtOutletServicesGroup\DtOutletCustomerRequest;
use B2B\Wsdl\DtOutletServicesGroup\DtOutletCustomerServiceFindByIdRequest;
use B2B\Wsdl\DtOutletServicesGroup\RoutingService;

/** @SWG\Definition(
 *   definition="User",
 *   type="object",
 *   required={"uid"}
 *  )
 */
class User extends Model
{
  const TABLE = "api_users";

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $uid;

  /**
   * @SWG\Property()
   * @var string
   */
  public $axId;

  /**
   * @SWG\Property()
   * @var string
   */
  public $name;

  /**
   * @SWG\Property()
   * @var string
   */
  public $mail;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $created;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $access;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $login;

  /**
   * @SWG\Property(
   *   format="int32"
   * )
   * @var integer
   */
  public $status;

  /**
   * Method to get user object by username.
   *
   * @param $username
   *   Username to check.
   *
   * @return User
   */
  public function getForAuth($username): User
  {
    $statement = "SELECT * FROM ".self::TABLE." WHERE (name = '$username' or mail = '$username') AND status = 1";

    return parent::getItem($statement, User::class);
  }

  /**
   * Method to get user object.
   *
   * @param int $uid
   *   User identifier.
   *
   * @return User
   */
  public function get(int $uid): User
  {
    $statement = "SELECT * FROM ".self::TABLE." WHERE uid = $uid";

    return parent::getItem($statement, User::class);
  }

  /**
   * Get AX user data
   *
   * @param string $axId
   *   AX user id
   * @param bool $allAddresses
   *   Flag to determine to return all user addresses.
   *
   * @return \B2B\Wsdl\DtOutletServicesGroup\DtOutletCustomerRequest
   */
  public function getAxUser(string $axId, bool $allAddresses = true): DtOutletCustomerRequest
  {
    //TODO move to constructor as DI
    //TODO problem with $fetch_class constructor in Model::getItem
    if (function_exists('xdebug_disable')) {
      xdebug_disable();
    };

    $routingService = new RoutingService();
    $serviceRequest = new DtOutletCustomerServiceFindByIdRequest($axId, $allAddresses);
    $response = $routingService->FindById($serviceRequest)->getResponse();

    if (function_exists('xdebug_enable')) {
      xdebug_enable();
    };

    return $response;
  }
}
