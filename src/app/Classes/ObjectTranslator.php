<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 06.09.2017
 * Time: 15:13
 */

namespace B2B\Classes;


use B2B\Classes\Constants\AddressTypeInterface;
use B2B\Wsdl\DtOutletServicesGroup\DTOutletAddress;
use B2B\Wsdl\DtOutletServicesGroup\DTOutletAddressRole;

class ObjectTranslator
{
  public static function translate($object, string $source, string $target)
  {
    $sourceName = (new \ReflectionClass($source))->getShortName();
    $targetName = (new \ReflectionClass($target))->getShortName();
    if ($sourceName === 'Address' && $targetName === 'DTOutletAddress') {
      //check if $object is object or array
      if (gettype($object) === 'object') {
        $addressIdAX = $object->addressIdAX;
        $isPrimaryAX = $object->isPrimaryAX;
        $addressDescription = $object->nameLine;
        $thoroughfare = $object->thoroughfare;
        $premise = $object->premise;
        $postalCode = $object->postalCode;
        $locality = $object->locality;
        $country = $object->country;
        $type = $object->type;
      } else {
        $addressIdAX = $object['addressIdAX'];
        $isPrimaryAX = $object['isPrimaryAX'];
        $addressDescription = $object['nameLine'];
        $thoroughfare = $object['thoroughfare'];
        $premise = $object['premise'];
        $postalCode = $object['postalCode'];
        $locality = $object['locality'];
        $country = $object['country'];
        $type = $object['type'];
      }
      $address = new DTOutletAddress();
      $address->setDirPartyLocationRecId($addressIdAX);
      $address->setPrimary($isPrimaryAX);
      $address->setAddressDescription($addressDescription);
      $address->setAddressStreet($thoroughfare);
      $address->setAddressStreetNo($premise);
      $address->setAddressZipCode($postalCode);
      $address->setAddressCity($locality);
      $address->setAddressCountryRegionId($country);
      $dttype = DTOutletAddressRole::Other;
      switch ($type) {
        case AddressTypeInterface::SHIPPING:
          $dttype = DTOutletAddressRole::Delivery;
          break;
        case AddressTypeInterface::BILLING:
          $dttype = DTOutletAddressRole::Invoice;
      }
      /** @noinspection PhpParamsInspection */
      $address->setAddressRole($dttype);
      return $address;
    } elseif ($sourceName === 'DTOutletAddress' && $targetName === 'Address') {
      //check if $object is object or array
      if (gettype($object) === 'object') {
        $addressIdAX = $object->getDirPartyLocationRecId();
        $isPrimaryAX = $object->getPrimary();
        $addressDescription = $object->getAddressDescription();
        $thoroughfare = $object->getAddressStreet();
        $premise = $object->getAddressStreetNo();
        $postalCode = $object->getAddressZipCode();
        $locality = $object->getAddressCity();
        $country = $object->getAddressCountryRegionId();
        $type = $object->getAddressRole();
      } else {
        $addressIdAX = $object['addressIdAX'];
        $isPrimaryAX = $object['isPrimaryAX'];
        $addressDescription = $object['addressDescription'];
        $thoroughfare = $object['addressStreet'];
        $premise = $object['addressStreetNo'];
        $postalCode = $object['addressZipCode'];
        $locality = $object['addressCity'];
        $country = $object['addressCountryRegionId'];
        $type = $object['addressRole'];
      }
      $address = new \stdClass();
      $address->addressIdAX = $addressIdAX;
      $address->isPrimaryAX = $isPrimaryAX;
      $address->nameLine = $addressDescription;
      $address->thoroughfare = $thoroughfare;
      $address->premise = $premise;
      $address->postalCode = $postalCode;
      $address->locality = $locality;
      $address->profileId = 0;
      $address->firstName = null;
      $address->lastName = null;
      $address->country = $country;
      $atype = AddressTypeInterface::SHIPPING;
      switch ($type) {
        case DTOutletAddressRole::Delivery:
          $atype = AddressTypeInterface::SHIPPING;
          break;
        case DTOutletAddressRole::Invoice:
          $atype = AddressTypeInterface::BILLING;
          break;
      }
      $address->type = $atype;
      return $address;
    }

    return null;
  }

}
