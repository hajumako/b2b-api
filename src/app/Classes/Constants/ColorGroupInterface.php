<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 23.02.2018
 * Time: 11:53
 */

namespace B2B\Classes\Constants;


interface ColorGroupInterface
{
  //TODO should be stored in database (mongo?)
  const GROUPS = [
    [
      'label' => 'Farba mokra',
      'decoration' => [
        'type' => DecoratorTypeInterface::GRADIENT,
        'value' => 'linear-gradient(180deg, rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.3) 17.87%, rgba(255,255,255,0.3) 49.91%, rgba(0,0,0,0.3) 79.96%, rgba(255,255,255,0.5) 100%)',
      ],
      'range' => [],
    ],
    [
      'label' => 'Pokrycie galwaniczne',
      'decoration' => [
        'type' => DecoratorTypeInterface::GRADIENT,
        'value' => 'linear-gradient(180deg, rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.3) 17.87%, rgba(255,255,255,0.3) 49.91%, rgba(0,0,0,0.3) 79.96%, rgba(255,255,255,0.5) 100%)',
      ],
      'range' => [],
    ],
    [
      'label' => 'Kolory specjalne',
      'decoration' => [
        'type' => DecoratorTypeInterface::GRADIENT,
        'value' => 'linear-gradient(180deg, rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.3) 17.87%, rgba(255,255,255,0.3) 49.91%, rgba(0,0,0,0.3) 79.96%, rgba(255,255,255,0.5) 100%)',
      ],
      'range' => [],
    ],
    [
      'label' => 'Nieskategoryzowane',
      'decoration' => [
        'type' => DecoratorTypeInterface::GRADIENT,
        'value' => 'linear-gradient(180deg, rgba(255,255,255,0.5) 0%, rgba(0,0,0,0.3) 17.87%, rgba(255,255,255,0.3) 49.91%, rgba(0,0,0,0.3) 79.96%, rgba(255,255,255,0.5) 100%)',
      ],
      'range' => [],
    ],
    [
      'label' => 'Odcienie żółtego',
      'decoration' => [
        'type' => DecoratorTypeInterface::COLOR,
        'value' => '#f8e71c',
      ],
      'range' => [
        100,
        200,
      ],
    ],
    [
      'label' => 'Odcienie czerwonego',
      'decoration' => [
        'type' => DecoratorTypeInterface::COLOR,
        'value' => '#d0021b',
      ],
      'range' => [
        201,
        410,
      ],
    ],
    [
      'label' => 'Odcienie niebieskiego',
      'decoration' => [
        'type' => DecoratorTypeInterface::COLOR,
        'value' => '#4a90e2',
      ],
      'range' => [
        500,
        524,
      ],
    ],
    [
      'label' => 'Odcienie zielonego',
      'decoration' => [
        'type' => DecoratorTypeInterface::COLOR,
        'value' => '#7ed321',
      ],
      'range' => [
        600,
        634,
      ],
    ],
    [
      'label' => 'Odcienie szarego',
      'decoration' => [
        'type' => DecoratorTypeInterface::COLOR,
        'value' => '#cbcbcb',
      ],
      'range' => [
        700,
        747,
      ],
    ],
    [
      'label' => 'Odcienie brązowego',
      'decoration' => [
        'type' => DecoratorTypeInterface::COLOR,
        'value' => '#b24600',
      ],
      'range' => [
        800,
        828,
      ],
    ],
    [
      'label' => 'Odcienie bieli i czerni',
      'decoration' => [
        'type' => DecoratorTypeInterface::GRADIENT,
        'value' => 'linear-gradient(to right, #ffffff 0%,#ffffff 40%,#4a4a4a 60%,#4a4a4a 100%)',
      ],
      'range' => [
        901,
        918,
      ],
    ],
  ];
}