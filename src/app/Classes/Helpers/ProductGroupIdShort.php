<?php

namespace B2B\Classes\Helpers;

/**
 * @SWG\Definition(
 *   definition="ProductGroupIdShort",
 *   type="string",
 *   enum={"WG","WL","WS","WW","WZ","WE","WR","TG"},
 *   default="WYRG"
 * )
 */
abstract class ProductGroupIdShort
{
  /**
   * AX constants - short names of product groups.
   */
  const AX_PRODUCT_GROUP_NORMAL_RADIATORS = 'WG';
  const AX_PRODUCT_GROUP_ELECTRIC_RADIATORS = 'WL';
  const AX_PRODUCT_GROUP_ELECTRIC_CORD_RADIATORS = 'WS';
  const AX_PRODUCT_GROUP_ELECTRIC_INTEGRATED_RADIATORS = 'WW';
  const AX_PRODUCT_GROUP_NORMAL_INTEGRATED_RADIATORS = 'WZ';
  const AX_PRODUCT_GROUP_IRON_RADIATORS = '??';
  const AX_PRODUCT_GROUP_HEATERS = 'WE';
  const AX_PRODUCT_GROUP_VALVES = 'WR';
  const AX_PRODUCT_GROUP_OTHERS = 'TG';

  /**
   * Get product short group id by its long name
   *
   * @param string $groupId
   *   long name of product group id (ProductGroupId)
   * @return string
   */
  public static function getGroupIdShort(string $groupId): string
  {
    switch ($groupId) {
      case ProductGroupId::AX_PRODUCT_GROUP_NORMAL_RADIATORS:
        return self::AX_PRODUCT_GROUP_NORMAL_RADIATORS;
      case ProductGroupId::AX_PRODUCT_GROUP_IRON_RADIATORS:
        return self::AX_PRODUCT_GROUP_IRON_RADIATORS;
      case ProductGroupId::AX_PRODUCT_GROUP_ELECTRIC_RADIATORS:
        return self::AX_PRODUCT_GROUP_ELECTRIC_RADIATORS;
      case ProductGroupId::AX_PRODUCT_GROUP_HEATERS:
        return self::AX_PRODUCT_GROUP_HEATERS;
      case ProductGroupId::AX_PRODUCT_GROUP_VALVES:
        return self::AX_PRODUCT_GROUP_VALVES;
      case ProductGroupId::AX_PRODUCT_GROUP_OTHERS:
        return self::AX_PRODUCT_GROUP_OTHERS;
      default:
        return self::AX_PRODUCT_GROUP_NORMAL_RADIATORS;
    }
  }
}
