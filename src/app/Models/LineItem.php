<?php

namespace B2B\Models;

use B2B\Classes\Traits\CastFromParent;

/**
 * @SWG\Definition(
 *     definition="LineItem",
 *     type="object"
 *  )
 */
class LineItem extends Model
{
  use CastFromParent;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelName;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $itemId;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $orderId;

  /**
   * @SWG\Property(
   *   property="sku"
   * )
   * @var string
   */

  /**
   * @SWG\Property()
   * @var number
   */
  public $quantity;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $created;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $changed;

  /**
   * @SWG\Property(
   *   format="int32"
   * )
   * @var integer
   */
  public $amount;

  /**
   * @SWG\Property()
   * @var string
   */
  public $currencyCode;

  /**
   * @SWG\Property()
   * @var string
   */
  public $data;

  /**
   * @SWG\Property(
   *   format="int32"
   * )
   * @var integer
   */
  public $deleted;


  private $itemIdAX;

  private $configIdAX;

  public function __set($name, $value)
  {
    if ($name === 'sku') {
      $this->sku = $value;
      $sku = explode('-', $value);
      if (count($sku) === 2) {
        $this->itemIdAX = $sku[0];
        $this->configIdAX = $sku[1];
      }
    }
  }

  /**
   * Get Item Id - model & size
   *
   * @return string
   */
  public function getItemIdAX(): string
  {
    return $this->itemIdAX;
  }

  /**
   * Get ConfigId - color + connection
   *
   * @return string
   */
  public function getConfigIdAX(): string
  {
    return $this->configIdAX;
  }

  /**
   * Get sku - itemId + configId
   *
   * @return string
   */
  public function getSku(): string
  {
    return $this->itemIdAX.'-'.$this->configIdAX;
  }

  /**
   *  Method to load attributes from parent class object.
   *
   * @param array $lineItem
   */
  public function loadFromArray(array $lineItem)
  {
    $this->castFromParent($lineItem);
  }


  /**
   * Function for object serialization (for JSON)
   *
   * @return $this
   */
  public function jsonSerialize()
  {
    return $this;
  }
}
