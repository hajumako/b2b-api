<?php

namespace B2B\Classes\Helpers\WSDL;


use B2B\Wsdl\B2BHelperTER\NoYesAll_TER;

/**
 * @SWG\Definition(
 *   definition="NoYesAll_Enum",
 *   type="object"
 * )
 */
class NoYesAll_TER_Wrapper
{
  /**
   * @SWG\Property(
   *   ref="#/definitions/NoYesAll_TER"
   * )
   * @var NoYesAll_TER
   */
  public $NoYesAll;
}
