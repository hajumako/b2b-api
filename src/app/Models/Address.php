<?php
/**
 * @file
 * Contains Address model code implementation.
 */

namespace B2B\Models;


use B2B\Classes\Constants\AddressTypeInterface;
use B2B\Classes\Helpers\MongoDb;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetLogisticsAddressCountryRegionsRequest;
use B2B\Wsdl\B2BHelperTER\RoutingService;

/** @SWG\Definition(
 *    definition="NewAddress",
 *    type="object",
 *    required={"type", "uid"}
 *  )
 */
class Address extends Model
{
  public $orderId;

  public $profileId;

  public $addressIdAX;

  public $isPrimaryAX;

  /**
   * @SWG\Property(
   *     ref="#/definitions/AddressTypeInterface"
   * )
   * @var AddressTypeInterface
   */
  public $type;

  /**
   * @SWG\Property(
   *     format="int64"
   * )
   * @var integer
   */
  public $uid;

  public $status;

  public $created;

  public $changed;

  /**
   * ISO3166 alpha-3 returned & stored in AX,
   * ISO3166 alpha-2 stored in DB
   * @SWG\Property()
   * @var string
   */
  public $country;

  /**
   * @SWG\Property()
   * @var string
   */
  public $locality;

  /**
   * @SWG\Property()
   * @var string
   */
  public $postalCode;

  /**
   * @SWG\Property()
   * @var string
   */
  public $thoroughfare;

  /**
   * @SWG\Property()
   * @var string
   */
  public $premise;

  /**
   * @SWG\Property()
   * @var string
   */
  public $organisationName;

  /**
   * @SWG\Property()
   * @var string
   */
  public $nameLine;

  /**
   * @SWG\Property()
   * @var string
   */
  public $firstName;

  /**
   * @SWG\Property()
   * @var string
   */
  public $lastName;

  /**
   * Method to get country regions list.
   *
   * @param string $lang
   *   Language.
   *
   * @return array
   */
  public function getCountryRegions(string $lang = 'pl')
  {
    $date = strtotime(date("Y-m-d h:i:s"));
    $document = $this->getCollectionDoc(MongoDb::COLLECTION_COUNTRY_REGIONS, ['expiration_date' => ['$gt' => $date]]);
    if (empty($document)) {
      $routingService = new RoutingService();
      $serviceRequest = new B2BHelper_TERGetLogisticsAddressCountryRegionsRequest($lang);
      $countries = $routingService->GetLogisticsAddressCountryRegions($serviceRequest)->getResponse()->jsonSerialize();
      // Save data to mongodb and set expiration date for 30 days.
      $this->setCollectionDoc(
        MongoDb::COLLECTION_COUNTRY_REGIONS,
        [
          'data' => $countries,
          'created' => $date,
          'expiration_date' => strtotime(
            date("Y-m-d h:i:s", $date)." +30 days"
          ),
        ]
      );
    } else {
      $countries = $document->bsonSerialize()->data;
    }

    return $countries;
  }

}

/**
 *  @SWG\Definition(
 *   definition="Address",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(ref="#/definitions/NewAddress"),
 *       @SWG\Schema(
 *           required={"orderId", "profileId", "addressIdAX"},
 *           @SWG\Property(property="orderId", format="int64", type="integer"),
 *           @SWG\Property(property="profileId", format="int64", type="integer"),
 *           @SWG\Property(property="addressIdAX", format="int64", type="integer"),
 *           @SWG\Property(property="isPrimaryAX", type="boolean"),
 *           @SWG\Property(property="status", format="int32", type="integer"),
 *           @SWG\Property(property="created", format="int64", type="integer"),
 *           @SWG\Property(property="changed", format="int64", type="integer")
 *       )
 *   }
 * )
 */
