<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 02.02.2018
 * Time: 09:53
 */

namespace B2B\Classes\Traits;


trait CastFromParent
{
  /**
   * Method to load attributes from parent class object.
   *
   * @param $obj
   *   Source object or array
   * @param bool $capitalizeKeys
   *   If object keys should be capitalized when object casted
   */
  protected function castFromParent($obj, bool $capitalizeKeys = false)
  {
    $objValues = get_object_vars($obj);
    $objValues = $objValues ?: $obj;
    foreach ($objValues AS $key => $value) {
      $key = $capitalizeKeys ? ucfirst($key) : $key;
      $this->$key = $value;
    }
  }
}