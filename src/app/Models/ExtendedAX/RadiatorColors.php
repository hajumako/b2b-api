<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 02.02.2018
 * Time: 09:36
 */

namespace B2B\Models\ExtendedAX;


use B2B\Classes\Traits\CastFromParent;
use B2B\Models\Color;
use B2B\Wsdl\B2BHelperTER\RadiatorColorsStruct_TER;
use stdClass;

/** @SWG\Definition(
 *  definition="RadiatorColors",
 *  type="object"
 * )
 */
class RadiatorColors extends RadiatorColorsStruct_TER
{
  use CastFromParent;

  /**
   * @SWG\Property()
   * @var string
   */
  public $hex;

  /**
   * @SWG\Property()
   * @var string
   */
  public $rgb;

  /**
   * @SWG\Property()
   * @var string
   */
  public $img;

  /**
   * @SWG\Property(
   *     format="int64"
   * )
   * @var integer
   */
  public $height;

  /**
   * @SWG\Property(
   *     format="int64"
   * )
   * @var integer
   */
  public $width;

  /**
   * @SWG\Property()
   * @var boolean
   */
  protected $configExists;

  protected $colorModel;

  /**
   * RadiatorColors constructor.
   *
   * @param Color $colorModel
   */
  public function __construct(Color $colorModel)
  {
    $this->colorModel = $colorModel;
  }

  /**
   * Set config exists
   *
   * @param bool $exists
   */
  public function setConfigExists(bool $exists)
  {
    $this->configExists = $exists;
  }

  /**
   * Get config exists
   *
   * @return bool
   */
  public function getConfigExists()
  {
    return $this->configExists;
  }


  /**
   *  Method to load attributes from parent class object.
   *
   * @param RadiatorColorsStruct_TER $axColor
   */
  public function loadFromParentObj(RadiatorColorsStruct_TER $axColor)
  {
    $this->castFromParent($axColor);
  }

  /**
   * @param $axColor
   *
   * @return $this
   */
  public function loadColorData($axColor)
  {
    $this->castFromParent($axColor, true);
    $i = $this->getColorId();
    $drupalColors = $this->colorModel->getList();
    if (array_key_exists($i, $drupalColors)) {
      $this->hex = $drupalColors[$i]->hex;
      $this->rgb = $drupalColors[$i]->rgb;
      $this->img = $drupalColors[$i]->img;
      $this->height = $drupalColors[$i]->height;
      $this->width = $drupalColors[$i]->width;
    }

    return $this;
  }
}