<?php

namespace B2B\Models;

use Psr\Http\Message\ServerRequestInterface as Request;

/** @SWG\Definition(
 *    definition="NewHeater",
 *    type="object",
 *    required={"modelName"}
 *  )
 */
class Heater extends Product
{
  const TABLE = "api_heaters";

  public $nid;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelName;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelNameLoc;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelAX;

  /**
   * @SWG\Property()
   * @var string
   */
  public $language;

  /**
   * @SWG\Property()
   * @var string
   */
  public $lead;

  /**
   * @SWG\Property(
   *     format="int64"
   * )
   * @var integer
   */
  public $created;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $changed;

  /**
   * @SWG\Property()
   * @var string
   */
  public $description;

  /**
   * @SWG\Property()
   * @var string
   */
  public $description2;

  /**
   * @SWG\Property(
   *   property="gallery"
   * )
   * @var string
   */

  /**
   * @SWG\Property()
   * @var string
   */
  public $heaterPower;

  /**
   * @SWG\Property()
   * @var string
   */
  public $heaterPowerClass;

  /**
   * @SWG\Property()
   * @var string
   */
  public $heaterPowers;

  /**
   * @SWG\Property()
   * @var string
   */
  public $connect;

  /**
   * @SWG\Property()
   * @var string
   */
  public $ip;

  public function __set($name, $value)
  {
    if ($name === 'gallery') {
      //TODO $this->container->get('img_host') throws error - container is null when __set is being called
      $this->gallery = str_replace('public://', 'https://www.termaheat.pl/sites/default/files/', $value);
    }
  }

  /**
   * Get heaters list
   *
   * @param $lang
   *   Language
   * @param Request $request
   *
   * @return Heater[]
   */
  public function getList(string $lang, Request $request): array
  {
    $statement = "SELECT * FROM " . self::TABLE . " WHERE language='$lang'";

    return parent::getItems($statement, $request, self::class);
  }

  /**
   * Get heater details
   *
   * @param int $nid
   *   Node identifier.
   * @param string $lang
   *   Language.
   *
   * @return Heater
   */
  public function get(int $nid, string $lang): Heater
  {
    $statement = "CALL api_get_heater_details($nid, '$lang')";

    return parent::getItem($statement, self::class);
  }

}

/**
 * @SWG\Definition(
 *   definition="Heater",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(ref="#/definitions/NewHeater"),
 *       @SWG\Schema(
 *           required={"nid"},
 *           @SWG\Property(property="nid", format="int64", type="integer")
 *       )
 *   }
 * )
 */
