<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 30.08.2017
 * Time: 12:23
 */

namespace B2B\Classes\Constants;

/**
 * @SWG\Definition(
 *   definition="AddressTypeInterface",
 *   type="string",
 *   enum={"billing","shipping"},
 *   default="shipping"
 * )
 */
interface AddressTypeInterface
{
  const BILLING = 'billing';
  const SHIPPING = 'shipping';
}


