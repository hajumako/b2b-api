<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 08.02.2018
 * Time: 14:06
 */

/** @SWG\Get(
 *    path="/warehouse/{lang}/models/{productType}",
 *    description="List all models in warehouse of type",
 *    summary="Get list of all radiator models in warehouse",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/productType"),
 *    @SWG\Parameter(ref="#/parameters/limit"),
 *    @SWG\Parameter(ref="#/parameters/offset"),
 *    @SWG\Response(
 *      response=200,
 *      description="stock model response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/StockModel")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/warehouse/{lang}/models/{productType}', ['\B2B\Controllers\WarehouseController', 'listModels']);

/** @SWG\Get(
 *    path="/warehouse/{lang}/ax/models/{groupId}",
 *    description="List all models in warehouse of type from AX",
 *    summary="Get list of all radiator models in warehouse from AX",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/groupId"),
 *    @SWG\Response(
 *      response=200,
 *      description="string array response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/ProductLabel")
 *      ),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/warehouse/{lang}/ax/models/{groupId}', ['\B2B\Controllers\WarehouseController', 'listAXModels']);

/** @SWG\Get(
 *    path="/warehouse/{lang}/list/{productType}",
 *    description="List all items in warehouse of type from Drupal",
 *    summary="Get list of all items in warehouse from Drupal",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/productType"),
 *    @SWG\Parameter(ref="#/parameters/filter"),
 *    @SWG\Parameter(ref="#/parameters/order"),
 *    @SWG\Parameter(ref="#/parameters/limit"),
 *    @SWG\Parameter(ref="#/parameters/offset"),
 *    @SWG\Response(
 *      response=200,
 *      description="warehouse response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/WarehouseItem")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/warehouse/{lang}/list/{productType}', ['\B2B\Controllers\WarehouseController', 'listItems']);

/** @SWG\Get(
 *    path="/warehouse/{lang}/item/{nid}",
 *    description="Get warehouse item details",
 *    summary="Get details of the heater with nid",
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/node_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="heater response",
 *      @SWG\Schema(ref="#/definitions/WarehouseItem")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/warehouse/{lang}/item/{nid}', ['\B2B\Controllers\WarehouseController', 'itemDetails']);

/** @SWG\Get(
 *    path="/warehouse/{lang}/ax/list/{groupId}",
 *    description="List all items in warehouse of type from AX",
 *    summary="Get list of all items in warehouse from AX",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/groupId"),
 *    @SWG\Parameter(ref="#/parameters/filter"),
 *    @SWG\Parameter(ref="#/parameters/limit"),
 *    @SWG\Parameter(ref="#/parameters/offset"),
 *    @SWG\Response(
 *      response=200,
 *      description="warehouse response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/InventAvailability")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/warehouse/{lang}/ax/list/{groupId}', ['\B2B\Controllers\WarehouseController', 'listAXItems']);