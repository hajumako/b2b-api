<?php
/**
 * @file
 * Contains class ConfiguratorColors code implementation.
 */

namespace B2B\Models;

use B2B\Models\ExtendedAX\RadiatorColors;

/** @SWG\Definition(
 *   definition="ConfiguratorColors",
 *   type="object"
 *  )
 */
class ConfiguratorColors
{

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/RadiatorColors")
   * )
   * @var RadiatorColors[]
   */
  public $iron;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/RadiatorColors")
   * )
   * @var RadiatorColors[]
   */
  public $galvanic;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/RadiatorColors")
   * )
   * @var RadiatorColors[]
   */
  public $powder;
}
