<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 23.02.2018
 * Time: 11:51
 */

namespace B2B\Models;


use B2B\Classes\Constants\DecoratorTypeInterface;

/** @SWG\Definition(
 *  definition="Decoration",
 *  type="object"
 * )
 */
class Decoration
{
  /**
   * @SWG\Property(
   *   ref="#/definitions/DecoratorTypeInterface"
   * )
   * @var DecoratorTypeInterface
   */
  public $type;

  /**
   * @SWG\Property()
   * @var string
   */
  public $value;

  public function __construct(string $type, string $value)
  {
    $this->type = $type;
    $this->value = $value;
  }
}