<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 09.10.2017
 * Time: 09:45
 */

namespace B2B\Classes\Constants;

/**
 * @SWG\Definition(
 *   definition="DeliveryTypeInterface",
 *   type="string",
 *   enum={"Kurier - paczka","Kurier - paczka za pobraniem","Kurier - paleta  (Schenker, GLS lub Hellmann)","Odbiór osobisty","Odbiór osobisty (po przedpłacie)"},
 *   default="Kurier - paczka"
 * )
 */
interface DeliveryTypeInterface
{
  const COURIER_PAID = 'Kurier - paczka';
  const COURIER_UNPAID = 'Kurier - paczka za pobraniem';
  const COURIER_PALLET_PAID = 'Kurier - paleta  (Schenker, GLS lub Hellmann)';
  const PERSONAL_PAID = 'Odbiór osobisty';
  const PERSONAL_UNPAID = 'Odbiór osobisty (po przedpłacie)';
}
