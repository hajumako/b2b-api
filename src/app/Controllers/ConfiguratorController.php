<?php
/**
 * @file
 * Contains class ConfiguratorController code implementation.
 */

namespace B2B\Controllers;


use B2B\Classes\Constants\CoatingTypeInterface;
use B2B\Classes\Constants\ColorGroupInterface;
use B2B\Classes\Exceptions\ApiException;
use B2B\Classes\Helpers\ProductGroupId;
use B2B\Classes\Helpers\ProductGroupIdShort;
use B2B\Classes\ProductCodeParser;
use B2B\Models\Cache;
use B2B\Models\Color;
use B2B\Models\ColorGroup;
use B2B\Models\ConfiguratorColors;
use B2B\Models\ConfigValidator;
use B2B\Models\Decoration;
use B2B\Models\ExtendedAX\HeaterConfigs;
use B2B\Models\ExtendedAX\HeaterModel;
use B2B\Models\ExtendedAX\RadiatorColors;
use B2B\Models\ExtendedAX\RadiatorConnections;
use B2B\Models\ExtendedAX\RadiatorModel;
use B2B\Models\ExtendedAX\RadiatorSize;
use B2B\Models\Image;
use B2B\Models\Model;
use B2B\Models\ProductConfiguration;
use B2B\Models\User;
use B2B\Wsdl\B2BHelperTER\ArrayOfDTItemInfoReference;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetConfigurationStateRequest;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetPossibleRadiatorCoatingsRequest;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetPossibleRadiatorConnectionsRequest;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetPossibleRadiatorHeatersConfigurationRequest;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetPossibleRadiatorHeatersItemIdRequest;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetRadiatorColorsByItemIdRequest;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetRadiatorModelsByProdGroupRequest;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetRadiatorsByModelRequest;
use B2B\Wsdl\B2BHelperTER\DTItemInfoReference;
use B2B\Wsdl\B2BHelperTER\NoYes;
use B2B\Wsdl\B2BHelperTER\NoYesAll_TER;
use B2B\Wsdl\B2BHelperTER\RadiatorModelB2BStruct_TER;
use B2B\Wsdl\B2BHelperTER\RoutingService as B2BRoutingService;
use B2B\Wsdl\PriceService\ArrayOfPriceCustomerRequest_TER;
use B2B\Wsdl\PriceService\PriceCustomerRequest_TER;
use B2B\Wsdl\PriceService\PriceService_TERGetCustomerPricesRequest;
use B2B\Wsdl\PriceService\RoutingService as PriceRoutingService;
use DI\Container;
use MongoException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ConfiguratorController extends Controller
{
  protected $B2BroutingService;
  protected $priceRoutingService;
  protected $tokenController;
  protected $userModel;
  protected $colorModel;
  protected $parser;
  protected $configValidator;
  protected $imageModel;
  protected $productConfiguration;
  protected $cache;

  protected $model; //TODO out

  const VIEWS = [
    'CAM0',
    'CAM1',
    'CAM2',
    'CAM3',
    'CAM4',
    'CAM5',
  ];

  /**
   * ConfiguratorController constructor.
   *
   * @param Container $container
   * @param B2BRoutingService $B2BroutingService
   * @param PriceRoutingService $priceRoutingService
   * @param TokenController $tokenController
   * @param User $userModel
   * @param Color $colorModel
   * @param ProductCodeParser $parser
   * @param ConfigValidator $configValidator
   * @param Image $imageModel
   * @param Model $model
   */
  public function __construct(
    Container $container,
    B2BRoutingService $B2BroutingService,
    PriceRoutingService $priceRoutingService,
    TokenController $tokenController,
    User $userModel,
    Color $colorModel,
    ProductCodeParser $parser,
    ConfigValidator $configValidator,
    ProductConfiguration $productConfiguration,
    Image $imageModel,
    Model $model,
    Cache $cache
  ) {
    $this->B2BroutingService = $B2BroutingService;
    $this->priceRoutingService = $priceRoutingService;
    $this->tokenController = $tokenController;
    $this->userModel = $userModel;
    $this->colorModel = $colorModel;
    $this->parser = $parser;
    $this->configValidator = $configValidator;
    $this->imageModel = $imageModel;
    $this->productConfiguration = $productConfiguration;
    $this->model = $model;
    parent::__construct($container);
  }

  /**
   * Get list of models grouped by type (WG + WL)
   *
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getModelsGrouped(Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $axId = $this->userModel->get($uid)->axId;
    }
    $params = new B2BHelper_TERGetRadiatorModelsByProdGroupRequest(
      ProductGroupId::AX_PRODUCT_GROUP_NORMAL_RADIATORS,
      NoYesAll_TER::Yes,
      'pl',
      $axId ?: ''
    );
    $WYRG = $this->B2BroutingService->GetRadiatorModelsByProdGroup($params)->getResponse();
    $params = new B2BHelper_TERGetRadiatorModelsByProdGroupRequest(
      ProductGroupId::AX_PRODUCT_GROUP_ELECTRIC_RADIATORS,
      NoYesAll_TER::Yes,
      'pl',
      $axId ?: ''
    );
    $WYRL = $this->B2BroutingService->GetRadiatorModelsByProdGroup($params)->getResponse();
    $WYRGmodels = [];
    foreach ($WYRG as $_WYRG) {
      $model = $this->prepareModel($_WYRG, $request);
      $model->setConfigExists(true);
      $WYRGmodels[substr($_WYRG->getRadiatorModel(), 2, 3)] = [
        lcfirst(substr($_WYRG->getRadiatorModel(), 0, 2)) => $model->jsonSerialize(),
      ];
    }
    $WYRLmodels = [];
    foreach ($WYRL as $_WYRL) {
      $model = $this->prepareModel($_WYRL, $request);
      $model->setConfigExists(true);
      $WYRLmodels[substr($_WYRL->getRadiatorModel(), 2, 3)] = [
        lcfirst(substr($_WYRL->getRadiatorModel(), 0, 2)) => $model->jsonSerialize(),
      ];
    }
    $mergedModels = array_merge_recursive($WYRGmodels, $WYRLmodels);
    ksort($mergedModels);
    $response
      ->getBody()
      ->write(json_encode(array_values($mergedModels), $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get list of products for product group.
   *
   * @param string $groupId
   *   Product group.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getModels(string $groupId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $axId = $this->userModel->get($uid)->axId;
    }
    //TODO move language to method parameters
    $params = new B2BHelper_TERGetRadiatorModelsByProdGroupRequest(
      $groupId,
      NoYesAll_TER::Yes,
      'pl',
      $axId ?: ''
    );
    $axModels = $this->B2BroutingService->GetRadiatorModelsByProdGroup($params)->getResponse();

    $models = [];
    $validModels = $this->configValidator->getValidModels(ProductGroupIdShort::getGroupIdShort($groupId));
    foreach ($axModels as $axModel) {
      $model = $this->prepareModel($axModel, $request);
      $model->setConfigExists(in_array($axModel->getRadiatorModel(), $validModels));
      $models[] = $model->jsonSerialize();
    }
    usort(
      $models,
      function ($a, $b) {
        return strnatcmp($a['labelName'], $b['labelName']);
      }
    );
    $response
      ->getBody()
      ->write(json_encode($models, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get list of all available sizes for specific model.
   *
   * @param string $model
   *   Model name.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getSizes(string $model, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $axId = $this->userModel->get($uid)->axId;
    }
    //TODO move language to method parameters
    $params = new B2BHelper_TERGetRadiatorsByModelRequest($model, NoYesAll_TER::Yes, 'pl', $axId ?: '');
    $axSizes = $this->B2BroutingService->GetRadiatorsByModel($params)->getResponse();

    $sizes = [];
    $validSizes = $this->configValidator->getValidSizes($model);
    foreach ($axSizes->getRadiatorB2BStruct_TER() as $axSize) {
      $size = new RadiatorSize();
      $nameArr = explode(' ', $axSize->getName());
      $s = str_replace('/', '_', $nameArr[count($nameArr) - 1]);
      $images = [];
      foreach (self::VIEWS as $view) {
        $this->imageModel->setProperties($axSize->getLabelName(), $request->getHeaders(), null, $view, $s);
        $this->imageModel->prepareDefaultUrl();
        $this->imageModel->prepareSrcset();
        $images[] = clone $this->imageModel;
      }
      $size->setImages($images);
      $this->imageModel->setProperties($axSize->getLabelName(), $request->getHeaders(), null, null, $s);
      $this->imageModel->prepareDefaultUrl();
      $this->imageModel->prepareSrcset();
      $size->setImage(clone $this->imageModel);
      $size->setConfigExists(in_array($axSize->getItemId(), $validSizes));
      $size->loadFromParentObj($axSize);
      $sizes[] = $size->jsonSerialize();
    }

    $response
      ->getBody()
      ->write(json_encode($sizes, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get list of all available connectors for specific product.
   *
   * @param string $itemId
   *   Product identifier.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getConnections(string $itemId, Request $request, Response $response): Response
  {
    $params = new B2BHelper_TERGetPossibleRadiatorConnectionsRequest($itemId);
    $axConnections = $this->B2BroutingService->GetPossibleRadiatorConnections($params)->getResponse(
    )->getRadiatorConnectionsStruct_TER();

    $connections = [];
    $validConnections = $this->configValidator->getValidConnections($itemId);
    foreach ($axConnections as $axConnection) {
      $connection = new RadiatorConnections();
      $connection->setConfigExists(in_array($axConnection->getConnectionId(), $validConnections));
      $connection->loadFromParentObj($axConnection);
      $connections[] = $connection->jsonSerialize();
    }
    $response
      ->getBody()
      ->write(json_encode($connections, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get list of available coatings for specific product.
   * Returns list of coating types (paint/galvanic/iron)
   *
   * @param string $itemId
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getCoatings(string $itemId, Request $request, Response $response): Response
  {
    $params = new B2BHelper_TERGetPossibleRadiatorCoatingsRequest($itemId);
    $axCoatings = $this->B2BroutingService->GetPossibleRadiatorCoatings($params)->getResponse();
    $response
      ->getBody()
      ->write(json_encode($axCoatings->jsonSerialize(), $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get list of colors for specific product.
   * Returns full list of available colors (not coating types)
   *
   * @param string $itemId
   *   Product identifier.
   * @param string $connectionId
   *   Product connection
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getColorsByItemId(
    string $itemId,
    string $connectionId,
    Request $request,
    Response $response
  ): Response {
    $params = new B2BHelper_TERGetRadiatorColorsByItemIdRequest($itemId);
    $axColors = $this->B2BroutingService->GetRadiatorColorsByItemId($params)->getResponse();
    $colors = new ConfiguratorColors();

    $_colors = $this->colorModel->getList();
    $validColors = $this->configValidator->getValidColors($itemId, $connectionId);
    foreach ($axColors as $axColor) {
      $c = new RadiatorColors($this->colorModel);
      $c->setConfigExists(in_array($axColor->getColorId(), $validColors));
      $c->loadFromParentObj($axColor);
      $c->loadColorData($_colors);
      if ($c->getIsCastIron() === NoYes::Yes) {
        $colors->iron[] = $c->jsonSerialize();
      } elseif ($c->getIsGalvanic() === NoYes::Yes) {
        $colors->galvanic[] = $c->jsonSerialize();
      } else {
        $colors->powder[] = $c->jsonSerialize();
      }
    }

    $response
      ->getBody()
      ->write(json_encode($colors, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * @param string $itemId
   * @param string $configId
   * @param Request $request
   * @param Response $response
   * @return Response
   */
  public function getRadiatorHeaters(string $itemId, string $configId, Request $request, Response $response): Response
  {
    $params = new B2BHelper_TERGetPossibleRadiatorHeatersItemIdRequest($itemId, $configId);
    $axHeaters = $this->B2BroutingService->GetPossibleRadiatorHeatersItemId($params)->getResponse();

    $heaters = [];
    foreach ($axHeaters as $axHeater) {
      $heater = new HeaterModel();
      $matches = ProductCodeParser::extractHeaterName($axHeater->getParmName());
      $labelName = ProductCodeParser::normalizeHeaterName($matches[2] ?: $matches[4]);
      $heater->setLabelName(str_replace('_', ' ', $labelName));
      $heater->setDisplayedName(ProductCodeParser::createHeaterDisplayedName($matches[1], $matches[2] ?: $matches[4]));
      $heater->setPower($matches[5] ?: '');
      $this->imageModel->setProperties($heater->getLabelName(), $request->getHeaders(), null, 'CAM4');
      $this->imageModel->prepareDefaultUrl();
      $this->imageModel->prepareSrcset();
      $heater->setImage(clone $this->imageModel);
      $heater->loadFromParentObj($axHeater);
      $heaters[] = $heater->jsonSerialize();
      //$heaters[] = $axHeater->getParmName();
    }

    $response
      ->getBody()
      ->write(json_encode($heaters, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  public function getRadiatorHeatersConfigs(
    string $itemId,
    string $configId,
    string $heaterItemId,
    Request $request,
    Response $response
  ): Response {
    $params = new B2BHelper_TERGetPossibleRadiatorHeatersConfigurationRequest($itemId, $configId, $heaterItemId);
    $axHeaterConfigs = $this->B2BroutingService->GetPossibleRadiatorHeatersConfiguration($params)->getResponse();


    $heaterConfigs = new HeaterConfigs();
    foreach ($axHeaterConfigs as $axHeaterConfig) {
      $pkg = substr($axHeaterConfig->getConfigId(), 0, 1);
      if (!in_array($pkg, $heaterConfigs->packages)) {
        $heaterConfigs->packages[] = $pkg;
      }
      $col = substr($axHeaterConfig->getConfigId(), 1, 3);
      if (!in_array($col, $heaterConfigs->colors)) {
        $heaterConfigs->colors[] = $col;
      }
      $cab = substr($axHeaterConfig->getConfigId(), 4, 1);
      if (!in_array($cab, $heaterConfigs->cables)) {
        $heaterConfigs->cables[] = $cab;
      }
      $this->imageModel->setSKU($heaterItemId . '-F' . $col . 'W', $request->getHeaders(), null, 'CAMX');
      $this->imageModel->prepareDefaultUrl();
      $this->imageModel->prepareSrcset();
      $heaterConfigs->setImage($col, clone $this->imageModel);
    }

    $response
      ->getBody()
      ->write(json_encode($heaterConfigs, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Check if model exists.
   *
   * @param string $groupId
   *   Product group.
   * @param string $model
   *   Product model.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function checkModel(string $model, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $axId = $this->userModel->get($uid)->axId;
    }
    //TODO move language to method parameters
    $groupId = ProductGroupId::getGroupId(substr($model, 0, 2));
    $params = new B2BHelper_TERGetRadiatorModelsByProdGroupRequest(
      $groupId,
      NoYesAll_TER::Yes,
      'pl',
      $axId ?: ''
    );
    $axModels = $this->B2BroutingService->GetRadiatorModelsByProdGroup($params)->getResponse();

    $models = [];
    $validModels = $this->configValidator->getValidModels(ProductGroupIdShort::getGroupIdShort($groupId));
    foreach ($axModels as $axModel) {
      if ($axModel->getRadiatorModel() === $model) {
        $model = new RadiatorModel();
        $this->imageModel->setProperties($axModel->getLabelName(), $request->getHeaders(), null, null);
        $this->imageModel->prepareDefaultUrl();
        $this->imageModel->prepareSrcset();
        $model->setImage(clone $this->imageModel);
        $model->setConfigExists(in_array($axModel->getRadiatorModel(), $validModels));
        $model->loadFromParentObj($axModel);
        $model->setLabelName(ucwords(mb_strtolower($model->getLabelName())));
        $models[] = $model->jsonSerialize();
      }
    }
    $response
      ->getBody()
      ->write(json_encode($models, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Check if size exists for given model.
   * If item id exists.
   *
   * @param string $itemId
   *   Item id.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function checkSize(string $itemId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $axId = $this->userModel->get($uid)->axId;
    }
    //TODO move language to method parameters
    $model = substr($itemId, 0, 5);
    $params = new B2BHelper_TERGetRadiatorsByModelRequest($model, NoYesAll_TER::Yes, 'pl', $axId ?: '');
    $axSizes = $this->B2BroutingService->GetRadiatorsByModel($params)->getResponse();

    $sizes = [];
    $validSizes = $this->configValidator->getValidSizes($model);
    foreach ($axSizes->getRadiatorB2BStruct_TER() as $axSize) {
      if ($axSize->getItemId() === $itemId) {
        $size = new RadiatorSize();
        $nameArr = explode(' ', $axSize->getName());
        $s = str_replace('/', '_', $nameArr[count($nameArr) - 1]);
        $images = [];
        foreach (self::VIEWS as $view) {
          $this->imageModel->setProperties($axSize->getLabelName(), $request->getHeaders(), null, $view, $s);
          $this->imageModel->prepareDefaultUrl();
          $this->imageModel->prepareSrcset();
          $images[] = clone $this->imageModel;
        }
        $size->setImages($images);
        $this->imageModel->setProperties($axSize->getLabelName(), $request->getHeaders(), null, null, $s);
        $this->imageModel->prepareDefaultUrl();
        $this->imageModel->prepareSrcset();
        $size->setImage(clone $this->imageModel);
        $size->setConfigExists(in_array($axSize->getItemId(), $validSizes));
        $size->loadFromParentObj($axSize);
        $sizes[] = $size->jsonSerialize();
      }
    }

    $response
      ->getBody()
      ->write(json_encode($sizes, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Check if connection is possible for given product.
   *
   * @param string $itemId
   *   Product identifier.
   * @param string $connectionId
   *   Product connector.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function checkConnection(string $itemId, string $connectionId, Request $request, Response $response): Response
  {
    $params = new B2BHelper_TERGetPossibleRadiatorConnectionsRequest($itemId);
    $axConnections = $this->B2BroutingService->GetPossibleRadiatorConnections($params)->getResponse(
    )->getRadiatorConnectionsStruct_TER();

    $connections = [];
    $validConnections = $this->configValidator->getValidConnections($itemId);
    foreach ($axConnections as $axConnection) {
      if ($axConnection->getConnectionId() === $connectionId) {
        $connection = new RadiatorConnections();
        $connection->setConfigExists(in_array($axConnection->getConnectionId(), $validConnections));
        $connection->loadFromParentObj($axConnection);
        $connections[] = $connection->jsonSerialize();
      }
    }
    $response
      ->getBody()
      ->write(json_encode($connections, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Check if color is possible for given product.
   *
   * @param string $itemId
   *   Product identifier.
   * @param string $colorId
   *   Color id.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function checkColor(
    string $itemId,
    string $colorId,
    Request $request,
    Response $response
  ): Response {
    $params = new B2BHelper_TERGetRadiatorColorsByItemIdRequest($itemId);
    $axColors = $this->B2BroutingService->GetRadiatorColorsByItemId($params)->getResponse();

    $colors = [];
    $_colors = $this->colorModel->getList();
    foreach ($axColors as $axColor) {
      if ($axColor->getColorId() === $colorId) {
        $color = new RadiatorColors($this->colorModel);
        $color->setConfigExists(true);
        $color->loadFromParentObj($axColor);
        $color->loadColorData($_colors);
        $colors[] = $color->jsonSerialize();
      }
    }

    $response
      ->getBody()
      ->write(json_encode($colors, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get list (array) of color ids available for specified item id & connection id
   *
   * @param string $itemId
   *   Product identifier.
   * @param string $connectionId
   *   Connection identifier.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getColorsByItemIdShort(
    string $itemId,
    string $connectionId,
    Request $request,
    Response $response
  ): Response {
    $params = new B2BHelper_TERGetRadiatorColorsByItemIdRequest($itemId);
    $axColors = $this->B2BroutingService->GetRadiatorColorsByItemId($params)->getResponse();

    $existing = [];
    $validColors = $this->configValidator->getValidColors($itemId, $connectionId);
    foreach ($axColors as $axColor) {
      if (in_array($axColor->getColorId(), $validColors)) {
        $existing[] = $axColor->getColorId();
      }
    }

    $response
      ->getBody()
      ->write(json_encode($existing, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get available colors for specified item id & connection id divided in color groups
   * (tone & type division)
   *
   * @param string $itemId
   *   Product identifier.
   * @param string $connectionId
   *   Connection identifier.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getColorsByItemIdInGroups(
    string $itemId,
    string $connectionId,
    Request $request,
    Response $response
  ): Response {
    $params = new B2BHelper_TERGetRadiatorColorsByItemIdRequest($itemId);
    $axColors = $this->B2BroutingService->GetRadiatorColorsByItemId($params)->getResponse();

    $colorGroups = [];
    foreach (ColorGroupInterface::GROUPS as $group) {
      $colorGroups[] = new ColorGroup(
        $group['label'],
        new Decoration($group['decoration']['type'], $group['decoration']['value']),
        $group['range']
      );
    }

    $_colors = $this->colorModel->getList();
    $validColors = $this->configValidator->getValidColors($itemId, $connectionId);
    foreach ($axColors as $axColor) {
      $c = new RadiatorColors($this->colorModel);
      $c->setConfigExists(in_array($axColor->getColorId(), $validColors));
      $c->loadFromParentObj($axColor);
      $c->loadColorData($_colors);

      if ($c->getIsCastIron() === NoYes::Yes) {
        $colorGroups[0]->type = CoatingTypeInterface::IRON;
        $colorGroups[0]->colors[] = $c->jsonSerialize();
      } elseif ($c->getIsGalvanic() === NoYes::Yes) {
        $colorGroups[1]->type = CoatingTypeInterface::GALVANIC;
        $colorGroups[1]->colors[] = $c->jsonSerialize();
      } else {
        $colorId = intval($c->getColorId());
        if ($colorId) {
          foreach ($colorGroups as $i => &$colorGroup) {
            if ($i > 3) {
              if ($colorId >= $colorGroup->range[0] && $colorId <= $colorGroup->range[1]) {
                $colorGroup->type = CoatingTypeInterface::POWDER;
                $colorGroup->colors[] = $c->jsonSerialize();
                $found = true;
                break;
              }
            }
          }
          if (empty($found)) {
            $colorGroups[3]->type = CoatingTypeInterface::POWDER;
            $colorGroups[3]->colors[] = $c->jsonSerialize();
          }
        } else {
          $colorGroups[2]->type = CoatingTypeInterface::POWDER;
          $colorGroups[2]->colors[] = $c->jsonSerialize();
        }
      }
    }

    $response
      ->getBody()
      ->write(json_encode($colorGroups, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get product's configuration info by product code.
   *
   * @param string $sku
   *   Product code.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getConfigBySku(string $sku, Request $request, Response $response): Response
  {
    $productConfiguration = $this->productConfiguration->getConfig($sku);
    $DTItemInfoReference = new DTItemInfoReference();
    $DTItemInfoReference->setProductId($productConfiguration->itemId);
    $DTItemInfoReference->setVariant($productConfiguration->configId);
    $DTItemInfoReferenceArr[] = $DTItemInfoReference;
    $arrayOfDTItemInfoReference = new ArrayOfDTItemInfoReference();
    $arrayOfDTItemInfoReference->setDTItemInfoReference($DTItemInfoReferenceArr);
    $params = new B2BHelper_TERGetConfigurationStateRequest($arrayOfDTItemInfoReference);
    $axResponse = $this->B2BroutingService->GetConfigurationState($params)->getResponse();
    $productConfiguration->configurationState = $axResponse->getInventConfigurationStateStruct_TER(
    )[0]->getConfigurationState();
    $response
      ->getBody()
      ->write(json_encode($productConfiguration, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get product's configuration info by itemId and configId.
   *
   * @param string $itemId
   *   Product identifier.
   * @param string $configId
   *   Configuration code.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getConfigByParts(string $itemId, string $configId, Request $request, Response $response): Response
  {
    return $this->getConfigBySku($itemId . '-' . $configId, $request, $response);
  }

  /**
   * Get products' configurations for given product codes.
   *
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   * @throws ApiException
   */
  public function getConfigs(Request $request, Response $response): Response
  {
    $productCodes = $request->getParsedBody();
    if ($productCodes != null && is_array($productCodes)) {
      $productConfigurations = [];
      foreach ($productCodes as $productCode) {
        $productConfigurations[] = json_decode(
          json_encode($this->productConfiguration->getConfig(strtoupper($productCode)))
        );
      }
      $response
        ->getBody()
        ->write(json_encode($productConfigurations, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new ApiException('unable to parse passed data');
  }

  /**
   * Save product's configuration info in db for given product code.
   *
   * @param string $sku
   *   Product code.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   * @throws ApiException
   * @throws MongoException
   */
  public function setConfig(string $sku, Request $request, Response $response): Response
  {
    $configData = $request->getParsedBody();
    if ($configData != null) {
      $configData = $this->productConfiguration->setConfig($sku, $configData);
      $response
        ->getBody()
        ->write(json_encode($configData, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new ApiException('unable to parse passed data');
  }

  /**
   * Get product's price for given product code.
   *
   * @param string $sku
   *   Product code.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   * @throws ApiException
   */
  public function getPriceBySku(string $sku, Request $request, Response $response): Response
  {
    if ($sku != null) {
      $skuArr = explode("-", $sku);
      if (count($skuArr) === 2) {
        return $this->getPriceByParts($skuArr[0], $skuArr[1], $request, $response);
      }
      throw new ApiException('wrong $sku format in ' . __METHOD__);
    }
    throw new ApiException('missing $sku in ' . __METHOD__);
  }

  /**
   * Get product's price for given itemId & configId.
   *
   * @param string $itemId
   *   Product identifier.
   * @param string $configId
   *   Configuration code.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   * @throws ApiException
   */
  public function getPriceByParts(string $itemId, string $configId, Request $request, Response $response): Response
  {
    if ($itemId != null && $configId != null) {
      $axId = $this->userModel->get($this->tokenController->getUserId())->axId;
      if ($axId != null) {
        $priceCustomerRequestTER = new PriceCustomerRequest_TER();
        $priceCustomerRequestTER->setCustAccount($axId);
        $priceCustomerRequestTER->setItemId($itemId);
        $priceCustomerRequestTER->setConfigId($configId);
        $priceCustomerRequestTER->setCalculateSalesPrice(true);
        //TODO for now currency is fixed
        $priceCustomerRequestTER->setCurrency('PLN');
        $arrayOfPriceCustomerRequestTER = new ArrayOfPriceCustomerRequest_TER();
        $arrayOfPriceCustomerRequestTER->setPriceCustomerRequest_TER([$priceCustomerRequestTER]);
        $params = new PriceService_TERGetCustomerPricesRequest($arrayOfPriceCustomerRequestTER);
        $axResponse = $this->priceRoutingService->GetCustomerPrices($params)->getResponse(
        )->getPriceCustomerResponse_TER();
        foreach ($axResponse as $axPrice) {
          $axPrice->setSalesPrice(round($axPrice->getSalesPrice() * 100));
        }
        $response
          ->getBody()
          ->write(json_encode($axResponse[0]->jsonSerialize(), $this->isDebugMode ? JSON_PRETTY_PRINT : null));

        return $response;
      }
      throw new ApiException('missing $axId in ' . __METHOD__);
    }
    throw new ApiException('missing $itemId and/or $configId in ' . __METHOD__);
  }

  /**
   * Get products' prices for given product codes.
   *
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   * @throws ApiException
   */
  public function getPrices(Request $request, Response $response): Response
  {
    $productCodes = $request->getParsedBody();
    if ($productCodes != null && is_array($productCodes)) {
      $axId = $this->userModel->get($this->tokenController->getUserId())->axId;
      if ($axId != null) {
        $priceRequests = [];
        foreach ($productCodes as $productCode) {
          $parts = explode('-', $productCode);
          $priceCustomerRequestTER = new PriceCustomerRequest_TER();
          $priceCustomerRequestTER->setCustAccount($axId);
          $priceCustomerRequestTER->setItemId($parts[0]);
          $priceCustomerRequestTER->setConfigId($parts[1]);
          $priceCustomerRequestTER->setCalculateSalesPrice(true);
          //TODO for now currency is fixed
          $priceCustomerRequestTER->setCurrency('PLN');
          $priceRequests[] = $priceCustomerRequestTER;
        }
        $arrayOfPriceCustomerRequestTER = new ArrayOfPriceCustomerRequest_TER();
        $arrayOfPriceCustomerRequestTER->setPriceCustomerRequest_TER($priceRequests);
        $params = new PriceService_TERGetCustomerPricesRequest($arrayOfPriceCustomerRequestTER);
        $axResponse = $this->priceRoutingService->GetCustomerPrices($params)->getResponse();
        foreach ($axResponse as $axPrice) {
          $axPrice->setSalesPrice(round($axPrice->getSalesPrice() * 100));
        }
        $response
          ->getBody()
          ->write(json_encode($axResponse->jsonSerialize(), $this->isDebugMode ? JSON_PRETTY_PRINT : null));

        return $response;
      }
      throw new ApiException('missing $axId in ' . __METHOD__);
    }
    throw new ApiException('unable to parse passed data');
  }

  private function prepareModel(RadiatorModelB2BStruct_TER $axModel, Request $request): RadiatorModel
  {
    $model = new RadiatorModel();
    $this->imageModel->setProperties($axModel->getLabelName(), $request->getHeaders(), null, null);
    $this->imageModel->prepareDefaultUrl();
    $this->imageModel->prepareSrcset();
    $model->setImage(clone $this->imageModel);
    //$model->setConfigExists(in_array($axModel->getRadiatorModel(), $validModels));
    $model->loadFromParentObj($axModel);
    $model->setLabelName(ucwords(mb_strtolower($model->getLabelName())));

    return $model;
  }
}
