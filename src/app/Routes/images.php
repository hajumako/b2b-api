<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 07.08.2018
 * Time: 09:07
 */

/** @SWG\Post(
 *    path="/images/bylabel/get",
 *    description="Images for products",
 *    summary="Images for given product names",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/models"),
 *    @SWG\Response(
 *      response=200,
 *      description="images response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Image")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/images/bylabel/get', ['\B2B\Controllers\ImageController', 'getImagesByLabel']);

/** @SWG\Get(
 *    path="/image/sku/{sku}",
 *    description="Get image for SKU.",
 *    summary="Get image for given SKU (itemId - configId).",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/sku"),
 *    @SWG\Response(
 *      response=200,
 *      description="image response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Image")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/image/sku/{sku}', ['\B2B\Controllers\ImageController', 'getImageBySKU']);

/** @SWG\Get(
 *    path="/image/{itemId}/{configId}",
 *    description="Get image for product (itemId & configId).",
 *    summary="Get image for given itemId & (optional) configId.",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/configId"),
 *    @SWG\Response(
 *      response=200,
 *      description="image response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Image")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/image/{itemId}/{configId}', ['\B2B\Controllers\ImageController', 'getImage']);
