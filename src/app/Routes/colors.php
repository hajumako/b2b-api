<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 08.02.2018
 * Time: 14:51
 */

/** @SWG\Get(
 *    path="/colors",
 *    description="List all available colors",
 *    summary="Get list of all available colors",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="configurator colors response",
 *      @SWG\Schema(ref="#/definitions/ConfiguratorColors"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/colors', ['\B2B\Controllers\ColorController', 'getColors']);

/** @SWG\Get(
 *    path="/colors/ingroups",
 *    description="List all available colors grouped by color range",
 *    summary="Get list of all available colors grouped by color range",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="configurator colors response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/ColorGroup")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/colors/ingroups', ['\B2B\Controllers\ColorController', 'getColorsInGroups']);
