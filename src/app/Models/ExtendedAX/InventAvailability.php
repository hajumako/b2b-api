<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 02.02.2018
 * Time: 09:36
 */

namespace B2B\Models\ExtendedAX;


use B2B\Classes\Traits\CastFromParent;
use B2B\Models\Image;
use B2B\Models\ProductConfiguration;
use B2B\Wsdl\B2BHelperTER\InventAvailabilityStruct_TER;

/** @SWG\Definition(
 *  definition="InventAvailability",
 *  type="object"
 * )
 */
class InventAvailability extends InventAvailabilityStruct_TER
{
  use CastFromParent;

  /**
   * @SWG\Property()
   * @var ProductConfiguration
   */
  protected $config;

  /**
   * @SWG\Property()
   * @var float
   */
  protected $salesPrice;

  /**
   * @SWG\Property()
   * @var float
   */
  protected $listPrice;

  /**
   * Set config
   *
   * @param ProductConfiguration $config
   */
  public function setConfig(ProductConfiguration $config)
  {
    $this->config = $config;
  }

  /**
   * Get config
   *
   * @return ProductConfiguration
   */
  public function getConfig(): ProductConfiguration
  {
    return $this->config;
  }

  /**
   * Set sales price (outlet)
   *
   * @param int $salesPrice
   */
  public function setSalesPrice(int $salesPrice)
  {
    $this->salesPrice = $salesPrice;
  }

  /**
   * Get sales price (outlet)
   *
   * @return int
   */
  public function getSalesPrice(): int
  {
    return $this->salesPrice;
  }

  /**
   * Set list price (basic)
   *
   * @param int $listPrice
   */
  public function setListPrice(int $listPrice)
  {
    $this->listPrice = $listPrice;
  }

  /**
   * Get list price (basic)
   *
   * @return int
   */
  public function getListPrice(): int
  {
    return $this->listPrice;
  }

  /**
   *  Method to load attributes from parent class object.
   *
   * @param InventAvailabilityStruct_TER $obj
   */
  public function loadFromParentObj(InventAvailabilityStruct_TER $obj)
  {
    $this->castFromParent($obj);
  }
}