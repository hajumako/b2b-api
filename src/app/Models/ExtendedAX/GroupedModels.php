<?php

namespace B2B\Models\ExtendedAX;


/** @SWG\Definition(
 *  definition="GroupedModels",
 *  type="object"
 * )
 */
class GroupedModels
{

  /**
   * @SWG\Property()
   * @var RadiatorModel
   */
  public $WG;

  /**
   * @SWG\Property()
   * @var RadiatorModel
   */
  public $WL;

  /**
   * @SWG\Property()
   * @var RadiatorModel
   */
  public $WS;

  /**
   * @SWG\Property()
   * @var RadiatorModel
   */
  public $WW;

  /**
   * @SWG\Property()
   * @var RadiatorModel
   */
  public $WZ;
}
