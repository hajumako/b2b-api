<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 06.11.2017
 * Time: 09:08
 */

namespace B2B\Classes\Constants;

/**
 * @SWG\Definition(
 *   definition="WarehouseItemTypeInterface",
 *   type="string",
 *   enum={"all", "accessories", "heaters", "radiators"},
 *   default="all"
 * )
 */
interface WarehouseItemTypeInterface
{
  const ALL = 'all';
  const ACCESSORIES = 'accessories';
  const HEATERS = 'heaters';
  const RADIATORS = 'radiators';
}
