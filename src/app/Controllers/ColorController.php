<?php
/**
 * @file
 * Contains class ConfiguratorController code implementation.
 */

namespace B2B\Controllers;


use B2B\Classes\Constants\CoatingTypeInterface;
use B2B\Classes\Constants\ColorGroupInterface;
use B2B\Models\Color;
use B2B\Models\ColorGroup;
use B2B\Models\ConfiguratorColors;
use B2B\Models\Decoration;
use B2B\Models\ExtendedAX\RadiatorColors;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetAllRadiatorColorsRequest;
use B2B\Wsdl\B2BHelperTER\NoYes;
use B2B\Wsdl\B2BHelperTER\RoutingService;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ColorController extends Controller
{
  protected $routingService;
  protected $tokenController;
  protected $userModel;
  protected $colorModel;
  protected $parser;
  protected $configValidator;
  protected $imageModel;

  /**
   * ColorController constructor.
   *
   * @param Container $container
   * @param RoutingService $routingService
   * @param Color $colorModel
   */
  public function __construct(
    Container $container,
    RoutingService $routingService,
    Color $colorModel
  ) {
    $this->routingService = $routingService;
    $this->colorModel = $colorModel;
    parent::__construct($container);
  }

  /**
   * Method to get list of all available colors.
   *
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getColors(Request $request, Response $response): Response
  {
    $params = new B2BHelper_TERGetAllRadiatorColorsRequest();
    $axColors = $this->routingService->GetAllRadiatorColors($params)->getResponse()->getRadiatorColorsStruct_TER();
    $colors = new ConfiguratorColors();

    $_colors = $this->colorModel->getList();
    foreach ($axColors as $axColor) {
      $c = new RadiatorColors($this->colorModel);
      $c->loadFromParentObj($axColor);
      $c->loadColorData($_colors);
      if ($c->getIsCastIron() === NoYes::Yes) {
        $colors->iron[] = $c->jsonSerialize();
      } elseif ($c->getIsGalvanic() === NoYes::Yes) {
        $colors->galvanic[] = $c->jsonSerialize();
      } else {
        $colors->powder[] = $c->jsonSerialize();
      }
    }

    $response
      ->getBody()
      ->write(json_encode($colors, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get all available colors divided in color groups
   * (tone & type division)
   *
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getColorsInGroups(Request $request, Response $response): Response
  {
    $params = new B2BHelper_TERGetAllRadiatorColorsRequest();
    $axColors = $this->routingService->GetAllRadiatorColors($params)->getResponse()->getRadiatorColorsStruct_TER();

    $colorGroups = array();
    foreach (ColorGroupInterface::GROUPS as $group) {
      $colorGroups[] = new ColorGroup(
        $group['label'],
        new Decoration($group['decoration']['type'], $group['decoration']['value']),
        $group['range']
      );
    }

    foreach ($axColors as $axColor) {
      $c = new RadiatorColors($this->colorModel);
      $c->loadColorData($axColor);

      if ($c->getIsCastIron() === NoYes::Yes) {
        $colorGroups[0]->type = CoatingTypeInterface::IRON;
        $colorGroups[0]->colors[] = $c->jsonSerialize();
      } elseif ($c->getIsGalvanic() === NoYes::Yes) {
        $colorGroups[1]->type = CoatingTypeInterface::GALVANIC;
        $colorGroups[1]->colors[] = $c->jsonSerialize();
      } else {
        $colorId = intval($c->getColorId());
        if ($colorId) {
          foreach ($colorGroups as $i => &$colorGroup) {
            if ($i > 3) {
              if ($colorId >= $colorGroup->range[0] && $colorId <= $colorGroup->range[1]) {
                $colorGroup->type = CoatingTypeInterface::POWDER;
                $colorGroup->colors[] = $c->jsonSerialize();
                $found = true;
                break;
              }
            }
          }
          if (empty($found)) {
            $colorGroups[3]->type = CoatingTypeInterface::POWDER;
            $colorGroups[3]->colors[] = $c->jsonSerialize();
          }
        } else {
          $colorGroups[2]->type = CoatingTypeInterface::POWDER;
          $colorGroups[2]->colors[] = $c->jsonSerialize();
        }
      }
    }

    $response
      ->getBody()
      ->write(json_encode($colorGroups, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }
}
