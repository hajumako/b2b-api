<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 09.10.2017
 * Time: 14:17
 */

namespace B2B\Classes\Constants;

/**
 * @SWG\Definition(
 *   definition="PaymentMethodInterface",
 *   type="string",
 *   enum={"payu","commerce_cod"},
 *   default="payu"
 * )
 */
interface PaymentMethodInterface
{
  const PAYU = 'payu';
  const ON_DELIVERY = 'commerce_cod';
}
