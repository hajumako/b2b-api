<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 11.08.2017
 * Time: 14:33
 */

namespace B2B\Models;

/**
 * @SWG\Definition(
 *   definition="StockModel",
 *   type="object"
 *  )
 */
class StockModel extends Model
{
  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $nid;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelName;
}
