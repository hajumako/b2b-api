<?php

/** @SWG\Info(
 *    title="Terma B2B API",
 *    description="",
 *    version="0.0.1",
 *    @SWG\Contact(
 *      email="konrad.kucharski@termagroup.pl",
 *      name="Konrad Kucharski",
 *      url="http://termagroup.pl"
 *    )
 *  )
 *  @SWG\Swagger(
 *    host=API_HOST,
 *    basePath="/",
 *    schemes={API_SCHEMA},
 *    produces={"application/json; charset=utf-8"},
 *    consumes={"application/json; charset=utf-8"},
 *    @SWG\ExternalDocumentation(
 *      description="Swagger documentation",
 *      url=API_DOC_URL
 *    )
 *  )
 */

/** @SWG\Parameter(
 *    parameter="credentials",
 *    name="creds",
 *    in="body",
 *    required=true,
 *    @SWG\Schema(ref="#/definitions/Credentials")
 *  )
 */

/** @SWG\Parameter(
 *    parameter="language",
 *    name="lang",
 *    in="path",
 *    description="Language",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="node_id",
 *    name="nid",
 *    in="path",
 *    description="Node id",
 *    required=true,
 *    type="integer",
 *    format="int32"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="productType",
 *    name="productType",
 *    in="path",
 *    description="Product type",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="order_id",
 *    name="orderId",
 *    in="path",
 *    description="Order Id",
 *    required=true,
 *    type="integer",
 *    format="int32"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="order_id_ax",
 *    name="orderIdAX",
 *    in="path",
 *    description="Order Id AX",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="order_status",
 *    name="orderStatus",
 *    in="body",
 *    description="Order status",
 *    required=true,
 *    @SWG\Schema(ref="#/definitions/OrderStatus")
 *  )
 */

/** @SWG\Parameter(
 *    parameter="addresses",
 *    name="addresses",
 *    in="body",
 *    description="Array of Addresses",
 *    required=true,
 *    @SWG\Schema(
 *      type="array",
 *      @SWG\Items(ref="#/definitions/Address")
 *    )
 *  )
 */

/** @SWG\Parameter(
 *    parameter="address_type",
 *    name="addressType",
 *    in="path",
 *    description="Address type returned",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="line_items",
 *    name="lineItems",
 *    in="body",
 *    description="Array of Line Items",
 *    required=true,
 *    @SWG\Schema(
 *      type="array",
 *      @SWG\Items(ref="#/definitions/LineItem")
 *    )
 *  )
 */

/** @SWG\Parameter(
 *    parameter="skus",
 *    name="skus",
 *    in="body",
 *    description="Array of skus",
 *    required=true,
 *    @SWG\Schema(
 *      type="array",
 *      @SWG\Items(type="string")
 *    )
 *  )
 */

/** @SWG\Parameter(
 *    parameter="delivery_type",
 *    name="deliveryType",
 *    in="body",
 *    description="Delivery Type",
 *    required=true,
 *    @SWG\Schema(ref="#/definitions/DeliveryType")
 *  )
 */

/** @SWG\Parameter(
 *    parameter="payment_method",
 *    name="paymentMethod",
 *    in="body",
 *    description="Payment Method",
 *    required=true,
 *    @SWG\Schema(ref="#/definitions/PaymentMethod")
 *  )
 */

/** @SWG\Parameter(
 *    parameter="payment",
 *    name="payment",
 *    in="body",
 *    description="Payment",
 *    required=true,
 *    @SWG\Schema(ref="#/definitions/Payment")
 *  )
 */

/** @SWG\Parameter(
 *    parameter="new_order",
 *    name="newOrder",
 *    in="body",
 *    description="New Order",
 *    required=true,
 *    @SWG\Schema(ref="#/definitions/NewOrder")
 *  )
 */

/** @SWG\Parameter(
 *    parameter="limit",
 *    name="limit",
 *    in="query",
 *    description="How many items to return at one time",
 *    required=false,
 *    type="integer",
 *    format="int32"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="filter",
 *    name="filter",
 *    in="query",
 *    description="Filter result by column value (only AND)",
 *    required=false,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="order",
 *    name="order",
 *    in="query",
 *    description="Order result by column value (multiple columns separated by comma)",
 *    required=false,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="offset",
 *    name="offset",
 *    in="query",
 *    description="Offset for limit",
 *    required=false,
 *    type="integer",
 *    format="int32"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="groupId",
 *    name="groupId",
 *    in="path",
 *    description="Product's group name",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="model",
 *    name="model",
 *    in="path",
 *    description="Model name",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="models",
 *    name="models",
 *    in="body",
 *    description="Model names",
 *    required=true,
 *    @SWG\Schema(
 *      type="array",
 *      @SWG\Items(type="string")
 *    )
 * )
 */

/** @SWG\Parameter(
 *    parameter="itemId",
 *    name="itemId",
 *    in="path",
 *    description="Item Id",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="configId",
 *    name="configId",
 *    in="path",
 *    description="Config Id",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="heaterItemId",
 *    name="heaterItemId",
 *    in="path",
 *    description="Heater Item Id",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="sku",
 *    name="sku",
 *    in="path",
 *    description="SKU",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="colorId",
 *    name="colorId",
 *    in="path",
 *    description="Color Id",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="connectionId",
 *    name="connectionId",
 *    in="path",
 *    description="Connection Id",
 *    required=true,
 *    type="string"
 *  )
 */

/** @SWG\Parameter(
 *    parameter="jsonObj",
 *    name="jsonObj",
 *    in="body",
 *    description="General object",
 *    required=true,
 *    @SWG\Schema(ref="#/definitions/JsonObj")
 * )
 */

/**
 * @SWG\Definition(
 *   definition="JsonObj",
 *   type="object"
 *  )
 */

/**
 * @SWG\Definition(
 *   definition="ProductLabel",
 *   type="object",
 *   @SWG\Property(
 *      property="model",
 *      type="string"
 *   ),
 *   @SWG\Property(
 *      property="label",
 *      type="string"
 *   )
 *  )
 */