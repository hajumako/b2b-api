<?php
/**
 * @file
 * Contains class LoginController code implementation.
 */

namespace B2B\Controllers;

use B2B\Classes\Constants\DeliveryTypeInterface;
use B2B\Classes\Constants\PaymentMethodInterface;
use B2B\Classes\Exceptions\UnauthorizedException;
use B2B\Classes\ObjectTranslator;
use B2B\Models\Address;
use B2B\Models\DeliveryType;
use B2B\Models\PaymentMethod;
use B2B\Models\User;
use B2B\Wsdl\DtOutletServicesGroup\DTOutletAddress;
use B2B\Wsdl\DtOutletServicesGroup\DTOutletAddressRole;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Dflydev\FigCookies\SetCookie;
use Dflydev\FigCookies\FigResponseCookies;

class UserController extends Controller
{
  private $tokenController;
  protected $userModel;
  protected $addressModel;

  public function __construct(
    Container $container,
    TokenController $tokenController,
    User $userModel,
    Address $addressModel
  ) {
    $this->tokenController = $tokenController;
    $this->userModel = $userModel;
    $this->addressModel = $addressModel;
    parent::__construct($container);
  }

  /**
   * Login
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function login(Request $request, Response $response): Response
  {
    $body = $request->getParsedBody();
    $u = $body['user'];
    $p = $body['password'];
    if (!empty($u) && !empty($p)) {
      $account = $this->userModel->getForAuth($u);
      // Import Drupal password functions.
      if ($account->uid) {
        include __DIR__ . '/../Imported/password.inc';
        if (call_user_func_array('user_check_password', [$p, $account])) {
          $cookieParams = $this->drupalLogin($account->uid);
          $response = FigResponseCookies::set(
            $response,
            SetCookie::create($cookieParams->name)
              ->withValue($cookieParams->value)
              ->withHttpOnly($cookieParams->httpOnly)
              ->withExpires($cookieParams->expires)
              ->withDomain($cookieParams->domain)
              ->withSecure($cookieParams->secure)
              ->withPath($cookieParams->path)
          );

          return $this->tokenController->issueTokenResponse($account->uid, $account->name, $request, $response);
        }
      }
    }
    throw new UnauthorizedException('invalid credentials ' . __METHOD__);
  }

  /**
   * Logout
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function logout(Request $request, Response $response): Response
  {
    $domain = $this->tokenController->getDomain($request->getHeader('origin')[0]);

    foreach ($request->getCookieParams() as $n => $v) {
      if (substr($n, 0, 4) === 'SESS' || $n === $this->tokenController->getTokenName()) {
        $response = FigResponseCookies::set(
          $response,
          SetCookie::create($n)
            ->withValue($v)
            ->withHttpOnly(true)
            ->withExpires(-1)
            ->withDomain($domain)
        );
      }
    }

    $result = $this->drupalLogout($this->tokenController->getUserId());
    $response
      ->getBody()
      ->write($result);

    return $response;
  }

  /**
   * Get addresses from AX for user
   *
   * @param string $addressType
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getAXAddresses(string $addressType, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid !== null) {
      $account = $this->userModel->get($uid);
      if (!empty($account->axId)) {
        $axUser = $this->userModel->getAxUser($account->axId, true);
        $addresses = array();
        foreach ($axUser->getAddresses()->getDTOutletAddress() as $address) {
          if (
            (
              $addressType === 'all' &&
              (
                in_array($address->getAddressRole(), [DTOutletAddressRole::Delivery, DTOutletAddressRole::Invoice]) ||
                $address->getPrimary()
              )
            ) ||
            (
              $addressType === 'delivery' &&
              (
                $address->getAddressRole() === DTOutletAddressRole::Delivery ||
                $address->getPrimary()
              )
            ) ||
            (
              $addressType === 'invoice' &&
              $address->getPrimary()
            )
          ) {
            //translate DTOutletAddress[] to Address[]
            $trans = ObjectTranslator::translate($address, DTOutletAddress::class, Address::class);
            if ($trans !== null) {
              $trans->organisationName = $axUser->getName();
              array_push($addresses, $trans);
            }
          }
        }
        $response
          ->getBody()
          ->write(json_encode($addresses, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

        return $response;
      }
      throw new UnauthorizedException('null $axId in ' . __METHOD__);
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get user data from AX
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getUser(Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid !== null) {
      $account = $this->userModel->get($uid);
      if (!empty($account->axId)) {
        $axUser = $this->userModel->getAxUser($account->axId, true);

        //$addresses = array();
        //translate DTOutletAddress[] to Address[]
        /*foreach ($axUser->getAddresses()->getDTOutletAddress() as $address) {
          $trans = ObjectTranslator::translate($address, DTOutletAddress::class, Address::class);
          if ($trans !== null) {
            array_push($addresses, $trans);
          }
        }*/
        //$axUserSerialized = $axUser->jsonSerialize(false);
        //$axUserSerialized['addresses'] = $addresses;
        $response
          ->getBody()
          ->write(json_encode($axUser->jsonSerialize(false), $this->isDebugMode ? JSON_PRETTY_PRINT : null));

        return $response;
      }
      throw new UnauthorizedException('null $axId in ' . __METHOD__);
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Method to get country regions from AX.
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function getCountries(Request $request, Response $response): Response
  {
    $countries = $this->addressModel->getCountryRegions();
    $response
      ->getBody()
      ->write(json_encode($countries, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Method to get user's default deliery type
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getDefaultDeliveryType(Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid !== null) {
      $delivertyType = new DeliveryType($this->container);
      $delivertyType->deliveryType = DeliveryTypeInterface::COURIER_PAID;

      $response
        ->getBody()
        ->write(json_encode($delivertyType, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Method to get user's default payment method
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getDefaultPaymentMethod(Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid !== null) {
      $paymentMethod = new PaymentMethod($this->container);
      $paymentMethod->paymentMethod = PaymentMethodInterface::PAYU;

      $response
        ->getBody()
        ->write(json_encode($paymentMethod, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  private function drupalLogin(string $uid = null)
  {
    if ($uid) {
      $ch = curl_init();
      curl_setopt_array(
        $ch,
        [
          CURLOPT_URL => $this->container->get('drupal_remote_login_url'),
          CURLOPT_POST => 1,
          CURLOPT_POSTFIELDS => array(
            'remote_auth_key' => $this->container->get('drupal_remote_auth_key'),
            'uid' => $uid,
          ),
          CURLOPT_RETURNTRANSFER => true,
        ]
      );

      $result = json_decode(curl_exec($ch));
      curl_close($ch);

      return $result;
    }

    return null;
  }

  private function drupalLogout(string $uid = null)
  {
    if ($uid) {
      $ch = curl_init();
      curl_setopt_array(
        $ch,
        [
          CURLOPT_URL => $this->container->get('drupal_remote_logout_url'),
          CURLOPT_POST => 1,
          CURLOPT_POSTFIELDS => array(
            'remote_auth_key' => $this->container->get('drupal_remote_auth_key'),
            'uid' => $uid,
          ),
          CURLOPT_RETURNTRANSFER => true,
        ]
      );

      $result = curl_exec($ch);
      curl_close($ch);

      return $result;
    }

    return null;
  }
}
