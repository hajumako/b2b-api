<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 08.02.2018
 * Time: 14:52
 */

/** @SWG\Post(
 *    path="/order/create",
 *    description="Create new order",
 *    summary="Create new order when item added to cart",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/new_order"),
 *    @SWG\Response(
 *      response=200,
 *      description="new order response",
 *      @SWG\Schema(ref="#/definitions/Order"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/order/create', ['\B2B\Controllers\OrderController', 'create']);

/** @SWG\Get(
 *    path="/order/cart/get",
 *    description="Get cart order",
 *    summary="Get order with 'cart' status",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="order response",
 *      @SWG\Schema(ref="#/definitions/Order"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/order/cart/get', ['\B2B\Controllers\OrderController', 'getCart']);

/** @SWG\Post(
 *    path="/order/{orderId}/status/get",
 *    description="Get order status",
 *    summary="Get status of the order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="order status response",
 *      @SWG\Schema(ref="#/definitions/OrderStatus"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/order/{orderId}/status/get', ['\B2B\Controllers\OrderController', 'getStatus']);

/** @SWG\Post(
 *    path="/order/{orderId}/status/set",
 *    description="Update order status",
 *    summary="Update status of the order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Parameter(ref="#/parameters/order_status"),
 *    @SWG\Response(
 *      response=200,
 *      description="order status response",
 *      @SWG\Schema(ref="#/definitions/OrderStatus"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/order/{orderId}/status/set', ['\B2B\Controllers\OrderController', 'setStatus']);

/** @SWG\Get(
 *    path="/account/ax/addresses/get/{addressType}",
 *    description="Customer addresses from AX based on type",
 *    summary="Get customer addresses from AX based on type",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/address_type"),
 *    @SWG\Response(
 *      response=200,
 *      description="customer response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Address")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/account/ax/addresses/get/{addressType}', ['\B2B\Controllers\UserController', 'getAXAddresses']);

/** @SWG\Get(
 *    path="/order/{orderId}/addresses/get",
 *    description="Get all addresses",
 *    summary="Get all addresses (delivery & invoice) for order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="address response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Address")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/order/{orderId}/addresses/get', ['\B2B\Controllers\OrderController', 'getAddresses']);

/** @SWG\Post(
 *    path="/order/{orderId}/addresses/set",
 *    description="Save addresses for order",
 *    summary="Create new order when item added to cart",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Parameter(ref="#/parameters/addresses"),
 *    @SWG\Response(
 *      response=200,
 *      description="address response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Address")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/order/{orderId}/addresses/set', ['\B2B\Controllers\OrderController', 'setAddresses']);

/** @SWG\Get(
 *    path="/order/{orderId}/lineitems/get",
 *    description="Get all line items",
 *    summary="Get all line items for order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="line item response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/LineItem")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/order/{orderId}/lineitems/get', ['\B2B\Controllers\OrderController', 'getLineItems']);

/** @SWG\Post(
 *    path="/order/{orderId}/lineitems/set",
 *    description="Save line items for order",
 *    summary="Set line items (add/update) in order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Parameter(ref="#/parameters/line_items"),
 *    @SWG\Response(
 *      response=200,
 *      description="line item response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/LineItem")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/order/{orderId}/lineitems/set', ['\B2B\Controllers\OrderController', 'setLineItems']);

/** @SWG\Get(
 *    path="/order/ax/{orderIdAX}/lineitems/get",
 *    description="Get all AX line items",
 *    summary="Get all line items for AX order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id_ax"),
 *    @SWG\Response(
 *      response=200,
 *      description="AX line item response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/SalesLineContract")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/order/ax/{orderIdAX}/lineitems/get', ['\B2B\Controllers\OrderController', 'getAXLineItems']);

/** @SWG\Get(
 *    path="/order/{orderId}/deliverytype/get",
 *    description="Get delivery type",
 *    summary="Get delivery type for order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="delivery type response",
 *      @SWG\Schema(ref="#/definitions/DeliveryType"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/order/{orderId}/deliverytype/get', ['\B2B\Controllers\OrderController', 'getDeliveryType']);

/** @SWG\Post(
 *    path="/order/{orderId}/deliverytype/set",
 *    description="Set delivery type",
 *    summary="Set delivery type for order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Parameter(ref="#/parameters/delivery_type"),
 *    @SWG\Response(
 *      response=200,
 *      description="delivery type response",
 *      @SWG\Schema(ref="#/definitions/DeliveryType"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/order/{orderId}/deliverytype/set', ['\B2B\Controllers\OrderController', 'setDeliveryType']);

/** @SWG\Get(
 *    path="/order/{orderId}/paymentmethod/get",
 *    description="Get payment method",
 *    summary="Get payment method for order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="payment method response",
 *      @SWG\Schema(ref="#/definitions/Payment"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/order/{orderId}/paymentmethod/get', ['\B2B\Controllers\OrderController', 'getPaymentMethod']);

/** @SWG\Post(
 *    path="/order/{orderId}/paymentmethod/set",
 *    description="Set delivery type",
 *    summary="Set delivery type for order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Parameter(ref="#/parameters/payment"),
 *    @SWG\Response(
 *      response=200,
 *      description="payment method response",
 *      @SWG\Schema(ref="#/definitions/Payment"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/order/{orderId}/paymentmethod/set', ['\B2B\Controllers\OrderController', 'setPaymentMethod']);

/** @SWG\Get(
 *    path="/order/{orderId}/data/get",
 *    description="Get additional data",
 *    summary="Get additional data for order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="additional data response",
 *      @SWG\Schema(ref="#/definitions/JsonObj"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/order/{orderId}/data/get', ['\B2B\Controllers\OrderController', 'getAdditionalData']);

/** @SWG\Post(
 *    path="/order/{orderId}/data/set",
 *    description="Set additional data",
 *    summary="Set additional data for order",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Parameter(ref="#/parameters/jsonObj"),
 *    @SWG\Response(
 *      response=200,
 *      description="additional data response",
 *      @SWG\Schema(ref="#/definitions/JsonObj"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/order/{orderId}/data/set', ['\B2B\Controllers\OrderController', 'setAdditionalData']);

/** @SWG\Post(
 *    path="/order/{orderId}/defaultdata",
 *    description="Fill order with the default data",
 *    summary="Fill order with the default data (delivery & invoice address, delivery type, payment method)",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="order response",
 *      @SWG\Schema(ref="#/definitions/Order"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/order/{orderId}/defaultdata', ['\B2B\Controllers\OrderController', 'fillOrderWithDefaultData']);
