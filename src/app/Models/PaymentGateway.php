<?php


namespace B2B\Models;

/** @SWG\Definition(
 *   definition="PaymentGateway",
 *   type="object"
 *  )
 */
class PaymentGateway
{
  /**
   * @SWG\Property()
   * @var string
   */
  public $redirectUri;
}
