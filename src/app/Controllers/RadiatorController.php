<?php
/**
 * @file
 * Contains class RadiatorController code implementation.
 */

namespace B2B\Controllers;

use B2B\Classes\Exceptions\ApiException;
use B2B\Models\Radiator;
use B2B\Models\Cache;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class RadiatorController extends Controller
{
  protected $radiatorModel;
  protected $cache;

  /**
   * RadiatorController constructor.
   * @param Container $container
   * @param Radiator $radiatorModel
   * @param Cache $cache
   */
  public function __construct(Container $container, Radiator $radiatorModel, Cache $cache)
  {
    $this->radiatorModel = $radiatorModel;
    $this->cache = $cache;
    parent::__construct($container);
  }

  /**
   * Get radiators list
   *
   * @param string $lang
   *   Language
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function listItems(string $lang, Request $request, Response $response): Response
  {
    $items = $this->radiatorModel->getList($lang, $request);
    $response
      ->getBody()
      ->write(json_encode($items, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
    $this->total = $this->radiatorModel->getTotal();

    return parent::addHeaders($lang, $request, $response);
  }

  /**
   * Get radiator details
   *
   * @param string $lang
   *   Language
   * @param int $nid
   *    Node id
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function itemDetails(string $lang, int $nid, Request $request, Response $response): Response
  {
    $item = $this->radiatorModel->get($nid, $lang);
    $response
      ->getBody()
      ->write(json_encode($item, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /*public function itemDetailsByModel(string $lang, Request $request, Response $response): Response
  {
    $items = $this->radiatorModel->getList($lang, $request);
    if(count($items))
  }*/


  public function getRadiatorAXSpecsBySku(string $sku, Request $request, Response $response) : Response
  {
    $skuArr = explode('-', $sku);
    if(count($skuArr) === 2) {
      return $this->getRadiatorAXSpecsByParts($skuArr[0], $skuArr[1], $request, $response);
    }

    throw new ApiException('unable to parse passed data');
  }

  /**
   * @param string $itemId
   * @param string $configId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function getRadiatorAXSpecsByParts(string $itemId, string $configId, Request $request, Response $response) : Response
  {
    $specs = $this->cache->getRadiatorSpecs($itemId, $configId);

    $response
      ->getBody()
      ->write(json_encode($specs, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }
}
