<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 02.02.2018
 * Time: 09:03
 */

namespace B2B\Models;


class Color extends Model
{
  const TABLE = "api_colors";

  public $id;

  public $colorId;

  public $name;

  public $type;

  public $hex;

  public $rgb;

  public $img;

  public $height;

  public $width;

  protected $colorsList;

  public function getList(): array
  {
    if(!$this->colorsList) {
      $statement = "SELECT * FROM ".self::TABLE;

      $this->colorsList = array();
      foreach (parent::getItems($statement, null, self::class) as $item) {
        $item->img = str_replace('public://', $this->container->get('img_host'), $item->img);
        $this->colorsList[$item->colorId] = $item;
      }
    }

    return $this->colorsList;
  }
}