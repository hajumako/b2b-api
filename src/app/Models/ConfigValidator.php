<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 05.02.2018
 * Time: 12:16
 */

namespace B2B\Models;


use B2B\Classes\Helpers\ProductGroupIdShort;
use PDO;

class ConfigValidator extends Model
{
  /**
   * Get configurations' settings by model and (optional) size, color & connection
   * returns products that were in the warehouse (have been ever ordered)
   *
   * @param string $model
   * @param string|null $size
   * @param string|null $colorId
   * @param string|null $connectionId
   *
   * @return array
   */
  public function getByParts(
    string $model,
    string $size = null,
    string $colorId = null,
    string $connectionId = null
  ): array {
    $regex = '^'.$model;
    if ($size) {
      $regex .= $size;

      if ($colorId) {
        $regex .= '-K'.$colorId;
      } elseif ($connectionId) {
        $regex .= '-K.{3}';
      }

      if ($connectionId) {
        $regex .= $connectionId;
      }
    }

    return $this->queryDB($regex);
  }

  /**
   * Get configurations' settings by itemId & (optional) configId
   * returns products that were in the warehouse (have been ever ordered)
   *
   * @param string $itemId
   * @param string|null $configId
   *
   * @return array
   */
  public function getBySKU(string $itemId, string $configId = null): array
  {
    $regex = '^'.$itemId;
    if ($configId) {
      $regex .= '-'.$configId;
    }

    return $this->queryDB($regex);
  }


  /**
   * Check if configuration settings exist by model and (optional) size, color & connection
   * returns true if product was in the warehouse (has been ever ordered)
   *
   * @param string $model
   * @param string|null $size
   * @param string|null $colorId
   * @param string|null $connectionId
   *
   * @return bool
   */
  public function checkByParts(
    string $model,
    string $size = null,
    string $colorId = null,
    string $connectionId = null
  ): bool {
    return count($this->getByParts($model, $size, $colorId, $connectionId)) ? true : false;
  }

  /**
   * Check if configuration settings exist by itemId & (optional) configId
   * returns true if product was in the warehouse (has been ever ordered)
   *
   * @param string $itemId
   * @param string|null $configId
   *
   * @return bool
   */
  public function checkBySKU(string $itemId, string $configId = null): bool
  {
    return count($this->getBySKU($itemId, $configId)) ? true : false;
  }

  /**
   * Get list of valid models for given product group
   *
   * @param ProductGroupIdShort $groupId
   *   Product group short name
   *
   * @return string[]
   */
  public function getValidModels(string $groupId): array {
    $statement = "CALL api_get_valid_models('$groupId')";
    $query = $this->db->query($statement);

    return $query->fetchAll(PDO::FETCH_COLUMN);
  }

  /**
   * Get list of valid sizes for given model
   *
   * @param string $model
   *   Product model - product group (2 letters) + model shortname (3 signs)
   *
   * @return string[]
   */
  public function getValidSizes(string $model): array {
    $statement = "CALL api_get_valid_sizes('$model')";
    $query = $this->db->query($statement);

    return $query->fetchAll(PDO::FETCH_COLUMN);
  }

  /**
   * Get list of valid connections for given itemId
   *
   * @param string $itemId
   *   ItemId - model + size
   *
   * @return string[]
   */
  public function getValidConnections(string $itemId): array
  {
    $statement = "CALL api_get_valid_connections('$itemId')";
    $query = $this->db->query($statement);

    return $query->fetchAll(PDO::FETCH_COLUMN);
  }

  /**
   * Get list of valid connections for given itemId
   *
   * @param string $itemId
   *   ItemId - model + size
   * @param string $connectionId
   *   ConnectionId
   *
   * @return string[]
   */
  public function getValidColors(string $itemId, string $connectionId): array {
    $regex = $itemId.'-K.{3}'.$connectionId;
    $statement = "CALL api_get_valid_colors('$regex')";
    $query = $this->db->query($statement);

    return $query->fetchAll(PDO::FETCH_COLUMN);
  }

  /**
   * Query db for configuration settings
   *
   * @param string $regex
   *
   * @return array
   */
  protected function queryDB(string $regex)
  {
    $statement = "CALL api_validate_config('$regex')";
    $query = $this->db->query($statement);

    return $query->fetchAll(PDO::FETCH_COLUMN);
  }

  public function q3(string $groupId) {
    $statement = "SELECT substring(sku, 1, 5) as x FROM theat_commerce_product WHERE sku REGEXP '^$groupId' AND type='grzejnik' group by x;";
    $query = $this->db->query($statement);

    return $query->fetchAll(PDO::FETCH_COLUMN);
  }
}