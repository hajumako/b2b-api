<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 09.10.2017
 * Time: 14:34
 */

namespace B2B\Models;

use B2B\Classes\Constants\PaymentMethodInterface;

/**
 * @SWG\Definition(
 *   definition="PaymentMethod",
 *   type="object"
 * )
 */
class PaymentMethod extends Model
{
  /**
   * @SWG\Property(
   *   ref="#/definitions/PaymentMethodInterface"
   * )
   * @var PaymentMethodInterface
   */
  public $paymentMethod;
}

