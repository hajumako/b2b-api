<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 08.02.2018
 * Time: 10:49
 */

namespace B2B\Controllers;


use B2B\Classes\ProductCodeParser;
use B2B\Models\Image;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ImageController extends Controller
{
  protected $productCodeParser;
  protected $imageModel;

  /**
   * ImageController constructor.
   *
   * @param Container $container
   * @param ProductCodeParser $productCodeParser
   * @param Image $imageModel
   */
  public function __construct(Container $container, ProductCodeParser $productCodeParser, Image $imageModel)
  {
    $this->productCodeParser = $productCodeParser;
    $this->imageModel = $imageModel;
    parent::__construct($container);
  }

  /**
   * Get image for given itemId & (optional) configId
   *
   * @param string $itemId
   *   Product item id (model, size).
   * @param string $configId
   *   Product configuration id (color, connection).
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getImage(string $itemId, string $configId, Request $request, Response $response): Response
  {
    $this->imageModel->setSKU($itemId.'-'.$configId, $request->getHeaders());
    $this->imageModel->prepareDefaultUrl();
    $this->imageModel->prepareSrcset();
    $response
      ->getBody()
      ->write(json_encode($this->imageModel, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get image for given SKU (itemId - configId)
   *
   * @param string $sku
   *   Product code.
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getImageBySKU(string $sku, Request $request, Response $response): Response
  {
    $this->imageModel->setSKU($sku, $request->getHeaders());
    $this->imageModel->prepareDefaultUrl();
    $this->imageModel->prepareSrcset();
    $response
      ->getBody()
      ->write(json_encode($this->imageModel, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get images for given product name
   *
   * @param Request $request
   *   Request object.
   * @param Response $response
   *   Response object.
   *
   * @return Response
   */
  public function getImagesByLabel(Request $request, Response $response): Response
  {
    $body = $request->getParsedBody();
    $images = [];
    if(is_array($body)) {
      foreach ($body as $labelName) {
        $this->imageModel->setProperties($labelName, $request->getHeaders(), null, Image::DEF);
        $this->imageModel->prepareDefaultUrl();
        $this->imageModel->prepareSrcset();
        $images[] = clone $this->imageModel;
      }
    }
    $response->getBody()
      ->write(json_encode($images, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }
  //TODO add method to get image from partial sku
}