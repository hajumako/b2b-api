<?php

namespace B2B\Classes\Helpers;


abstract class DrupalProductBundle
{
  /**
   * Drupal constants - bundles of product nodes.
   */
  const HEATER = 'grzalka';
  const RADIATOR = 'grzejnik';
  const HEATING_APPLIANCE = 'akcesorium_grzewcze';

}
