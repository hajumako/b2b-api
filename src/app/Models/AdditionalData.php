<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 30.08.2017
 * Time: 12:23
 */

namespace B2B\Models;

/**
 * @SWG\Definition(
 *   definition="AdditionalData",
 *   type="object"
 * )
 */
class AdditionalData
{
  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $additionalData;
}
