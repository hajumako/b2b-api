<?php

namespace B2B\Models;


/** @SWG\Definition(
 *    definition="RadiatorSpecs",
 *    type="object"
 *  )
 */
class RadiatorSpecs
{
  /**
   * @SWG\Property()
   * @var string
   */
  public $modelName;
}
