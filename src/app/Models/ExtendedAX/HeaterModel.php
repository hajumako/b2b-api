<?php

namespace B2B\Models\ExtendedAX;

use B2B\Classes\Traits\CastFromParent;
use B2B\Models\Image;
use B2B\Wsdl\B2BHelperTER\HeaterContract_TER;

/** @SWG\Definition(
 *  definition="HeaterModel",
 *  type="object"
 * )
 */
class HeaterModel extends HeaterContract_TER
{
  use CastFromParent;

  /**
   * @SWG\Property()
   * @var Image
   */
  protected $image;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/Image")
   * )
   * @var Image[]
   */
  protected $images;

  /**
   * @SWG\Property()
   * @var boolean
   */
  protected $configExists;

  /**
   * @SWG\Property()
   * @var string
   */
  protected $labelName;

  /**
   * @SWG\Property()
   * @var string
   */
  protected $displayedName;

  /**
   * @SWG\Property()
   * @var string
   */
  protected $power;

  /**
   * Set image
   *
   * @param Image $image
   */
  public function setImage(Image $image)
  {
    $this->image = $image;
  }

  /**
   * Get image
   *
   * @return Image
   */
  public function getImage(): Image
  {
    return $this->image;
  }

  /**
   * Set images
   *
   * @param Image[] $images
   */
  public function setImages(array $images)
  {
    $this->images = $images;
  }

  /**
   * Get images
   *
   * @return Image[]
   */
  public function getImages(): array
  {
    return $this->images;
  }

  /**
   * Set product label
   *
   * @param string $labelName
   */
  public function setLabelName(string $labelName)
  {
    $this->labelName = $labelName;
  }

  /**
   * Get product label
   *
   * @return string
   */
  public function getLabelName(): string
  {
    return $this->labelName;
  }

  /**
   * Set product displayed name
   *
   * @param string $labelName
   */
  public function setDisplayedName(string $displayedName)
  {
    $this->displayedName = $displayedName;
  }

  /**
   * Get product displayed name
   *
   * @return string
   */
  public function getDisplayedName(): string
  {
    return $this->displayedName;
  }


  /**
   * Set product power
   *
   * @param string $power
   */
  public function setPower(string $power = '')
  {
    $this->power = $power;
  }

  /**
   * Get product power
   *
   * @return string
   */
  public function getPower(): string
  {
    return $this->power;
  }

  /**
   * Set config exists
   *
   * @param bool $exists
   */
  public function setConfigExists(bool $exists) {
    $this->configExists = $exists;
  }

  /**
   * Get config exists
   *
   * @return bool
   */
  public function getConfigExists() {
    return $this->configExists;
  }

  /**
   *  Method to load attributes from parent class object.
   *
   * @param HeaterContract_TER $obj
   */
  public function loadFromParentObj(HeaterContract_TER $obj)
  {
    $this->castFromParent($obj);
  }
}
