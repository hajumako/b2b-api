<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 30.08.2017
 * Time: 12:23
 */

namespace B2B\Models;

use B2B\Classes\Constants\OrderStatusInterface;

/**
 * @SWG\Definition(
 *   definition="OrderStatus",
 *   type="object"
 * )
 */
class OrderStatus
{
  /**
   * @SWG\Property(
   *   ref="#/definitions/OrderStatusInterface"
   * )
   * @var OrderStatusInterface
   */
  public $status;
}
