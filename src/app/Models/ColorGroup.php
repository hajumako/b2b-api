<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 23.02.2018
 * Time: 11:47
 */

namespace B2B\Models;


use B2B\Classes\Constants\CoatingTypeInterface;
use B2B\Models\ExtendedAX\RadiatorColors;

/** @SWG\Definition(
 *  definition="ColorGroup",
 *  type="object"
 * )
 */
class ColorGroup
{
  /**
   * @SWG\Property()
   * @var string
   */
  public $label;

  /**
   * @SWG\Property(
   *   ref="#/definitions/CoatingTypeInterface"
   * )
   * @var CoatingTypeInterface
   */
  public $type;

  /**
   * @SWG\Property(
   *   ref="#/definitions/Decoration"
   * )
   * @var Decoration
   */
  public $decoration;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(type="string")
   * )
   * @var string[]
   */
  public $range;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/RadiatorColors")
   * )
   * @var RadiatorColors[]
   */
  public $colors;

  public function __construct(
    string $label,
    Decoration $decoration,
    array $range,
    array $type = null,
    array $colors = null
  ) {
    $this->label = $label;
    $this->decoration = $decoration;
    $this->range = $range;
    $this->type = $type;
    $this->colors = $colors;
  }
}