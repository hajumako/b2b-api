<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 02.02.2018
 * Time: 09:36
 */

namespace B2B\Models\ExtendedAX;


use B2B\Classes\Traits\CastFromParent;
use B2B\Models\LineItem;
use B2B\Models\Order;
use B2B\Wsdl\DtOutletServicesGroup\SalesLineContract_TER;

/** @SWG\Definition(
 *  definition="SalesLineContract",
 *  type="object"
 * )
 */
class SalesLineContract extends SalesLineContract_TER
{
  use CastFromParent;

  /**
   * @SWG\Property()
   * @var string
   */
  protected $sku;

  /**
   * @SWG\Property()
   * @var LineItem
   */
  protected $drupalLineItem;

  /**
   * Set sku for AX line item
   *
   * @param string $sku
   */
  public function setSku(string $sku)
  {
    $this->sku = $sku;
  }

  /**
   * Get sku
   *
   * @return string
   */
  public function getSku(): string
  {
    return $this->sku;
  }


  /**
   * Set Drupal line item for AX line item
   *
   * @param LineItem $lineItem
   */
  public function setDrupalLineItem(LineItem $lineItem)
  {
    $this->drupalLineItem = $lineItem;
  }

  /**
   * Get Drupal line item
   *
   * @return LineItem
   */
  public function getDrupalLineItem(): LineItem
  {
    return $this->drupalLineItem;
  }



  /**
   *  Method to load attributes from parent class object.
   *
   * @param SalesLineContract_TER $obj
   */
  public function loadFromParentObj(SalesLineContract_TER $obj)
  {
    $this->castFromParent($obj);
  }
}