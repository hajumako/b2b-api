<?php
/**
 * @file
 * Contains class TokenController code implementation.
 */

namespace B2B\Controllers;

use B2B\Models\Token;
use Firebase\JWT\JWT;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;
use \Di\Container;

class TokenController
{
  protected $token_settings;
  protected $api_host;
  protected $allowed_origins;
  protected $allowed_domain;
  protected $exp;
  protected $uid;

  public function __construct(Container $container)
  {
    $this->token_settings = $container->get('token_settings');
    $this->api_host = $container->get('api_host');
    $this->allowed_origins = $container->get('allowed_origins');
    $this->allowed_domain = $container->get('allowed_domain');
    $debugConfig = $container->get('debug_config');
    if (!$debugConfig['debug_mode'] ?? false) {
      $this->uid = $container->has('decoded_token') ? $container->get('decoded_token')->uid : null;
    } else {
      //uid for tests
      $this->uid = $debugConfig['user_id'];
    }
  }

  /**
   * Get token name
   *
   * @return string
   */
  public function getTokenName(): string
  {
    return $this->token_settings['name'];
  }

  /**
   * Get user id from token
   *
   * @return int
   */
  public function getUserId()
  {
    return $this->uid;
  }

  /**
   * @param int $uid
   * @param string $name
   *
   * @return Token
   */
  public function issueToken(int $uid, string $name): Token
  {
    $time = time();
    $this->exp = $time + ($this->token_settings['valid_for']);

    $payload = array(
      "iss" => $this->api_host,
      "aud" => implode(';', $this->allowed_origins),
      "exp" => $this->exp,
      "iat" => $time,
      "uid" => $uid,
      "nam" => $name,
    );

    $token = new Token();
    $token->token = JWT::encode($payload, $this->token_settings['salt']);

    return $token;
  }

  /**
   * Build token response
   *
   * @param int $uid
   * @param string $name
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function issueTokenResponse(int $uid, string $name, Request $request, Response $response): Response
  {
    $token = $this->issueToken($uid, $name);

    $domain = $this->getDomain($request->getHeader('origin')[0]);

    $response = $response
      ->withHeader('X-Token', 'b2b.' . explode('.', $token->token)[1] . '.b2b');

    return FigResponseCookies::set(
      $response,
      SetCookie::create($this->token_settings['name'])
        ->withValue($token->token)
        ->withHttpOnly(true)
        ->withExpires($this->exp)
        ->withDomain($domain)
        ->withSecure($this->token_settings['secure'])
        ->withPath('/')
    );
  }

  /**
   * Get domain for cookie if origin is allowed
   *
   * @param string $origin
   *
   * @return mixed|null
   */
  public function getDomain(string $origin)
  {
    if (in_array($origin, $this->allowed_origins)) {
      return $this->allowed_domain;
    }

    return null;
  }
}
