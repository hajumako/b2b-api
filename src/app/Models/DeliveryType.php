<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 09.10.2017
 * Time: 11:40
 */

namespace B2B\Models;

use B2B\Classes\Constants\DeliveryTypeInterface;

/**
 * @SWG\Definition(
 *   definition="DeliveryType",
 *   type="object"
 * )
 */
class DeliveryType extends Model
{
  /**
   * @SWG\Property(
   *   ref="#/definitions/DeliveryTypeInterface"
   * )
   * @var DeliveryTypeInterface
   */
  public $deliveryType;
}
