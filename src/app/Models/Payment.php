<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 09.10.2017
 * Time: 14:34
 */

namespace B2B\Models;

use B2B\Classes\Constants\PaymentMethodInterface;

/**
 * @SWG\Definition(
 *   definition="Payment",
 *   type="object"
 * )
 */
class Payment extends Model
{
  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $transactionId;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $uid;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $orderId;

  /**
   * @SWG\Property(
   *   ref="#/definitions/PaymentMethodInterface"
   * )
   * @var PaymentMethodInterface
   */
  public $paymentMethod;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $remoteId;

  /**
   * @SWG\Property(
   *   format="float"
   * )
   * @var number
   */
  public $amount;

  /**
   * @SWG\Property()
   * @var string
   */
  public $currencyCode;

  /**
   * @SWG\Property()
   * @var string
   */
  public $status;

  /**
   * @SWG\Property()
   * @var string
   */
  public $remoteStatus;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $created;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $changed;

  /**
   * @SWG\Property()
   * @var string
   */
  public $data;
}

