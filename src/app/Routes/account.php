<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 08.02.2018
 * Time: 14:53
 */

/** @SWG\Get(
 *    path="/account",
 *    description="Customer's account",
 *    summary="Get information about customer",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="customer response",
 *      @SWG\Schema(ref="#/definitions/DtOutletCustomerRequest"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/account', ['\B2B\Controllers\UserController', 'getUser']);

/** @SWG\Get(
 *    path="/account/orders/list",
 *    description="Get user orders",
 *    summary="Get all user orders",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/filter"),
 *    @SWG\Parameter(ref="#/parameters/order"),
 *    @SWG\Parameter(ref="#/parameters/limit"),
 *    @SWG\Parameter(ref="#/parameters/offset"),
 *    @SWG\Response(
 *      response=200,
 *      description="order response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Order")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/account/orders/list', ['\B2B\Controllers\OrderController', 'listItems']);

/** @SWG\Get(
 *    path="/account/orders/ax/list",
 *    description="Get user orders from AX",
 *    summary="Get all user orders from AX",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/limit"),
 *    @SWG\Parameter(ref="#/parameters/offset"),
 *    @SWG\Response(
 *      response=200,
 *      description="AX order response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/SalesTableContract")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/account/orders/ax/list', ['\B2B\Controllers\OrderController', 'listAXItems']);

/** @SWG\Get(
 *    path="/account/orders/item/{orderId}",
 *    description="Get order's details",
 *    summary="Get order details",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Parameter(ref="#/parameters/filter"),
 *    @SWG\Parameter(ref="#/parameters/order"),
 *    @SWG\Parameter(ref="#/parameters/limit"),
 *    @SWG\Parameter(ref="#/parameters/offset"),
 *    @SWG\Response(
 *      response=200,
 *      description="order response",
 *      @SWG\Schema(ref="#/definitions/Order"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/account/orders/item/{orderId}', ['\B2B\Controllers\OrderController', 'itemDetails']);

/** @SWG\Get(
 *    path="/account/orders/ax/item/{orderIdAX}",
 *    description="Get AX order's details",
 *    summary="Get AX order's details",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id_ax"),
 *    @SWG\Response(
 *      response=200,
 *      description="AX order response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/SalesLineContract")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/account/orders/ax/item/{orderIdAX}', ['\B2B\Controllers\OrderController', 'AXItemDetails']);

/** @SWG\Get(
 *    path="/account/deliverytype",
 *    description="Get delivery type",
 *    summary="Get user's default delivery type",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="delivery type response",
 *      @SWG\Schema(ref="#/definitions/DeliveryType"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/account/deliverytype', ['\B2B\Controllers\UserController', 'getDefaultDeliveryType']);

/** @SWG\Get(
 *    path="/account/paymentmethod",
 *    description="Get payment method",
 *    summary="Get user's default payment",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="payment method response",
 *      @SWG\Schema(ref="#/definitions/PaymentMethod"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/account/paymentmethod', ['\B2B\Controllers\UserController', 'getDefaultPaymentMethod']);
