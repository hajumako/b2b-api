<?php
/**
 * @file
 * Contains class PriceListController code implementation.
 */

namespace B2B\Controllers;

use DI\Container;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Slim\Http\Request;
use Slim\Http\Response;

class PriceListController extends Controller
{

  protected $mpdf;

  public function __construct(Container $container, Mpdf $mpdf)
  {
    $this->mpdf = $mpdf;
    parent::__construct($container);
  }

  /**
   * Method to generate PDF price list.
   *
   * @param \Slim\Http\Request $request
   *   Request object.
   * @param \Slim\Http\Response $response
   *   Response object.
   */
  public function generate(Request $request, Response $response)
  {
    $productIds = $request->getParsedBody();
    if (!empty($productIds)) {
      //TODO get products data from AX and generate PDF.
      $this->mpdf->WriteHTML('<h1>Cennik Terma</h1>');
      $filename = 'price-list-'.time().'.pdf';
      $this->mpdf->Output($filename, Destination::INLINE);
    }
  }

}
