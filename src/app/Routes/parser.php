<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 31.07.2018
 * Time: 11:49
 */

/** @SWG\Post(
 *    path="/parser/codes/validate",
 *    description="Validate list of skus",
 *    summary="Check against AX if given skus are valid",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/skus"),
 *    @SWG\Response(
 *      response=200,
 *      description="validation result response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/InventConfigurationStateStruct_TER")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/parser/codes/validate', ['\B2B\Controllers\ParserController', 'validateCodes']);
