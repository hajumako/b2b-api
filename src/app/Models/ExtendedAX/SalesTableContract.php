<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 02.02.2018
 * Time: 09:36
 */

namespace B2B\Models\ExtendedAX;


use B2B\Classes\Traits\CastFromParent;
use B2B\Models\Order;
use B2B\Wsdl\DtOutletServicesGroup\SalesTableContract_TER;

/** @SWG\Definition(
 *  definition="SalesTableContract",
 *  type="object"
 * )
 */
class SalesTableContract extends SalesTableContract_TER
{
  use CastFromParent;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/SalesLineContract")
   * )
   * @var SalesLineContract[]
   */
  protected $salesLineItems;

  /**
   * @SWG\Property()
   * @var Order
   */
  protected $drupalOrder;

  /**
   * Set line items for AX order
   *
   * @param SalesLineContract[] $salesLineItems
   */
  public function setSalesLineItems(array $salesLineItems)
  {
    $this->salesLineItems = $salesLineItems;
  }

  /**
   * Get order's line items
   *
   * @return SalesLineContract[]
   */
  public function getSalesLineItems(): array
  {
    return $this->salesLineItems;
  }

  /**
   * Set Drupal order for AX order
   *
   * @param Order $order
   */
  public function setDrupalOrder(Order $order)
  {
    $this->drupalOrder = $order;
  }

  /**
   * Get Drupal order
   *
   * @return Order
   */
  public function getDrupalOrder(): Order
  {
    return $this->drupalOrder;
  }

  /**
   *  Method to load attributes from parent class object.
   *
   * @param SalesTableContract_TER $obj
   */
  public function loadFromParentObj(SalesTableContract_TER $obj)
  {
    $this->castFromParent($obj);
  }
}