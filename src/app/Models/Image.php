<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 12.01.2018
 * Time: 11:57
 */

namespace B2B\Models;


use DI\Container;

/** @SWG\Definition(
 *    definition="Image",
 *    type="object"
 *  )
 */
class Image
{
  const JPG = 'jpg';
  const JPEG = 'jpeg';
  const PNG = 'png';
  const WEBP = 'webp';

  const DEF = 'default';
  const DEF_WIDTH = 1000;
  const DEF_SRCSET = [
    300,
    600,
    900,
  ];

  /**
   * @SWG\Property()
   * @var string
   */
  public $defaultUrl;

  /**
   * @SWG\Property()
   * @var string
   */
  public $originalUrl;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(type="string")
   * )
   * @var string
   */
  public $srcset;

  /**
   * @SWG\Property()
   * @var string
   */
  public $model;

  protected $container;
  protected $logger;
  protected $cdnHost;

  protected $view;
  protected $modelSize;
  protected $modelColor;
  protected $imageType;

  public function __construct(
    Container $container
  ) {
    $this->container = $container;
    $this->logger = $container->get('logger');
    $this->cdnHost = $container->get('cdn_host');
  }

  /**
   * Generate image path based on properties (model, size, color)
   *
   * @param string $model
   * @param array|null $headers
   * @param string|null $imageType
   * @param string|null $view
   * @param string|null $modelSize
   * @param string|null $modelColor
   */
  public function setProperties(
    string $model,
    array $headers = null,
    string $imageType = null,
    string $view = null,
    string $modelSize = null,
    string $modelColor = null
  ) {
    $this->model = $model;
    $this->view = $view ?: $this->container->get('def_cam');
    $this->modelSize = $modelSize;
    $this->modelColor = $modelColor;

    $this->setImageType($headers, $imageType);
    $this->setOriginalUrl(
      [
        str_replace(' ', '_', $this->model),
        $this->view,
        $this->modelSize,
        $this->modelColor,
      ]
    );
  }

  /**
   * Generate image path based on SKU (itemId - configId)
   *
   * @param string $sku
   * @param array|null $headers
   * @param string|null $imageType
   * @param string|null $view
   */
  public function setSKU(string $sku, array $headers = null, string $imageType = null, string $view = null)
  {
    $this->view = $view ?: $this->container->get('def_cam');

    $this->setImageType($headers, $imageType);
    $this->setOriginalUrl(
      [
        $sku,
        $this->view,
      ]
    );
  }

  /**
   * Prepare path for default resized image
   *
   * @param string|null $imageWidth
   */
  public function prepareDefaultUrl(string $imageWidth = null)
  {
    $this->defaultUrl = $this->originalUrl.'/'.($imageWidth ?: self::DEF_WIDTH);
  }

  /**
   * Generate srcset for image
   *
   * @param array|null $sizes
   */
  public function prepareSrcset(array $sizes = null)
  {
    $this->srcset = [];
    foreach (($sizes ?: self::DEF_SRCSET) as $size) {
      $this->srcset[] = $this->originalUrl.'/'.$size.' '.$size.'w';
    }
  }

  /**
   * Function for object serialization (for JSON)
   *
   * @return $this
   */
  public function jsonSerialize()
  {
    return $this;
  }

  /**
   * Set image type
   *
   * @param array|null $headers
   * @param string|null $imageType
   */
  protected function setImageType(array $headers = null, string $imageType = null)
  {
    if ($headers) {
      $webpSupported = array_key_exists('Webp-Supported', $headers) ? filter_var(
        $headers('Webp-Supported')[0],
        FILTER_VALIDATE_BOOLEAN
      ) : null;
      $imageType = array_key_exists('Image-Type', $headers) ? $headers('Image-Type')[0] : null;
    }
    switch ($imageType) {
      case self::WEBP:
        $this->imageType = self::WEBP;
        break;
      case self::JPG:
      case self::JPEG:
        $this->imageType = self::JPG;
        break;
      case self::PNG:
      default:
        $this->imageType = self::PNG;
        break;
    }
  }

  /**
   * Set original url
   *
   * @param array $params
   */
  protected function setOriginalUrl(array $params)
  {
    $this->originalUrl = $this->cdnHost.trim(
        implode(
          '/',
          $params
        ),
        '/'
      ).'/'.$this->imageType;
  }
}