<?php

namespace B2B\Models\ExtendedAX;


use B2B\Classes\Traits\CastFromParent;
use B2B\Models\Image;
use B2B\Wsdl\B2BHelperTER\RadiatorConnectionsStruct_TER;

/** @SWG\Definition(
 *  definition="RadiatorConnections",
 *  type="object"
 * )
 */
class RadiatorConnections extends RadiatorConnectionsStruct_TER
{
  use CastFromParent;

  /**
   * @SWG\Property()
   * @var Image
   */
  protected $image;

  /**
   * @SWG\Property()
   * @var boolean
   */
  protected $configExists;

  /**
   * Set image
   *
   * @param Image $image
   */
  public function setImage(Image $image)
  {
    $this->image = $image;
  }

  /**
   * Get image
   *
   * @return Image
   */
  public function getImage(): Image
  {
    return $this->image;
  }

  /**
   * Set config exists
   *
   * @param bool $exists
   */
  public function setConfigExists(bool $exists) {
    $this->configExists = $exists;
  }

  /**
   * Get config exists
   *
   * @return bool
   */
  public function getConfigExists() {
    return $this->configExists;
  }

  /**
   *  Method to load attributes from parent class object.
   *
   * @param RadiatorConnectionsStruct_TER $obj
   */
  public function loadFromParentObj(RadiatorConnectionsStruct_TER $obj)
  {
    $this->castFromParent($obj);
  }
}
