<?php

namespace B2B\Models\ExtendedAX;

use B2B\Classes\Traits\CastFromParent;
use B2B\Models\Image;
use B2B\Wsdl\B2BHelperTER\RadiatorB2BStruct_TER;

/** @SWG\Definition(
 *  definition="RadiatorSize",
 *  type="object"
 * )
 */
class RadiatorSize extends RadiatorB2BStruct_TER
{
  use CastFromParent;

  /**
   * @SWG\Property()
   * @var Image
   */
  protected $image;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/Image")
   * )
   * @var Image[]
   */
  protected $images;

  /**
   * @SWG\Property()
   * @var boolean
   */
  protected $configExists;

  /**
   * Set image
   *
   * @param Image $image
   */
  public function setImage(Image $image)
  {
    $this->image = $image;
  }

  /**
   * Get image
   *
   * @return Image
   */
  public function getImage(): Image
  {
    return $this->image;
  }

  /**
   * Set images
   *
   * @param Image[] $images
   */
  public function setImages(array $images)
  {
    $this->images = $images;
  }

  /**
   * Get images
   *
   * @return Image[]
   */
  public function getImages(): array
  {
    return $this->images;
  }

  /**
   * Set config exists
   *
   * @param bool $exists
   */
  public function setConfigExists(bool $exists) {
    $this->configExists = $exists;
  }

  /**
   * Get config exists
   *
   * @return bool
   */
  public function getConfigExists() {
    return $this->configExists;
  }

  /**
   *  Method to load attributes from parent class object.
   *
   * @param RadiatorB2BStruct_TER $obj
   */
  public function loadFromParentObj(RadiatorB2BStruct_TER $obj)
  {
    $this->castFromParent($obj);
  }
}
