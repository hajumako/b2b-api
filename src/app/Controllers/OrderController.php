<?php
/**
 * @file
 * Contains class OrderController code implementation.
 */

namespace B2B\Controllers;

use B2B\Classes\Constants\AddressTypeInterface;
use B2B\Classes\Constants\DeliveryTypeInterface;
use B2B\Classes\Constants\OrderStatusInterface;
use B2B\Classes\Constants\PaymentMethodInterface;
use B2B\Classes\Exceptions\ApiException;
use B2B\Classes\Exceptions\UnauthorizedException;
use B2B\Classes\ObjectTranslator;
use B2B\Models\Address;
use B2B\Models\ExtendedAX\SalesLineContract;
use B2B\Models\ExtendedAX\SalesTableContract;
use B2B\Models\LineItem;
use B2B\Models\Order;
use B2B\Models\User;
use B2B\Wsdl\DtOutletServicesGroup\DTOutletAddress;
use B2B\Wsdl\DtOutletServicesGroup\DtOutletSalesOrderServiceSalesOrderCountRequest;
use B2B\Wsdl\DtOutletServicesGroup\DtOutletSalesOrderServiceSalesOrderLineListRequest;
use B2B\Wsdl\DtOutletServicesGroup\DtOutletSalesOrderServiceSalesOrderListRequest;
use B2B\Wsdl\PriceService\ArrayOfPriceCustomerRequest_TER;
use B2B\Wsdl\PriceService\PriceCustomerRequest_TER;
use B2B\Wsdl\PriceService\PriceService_TERGetCustomerPricesRequest;
use B2B\Wsdl\PriceService\RoutingService as PriceRoutingService;
use B2B\Wsdl\DtOutletServicesGroup\RoutingService as SalesRoutingService;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class OrderController extends Controller
{
  protected $tokenController;
  protected $orderModel;
  protected $userModel;
  protected $priceRoutingService;
  protected $salesRoutingService;

  /**
   * OrderController constructor.
   *
   * @param Container $container
   * @param TokenController $tokenController
   * @param Order $orderModel
   * @param User $userModel
   * @param PriceRoutingService $priceRoutingService
   */
  public function __construct(
    Container $container,
    TokenController $tokenController,
    Order $orderModel,
    User $userModel,
    PriceRoutingService $priceRoutingService,
    SalesRoutingService $salesRoutingService
  ) {
    $this->tokenController = $tokenController;
    $this->orderModel = $orderModel;
    $this->userModel = $userModel;
    $this->priceRoutingService = $priceRoutingService;
    $this->salesRoutingService = $salesRoutingService;
    parent::__construct($container);
  }

  /**
   * List all orders
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function listItems(Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $items = $this->orderModel->listItems($uid, $request);
      $response
        ->getBody()
        ->write(json_encode($items, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
      $this->total = $this->orderModel->getTotal();

      return parent::addHeaders('', $request, $response);
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * List all orders from AX
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function listAXItems(Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid !== null) {
      $account = $this->userModel->get($uid);
      if (!empty($account->axId)) {
        $offset = 0;
        $limit = 0;
        foreach ($request->getQueryParams() as $name => $value) {
          if ($name === 'offset') {
            $offset = $value;
          } elseif ($name === 'limit') {
            $limit = $value;
          }
        }
        $salesOrderListRequest = new DtOutletSalesOrderServiceSalesOrderListRequest($account->axId, $offset, $limit);
        $AXorders = [];
        foreach ($this->salesRoutingService->salesOrderList($salesOrderListRequest)->getResponse() as $salesListItem) {
          $AXorder = new SalesTableContract();
          $AXorder->loadFromParentObj($salesListItem);
          $AXorder->setParmTotalAmount(round($AXorder->getParmTotalAmount() * 100));
          $drupalOrder = $this->orderModel->getOrderByIdAX($uid, $salesListItem->getParmSalesId());
          $AXorder->setDrupalOrder($drupalOrder);
          $AXorders[] = $AXorder->jsonSerialize();
        }
        $salesOrderCountRequest = new DtOutletSalesOrderServiceSalesOrderCountRequest($account->axId);
        $this->total = $this->salesRoutingService->salesOrderCount($salesOrderCountRequest)->getResponse();
        $response
          ->getBody()
          ->write(json_encode($AXorders, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
      }

      return parent::addHeaders('', $request, $response);
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get order details
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function itemDetails(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $order = $this->orderModel->getOrder($uid, $orderId);
      if ($order) {
        $order->addresses = $this->orderModel->getAddresses($uid, $orderId);
        $order->lineItems = $this->orderModel->getLineItems($uid, $orderId);
        $order->deliveryType = $this->orderModel->getDeliveryType($uid, $orderId)->deliveryType;
        $order->paymentMethod = $this->orderModel->getPaymentMethod($uid, $orderId)->paymentMethod;
        $response
          ->getBody()
          ->write(json_encode($order, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
      }

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get order details from AX
   *
   * @param string $orderIdAX
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function AXItemDetails(string $orderIdAX, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $account = $this->userModel->get($uid);
      if (!empty($account->axId)) {
        $AXorder = new SalesTableContract();
        $drupalLineItems = [];
        $orderIdAXrev = str_replace('_', '/', $orderIdAX);
        $salesOrderListRequest = new DtOutletSalesOrderServiceSalesOrderListRequest($account->axId, 0, 0);
        foreach ($this->salesRoutingService->salesOrderList($salesOrderListRequest)->getResponse() as $salesListItem) {
          if ($salesListItem->getParmSalesId() === $orderIdAXrev) {
            $AXorder->loadFromParentObj($salesListItem);
            $AXorder->setParmTotalAmount(round($AXorder->getParmTotalAmount() * 100));
            $drupalOrder = $this->orderModel->getOrderByIdAX($uid, $salesListItem->getParmSalesId());
            if ($drupalOrder->orderId) {
              $drupalOrder->addresses = $this->orderModel->getAddresses($uid, $drupalOrder->orderId);
              $drupalLineItems = $this->orderModel->getLineItems($uid, $drupalOrder->orderId);
              $drupalOrder->deliveryType = $this->orderModel->getDeliveryType(
                $uid,
                $drupalOrder->orderId
              )->deliveryType;
              $drupalOrder->paymentMethod = $this->orderModel->getPaymentMethod(
                $uid,
                $drupalOrder->orderId
              )->paymentMethod;
            }
            $AXorder->setDrupalOrder($drupalOrder);
            break;
          }
        }
        foreach ($drupalLineItems as $drupalLineItem) {
          $drupalLineItems[$drupalLineItem->getSku()] = $drupalLineItem;
        }
        $salesOrderLineListRequest = new DtOutletSalesOrderServiceSalesOrderLineListRequest($orderIdAXrev);
        $orderLineList = $this->salesRoutingService->salesOrderLineList($salesOrderLineListRequest)->getResponse();
        $AXlineItems = [];
        foreach ($orderLineList as $orderLine) {
          $orderLine->setParmLineAmount(round($orderLine->getParmLineAmount() * 100));
          $orderLine->setParmSalesPrice(round($orderLine->getParmSalesPrice() * 100));
          $AXlineItem = new SalesLineContract();
          $AXlineItem->loadFromParentObj($orderLine);
          //todo replace when ws ready
          $AXlineItem->setSku($orderLine->getParmItemId() . '-' . $orderLine->getParmConfigId());
          if (array_key_exists($sku = $AXlineItem->getSku(), $drupalLineItems)) {
            $AXlineItem->setDrupalLineItem($drupalLineItems[$sku]);
          }
          $AXlineItems[] = $AXlineItem->jsonSerialize();
        }
        $AXorder->setSalesLineItems($AXlineItems);
        $response
          ->getBody()
          ->write(json_encode($AXorder->jsonSerialize(), $this->isDebugMode ? JSON_PRETTY_PRINT : null));
      }

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Create new order in database (used when item is added to cart)
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function create(Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $order = $this->orderModel->createOrder($uid);
      if ($order) {
        $response
          ->getBody()
          ->write(json_encode($order, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
      }

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get order with 'cart' status
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   * @throws ApiException
   */
  public function getCart(Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $order = $this->orderModel->getCart($uid);
      if (!empty($order->orderId)) {
        $order->lineItems = $this->orderModel->getLineItems($uid, $order->orderId);
        $order->lineItems = $this->getPrices($order->lineItems);
        $order->amount = $this->orderModel->setTotalAmount(
          $uid,
          $order->orderId,
          $this->orderModel->calculateTotalAmount($order->lineItems)
        )->amount;
        $order->addresses = $this->orderModel->getAddresses($uid, $order->orderId);
        $order->deliveryType = $this->orderModel->getDeliveryType($uid, $order->orderId)->deliveryType;
        $order->paymentMethod = $this->orderModel->getPaymentMethod($uid, $order->orderId)->paymentMethod;
        $response
          ->getBody()
          ->write(json_encode($order, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
      } else {
        $newOrder = $this->orderModel->createOrder($uid);
        $newOrder->lineItems = [];
        //get billing (invoice) address from AX and add to new order
        $account = $this->userModel->get($uid);
        if (!empty($account->axId)) {
          $axUser = $this->userModel->getAxUser($account->axId);
          foreach ($axUser->getAddresses()->getDTOutletAddress() as $address) {
            if ($address->getPrimary()) {
              $billingAddress = ObjectTranslator::translate($address, DTOutletAddress::class, Address::class);
              if ($billingAddress !== null) {
                $billingAddress->organisationName = $axUser->getName();
                $newOrder->addresses = $this->orderModel->setAddresses($uid, $newOrder->orderId, [$billingAddress]);
                break;
              }
            }
          }
        }
        $response
          ->getBody()
          ->write(json_encode($newOrder, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
      }

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get order status
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getStatus(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $status = $this->orderModel->getOrderStatus($uid, $orderId);
      $response
        ->getBody()
        ->write(json_encode($status, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Set order status
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function setStatus(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $body = $request->getParsedBody();
      if (array_key_exists('status', $body)) {
        $status = $this->orderModel->getOrderStatus($uid, $orderId);
        if ($body['status'] === OrderStatusInterface::SAVED && $status->status === OrderStatusInterface::CHECKOUT_REVIEW) {
          $payment = $this->orderModel->getPaymentMethod($uid, $orderId);
          $payment->amount = $this->orderModel->getCart($uid)->amount;
          $payment = $this->orderModel->updatePayment($uid, $orderId, $payment);
          if ($payment->paymentMethod === PaymentMethodInterface::ON_DELIVERY) {
            //on delivery payment
            //send order to AX
            if ($this->orderModel->axSaveOrder($uid, $orderId)) {
              //on success
              $this->orderModel->setOrderStatus($uid, $orderId, OrderStatusInterface::INVOICED);
            } else {
              //on failure
              $this->orderModel->setOrderStatus($uid, $orderId, OrderStatusInterface::AX_PROCESSING);
            }
          } else {
            //payU payment
            $this->orderModel->setOrderStatus($uid, $orderId, OrderStatusInterface::SAVED);
          }
        } else {
          //other status
          $this->orderModel->setOrderStatus($uid, $orderId, $body['status']);
        }
      }
      $orderStatus = $this->orderModel->getOrderStatus($uid, $orderId);
      $response->getBody()
        ->write(json_encode($orderStatus, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get order's addresses
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getAddresses(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $addresses = $this->orderModel->getAddresses($uid, $orderId);
      $response
        ->getBody()
        ->write(json_encode($addresses, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Save order's addresses & add new ones to AX
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function setAddresses(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $body = $request->getParsedBody();
      $addresses = $this->orderModel->setAddresses($uid, $orderId, $body);
      $response
        ->getBody()
        ->write(json_encode($addresses, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get order's line items
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getLineItems(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $items = $this->orderModel->getLineItems($uid, $orderId);
      $response
        ->getBody()
        ->write(json_encode($items, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Add/update/remove order's line items
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   * @throws ApiException
   */
  public function setLineItems(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $lineItems = $request->getParsedBody();
      $lineItems = $this->getPrices($lineItems);
      $items = $this->orderModel->setLineItems($uid, $orderId, $lineItems);
      $response
        ->getBody()
        ->write(json_encode($items, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get AX order's line items
   *
   * @param string $orderIdAX
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getAXLineItems(string $orderIdAX, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $salesOrderLineListRequest = new DtOutletSalesOrderServiceSalesOrderLineListRequest(
        str_replace('_', '/', $orderIdAX)
      );
      $orderLineList = $this->salesRoutingService->salesOrderLineList($salesOrderLineListRequest)->getResponse();
      $AXlineItems = [];
      foreach ($orderLineList as $orderLine) {
        $orderLine->setParmLineAmount(round($orderLine->getParmLineAmount() * 100));
        $orderLine->setParmSalesPrice(round($orderLine->getParmSalesPrice() * 100));
        $AXlineItem = new SalesLineContract();
        $AXlineItem->loadFromParentObj($orderLine);
        //todo replace when ws ready
        $AXlineItem->setSku($orderLine->getParmItemId() . '-' . $orderLine->getParmConfigId());
        $AXlineItems[] = $AXlineItem->jsonSerialize();
      }
      $response
        ->getBody()
        ->write(json_encode($AXlineItems, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get order's delivery type
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getDeliveryType(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $delivery = $this->orderModel->getDeliveryType($uid, $orderId);
      $response
        ->getBody()
        ->write(json_encode($delivery, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Set order's delivery type
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function setDeliveryType(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $body = $request->getParsedBody();
      $delivery = $this->orderModel->setDeliveryType($uid, $orderId, $body['deliveryType']);
      $response
        ->getBody()
        ->write(json_encode($delivery, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get order's payment method
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getPaymentMethod(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $payment = $this->orderModel->getPaymentMethod($uid, $orderId);
      $response
        ->getBody()
        ->write(json_encode($payment, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Set order's payment method
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function setPaymentMethod(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $body = $request->getParsedBody();
      $payment = $this->orderModel->setPaymentMethod(
        $uid,
        $orderId,
        $body['paymentMethod'],
        $body['transactionId'] ?? 0
      );
      $response
        ->getBody()
        ->write(json_encode($payment, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get order's additional data
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getAdditionalData(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $data = $this->orderModel->getAdditionalData($uid, $orderId);
      $response
        ->getBody()
        ->write(json_encode($data, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Set order's additional data
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function setAdditionalData(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $body = $request->getParsedBody();
      $data = $this->orderModel->setAdditionalData(
        $uid,
        $orderId,
        $body
      );
      $response
        ->getBody()
        ->write(json_encode($data, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Fill order with the default data (delivery & invoice address, delivery type, payment method)
   *
   * @param int $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   * @throws ApiException
   */
  public function fillOrderWithDefaultData(int $orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $order = $this->orderModel->getOrder($uid, $orderId);
      if (!empty($order->orderId)) {
        $account = $this->userModel->get($uid);
        if (!empty($account->axId)) {
          //set addresses - based on primary address from AX
          $axUser = $this->userModel->getAxUser($account->axId);
          foreach ($axUser->getAddresses()->getDTOutletAddress() as $address) {
            if ($address->getPrimary()) {
              $primaryAddress = ObjectTranslator::translate($address, DTOutletAddress::class, Address::class);
              if ($primaryAddress !== null) {
                $primaryAddress->organisationName = $axUser->getName();
                break;
              }
            }
          }
          if (!empty($primaryAddress)) {
            $deliveryAddress = clone $primaryAddress;
            $deliveryAddress->type = AddressTypeInterface::SHIPPING;
            $invoiceAddress = clone $primaryAddress;
            $invoiceAddress->type = AddressTypeInterface::BILLING;
            foreach ($this->orderModel->getAddresses($uid, $orderId) as $address) {
              if ($address->type === AddressTypeInterface::SHIPPING) {
                $deliveryAddress->profileId = $address->profileId;
              } elseif ($address->type === AddressTypeInterface::BILLING) {
                $invoiceAddress->profileId = $address->profileId;
              }
            }
            $order->addresses = $this->orderModel->setAddresses(
              $uid,
              $order->orderId,
              [$deliveryAddress, $invoiceAddress]
            );
          }
          //get line items
          $order->lineItems = $this->orderModel->getLineItems($uid, $orderId);
          //get line items prices
          $order->lineItems = $this->getPrices($order->lineItems);
          //set delivery type 'Kurier'
          $order->deliveryType = $this->orderModel->setDeliveryType(
            $uid,
            $order->orderId,
            //TODO needs to be saved in db and/or AX
            DeliveryTypeInterface::COURIER_PAID
          )->deliveryType;
          //set payment method 'PayU'
          $order->paymentMethod = $this->orderModel->setPaymentMethod(
            $uid,
            $order->orderId,
            //TODO needs to be saved in db and/or AX
            PaymentMethodInterface::PAYU
          )->paymentMethod;
          $response
            ->getBody()
            ->write(json_encode($order, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

          return $response;
        }
        throw new UnauthorizedException('null $axId in ' . __METHOD__);
      }
      throw new UnauthorizedException('order does not exist ' . __METHOD__);
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Get prices for lineitems from AX
   *
   * @param LineItem[] $lineItems
   *
   * @return LineItem[]
   * @throws ApiException
   */
  protected function getPrices(array $lineItems): array
  {
    $user = $this->userModel->get($this->tokenController->getUserId());
    $axId = $user->axId;
    if ($axId != null) {
      $lineItemPos = [];
      $priceRequests = [];
      foreach ($lineItems as $i => $lineItem) {
        if (!$lineItem instanceof LineItem) {
          $data = $lineItem;
          $lineItem = new LineItem($this->container);
          $lineItem->loadFromArray($data);
        }
        $lineItemPos[$lineItem->getSku()] = $i;
        $priceCustomerRequestTER = new PriceCustomerRequest_TER();
        $priceCustomerRequestTER->setCustAccount($axId);
        $priceCustomerRequestTER->setItemId($lineItem->getItemIdAX());
        $priceCustomerRequestTER->setConfigId($lineItem->getConfigIdAX());
        $priceCustomerRequestTER->setCalculateLineAmount(true);
        //TODO for now currency is fixed
        $priceCustomerRequestTER->setCurrency('PLN');
        $priceRequests[] = $priceCustomerRequestTER;
      }
      if (count($priceRequests)) {
        $arrayOfPriceCustomerRequestTER = new ArrayOfPriceCustomerRequest_TER();
        $arrayOfPriceCustomerRequestTER->setPriceCustomerRequest_TER($priceRequests);
        $params = new PriceService_TERGetCustomerPricesRequest($arrayOfPriceCustomerRequestTER);
        $axPriceResponse = $this->priceRoutingService->GetCustomerPrices($params)->getResponse();
        foreach ($axPriceResponse as $axPrice) {
          $pos = $lineItemPos[$axPrice->getItemId() . '-' . $axPrice->getConfigId()];
          $lineItems[$pos]->amount = round($axPrice->getLineAmount() * 100);
        }
      }

      return $lineItems;
    }
    throw new ApiException('missing $axId in ' . __METHOD__);
  }
}
