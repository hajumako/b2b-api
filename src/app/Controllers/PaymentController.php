<?php
/**
 * @file
 * Contains class PaymentController code implementation.
 */

namespace B2B\Controllers;


use B2B\Classes\Exceptions\ApiException;
use B2B\Classes\Exceptions\UnauthorizedException;
use B2B\Classes\Helpers\DrupalCommercePayment;
use B2B\Classes\Helpers\MongoDb;
use B2B\Classes\Constants\OrderStatusInterface;
use B2B\Classes\Constants\PaymentMethodInterface;
use B2B\Models\Order;
use B2B\Models\PaymentGateway;
use B2B\Models\User;
use Di\Container;
use OpenPayU_Configuration;
use OpenPayU_Order;
use OpenPayU_Exception;
use OpenPayuOrderStatus;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class PaymentController
 *
 * @package B2B\Controllers
 */
class PaymentController extends Controller
{
  protected $config;
  protected $order;
  protected $tokenController;
  protected $orderModel;
  protected $userModel;

  public function __construct(
    Container $container,
    TokenController $tokenController,
    Order $orderModel,
    User $userModel
  ) {
    $this->config = $container->get('payu');
    $this->tokenController = $tokenController;
    $this->orderModel = $orderModel;
    $this->userModel = $userModel;
    parent::__construct($container);
  }

  /**
   * Get order's payment details
   *
   * @param $orderId
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function getOrder($orderId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $payment = $this->orderModel->getPayment($uid, $orderId);
      $response
        ->getBody()
        ->write(json_encode($payment, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }

  /**
   * Method to set order in payment gateway.
   * TODO: add parameter to determine payment gateway (integration with paypal in future?).
   *
   * @param int $orderId
   *   Order identifier.
   * @param Request $request
   *   The request object.
   * @param Response $response
   *   The response object.
   *
   * @return Response
   * @throws \Exception
   */
  public function setOrder($orderId, Request $request, Response $response): Response
  {
    $order = [];
    // Set PayU configuration from env.
    $this->loadPayuConfiguration();

    $uid = $this->tokenController->getUserId();
    $commerceOrder = $this->orderModel->getOrder($uid, $orderId);
    $transaction = $this->orderModel->getPaymentMethod($uid, $orderId);
    if (isset($commerceOrder->status) && $commerceOrder->status === OrderStatusInterface::SAVED &&
      !empty($transaction->paymentMethod) && $transaction->paymentMethod === PaymentMethodInterface::PAYU) {
      // Set basic order information.
      $order['continueUrl'] = $this->config['continueUrl'];
      $order['notifyUrl'] = $this->config['notifyUrl'];
      $order['customerIp'] = $_SERVER['REMOTE_ADDR'];
      $order['merchantPosId'] = OpenPayU_Configuration::getMerchantPosId();
      $order['description'] = 'Zamówienie numer: ' . $commerceOrder->orderId;
      $order['currencyCode'] = $transaction->currencyCode;
      //$order['totalAmount'] = $transaction->amount;
      $order['extOrderId'] = $commerceOrder->orderId . '_' . $uid;
      // Set line items.
      $lineItems = $this->orderModel->getLineItems($uid, $orderId);
      //TODO fake value set - payu doesn't accept 0
      $order['totalAmount'] = round(count($lineItems) * 100);
      if (empty($lineItems)) {
        throw new ApiException('Line items of order cannot be empty.');
      }
      $order['products'] = [];
      foreach ($lineItems as $lineItem) {
        $order['products'][] = [
          'name' => $lineItem->sku,
          //TODO fake value set - payu doesn't accept 0
          'unitPrice' => $lineItem->amount ?: 100,
          'quantity' => (int)$lineItem->quantity,
        ];
      }

      // Try to create order in PayU.
      $paymentResponse = OpenPayU_Order::create($order);
      $paymentObject = new PaymentGateway();
      $paymentObject->redirectUri = $paymentResponse->getResponse()->redirectUri;
      $response->getBody()->write(json_encode($paymentObject, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
      // Set 'pending' status for drupal commerce local payment transaction.
      $payment = $this->orderModel->getPaymentMethod($uid, $orderId);
      $payment->status = DrupalCommercePayment::PAYMENT_TRANSACTION_PENDING;
      $this->orderModel->updatePayment($uid, $orderId, $payment);
      // Order is saved successfully in PayU - set pending status that mean notify endpoint is waiting for payment.
      $this->orderModel->setOrderStatus($uid, $orderId, OrderStatusInterface::PENDING);
    } else {
      throw new ApiException('Wrong order status or payment method.');
    }

    return $response;
  }

  /**
   * Method to retrieve status notifications from PayU.
   *
   * @param Request $request
   *   Request object.
   *
   * @param Response $response
   *   Response object.
   */
  public function consumeNotify(Request $request, Response $response)
  {
    $this->loadPayuConfiguration();
    $body = json_encode($request->getParsedBody(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    $data = trim($body);

    try {
      if (!empty($data)) {
        $result = OpenPayU_Order::consumeNotification($data);
        $status = $result->getResponse()->order->status;
        $payuOrderId = $result->getResponse()->order->orderId;
        // Split order identifier to get Drupal order identifier and user identifier.
        $identifiers = explode('_', $result->getResponse()->order->extOrderId);
        $orderId = (int)$identifiers[0] ?? null;
        $uid = (int)$identifiers[1] ?? null;
        if (empty($orderId) || empty($uid)) {
          // There missing required data in extOrderId parameter, ignore current notification.
          $status = '';
        }

        switch ($status) {
          case OpenPayuOrderStatus::STATUS_COMPLETED:
            if (isset($payuOrderId) && isset($orderId)) {
              $order = OpenPayU_Order::retrieve($payuOrderId);
              if ($order->getStatus() === OpenPayU_Order::SUCCESS) {
                // Save PayU order to mongoDb.
                $this->orderModel->setCollectionDoc(
                  MongoDb::COLLECTION_PAYMENTS_PAYU,
                  [
                    'uid' => $uid,
                    'orderId' => $orderId,
                    'order' => $order,
                    'created' => time(),
                  ]
                );
                $orderStatus = $this->orderModel->getOrderStatus($uid, $orderId);
                if ($orderStatus->status === OrderStatusInterface::PENDING) {
                  $axOrder = $this->orderModel->axSaveOrder($uid, $orderId);
                  if ($axOrder) {
                    $this->orderModel->setOrderStatus($uid, $orderId, OrderStatusInterface::INVOICED);
                  } else {
                    $this->orderModel->setOrderStatus($uid, $orderId, OrderStatusInterface::AX_PROCESSING);
                  }
                  $payment = $this->orderModel->getPaymentMethod($uid, $orderId);
                  // Set remote payment identifier.
                  if (isset($result->getResponse()->properties[0]->name) && $result->getResponse(
                    )->properties[0]->name === 'PAYMENT_ID') {
                    $payment->remoteId = $result->getResponse()->properties[0]->value;
                  }
                  // Set 'success' status for drupal commerce local payment transaction.
                  $payment->status = DrupalCommercePayment::PAYMENT_TRANSACTION_SUCCESS;
                  $this->orderModel->updatePayment($uid, $orderId, $payment);
                }
                $response = new \Slim\Http\Response();
                $response->withStatus(200);

                return $response;
              }
            }
            break;

          case OpenPayuOrderStatus::STATUS_CANCELED:
          case OpenPayuOrderStatus::STATUS_REJECTED:
            $this->orderModel->setOrderStatus($uid, $orderId, OrderStatusInterface::PAYMENT_PROCESSING);
            $payment = $this->orderModel->getPaymentMethod($uid, $orderId);
            // Set 'failure' status for drupal commerce local payment transaction.
            $payment->status = DrupalCommercePayment::PAYMENT_TRANSACTION_FAILURE;
            $this->orderModel->updatePayment($uid, $orderId, $payment);
            break;

          default:
            // Do nothing if status is empty or one of those: NEW, PENDING, WAITING_FOR_CONFIRMATION.
            break;
        }
      }
    } catch (OpenPayU_Exception $e) {
      $this->logger->addInfo(
        'Error during retrieving notification: ' . $e->getMessage()
      );
    }
    //TODO add return
  }

  /**
   * Load PayU configuration.
   */
  protected function loadPayuConfiguration()
  {
    if (empty($this->config)) {
      throw new ApiException('Payment configuration was not found.');
    }
    OpenPayU_Configuration::setEnvironment($this->config['environment']);
    OpenPayU_Configuration::setMerchantPosId($this->config['pos_id']);
    OpenPayU_Configuration::setSignatureKey($this->config['md5']);
    OpenPayU_Configuration::setOauthClientId($this->config['pos_id']);
    OpenPayU_Configuration::setOauthClientSecret($this->config['client_secret']);
  }

}
