<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 08.02.2018
 * Time: 14:51
 */

//colors

/** @SWG\Get(
 *    path="/configurator/colors/{itemId}/{connectionId}",
 *    description="List of all available colors for specific product",
 *    summary="Get list of all available colors for specific product",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/connectionId"),
 *    @SWG\Response(
 *      response=200,
 *      description="configurator colors response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/ConfiguratorColors")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/colors/{itemId}/{connectionId}', ['\B2B\Controllers\ConfiguratorController', 'getColorsByItemId']);

/** @SWG\Get(
 *    path="/configurator/colors/short/{itemId}/{connectionId}",
 *    description="Get color ids of available colors for specific product",
 *    summary="Get color ids of available colors for specific product",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/connectionId"),
 *    @SWG\Response(
 *      response=200,
 *      description="configurator colors short response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(type="string")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/colors/short/{itemId}/{connectionId}', ['\B2B\Controllers\ConfiguratorController', 'getColorsByItemIdShort']);

/** @SWG\Get(
 *    path="/configurator/colors/ingroups/{itemId}/{connectionId}",
 *    description="List of all available colors for specific product grouped by color range",
 *    summary="Get list of all available colors for specific product grouped by color range",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/connectionId"),
 *    @SWG\Response(
 *      response=200,
 *      description="configurator colors grouped by color range response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/ColorGroup")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/colors/ingroups/{itemId}/{connectionId}', ['\B2B\Controllers\ConfiguratorController', 'getColorsByItemIdInGroups']);

//get config parts

//radiator
/** @SWG\Get(
 *    path="/configurator/models/grouped",
 *    description="List models grouped by type (WG + WL)",
 *    summary="Get list of models grouped by type (WG + WL)",
 *    produces={"application/json"},
 *    @SWG\Response(
 *      response=200,
 *      description="models response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/GroupedModels")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/models/grouped', ['\B2B\Controllers\ConfiguratorController', 'getModelsGrouped']);

/** @SWG\Get(
 *    path="/configurator/models/{groupId}",
 *    description="List of all available models (products) for specified group",
 *    summary="Get list of all available models (products) for specified group",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/groupId"),
 *    @SWG\Response(
 *      response=200,
 *      description="models response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/RadiatorModel")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/models/{groupId}', ['\B2B\Controllers\ConfiguratorController', 'getModels']);

/** @SWG\Get(
 *    path="/configurator/sizes/{model}",
 *    description="List of all available sizes for specific model (product)",
 *    summary="Get list of all available sizes for specific model (product)",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/model"),
 *    @SWG\Response(
 *      response=200,
 *      description="sizes response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/RadiatorSize")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/sizes/{model}', ['\B2B\Controllers\ConfiguratorController', 'getSizes']);

/** @SWG\Get(
 *    path="/configurator/connections/{itemId}",
 *    description="List of all available connections for specific product",
 *    summary="Get list of all available connections for specific product",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Response(
 *      response=200,
 *      description="connections response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/RadiatorConnections")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/connections/{itemId}', ['\B2B\Controllers\ConfiguratorController', 'getConnections']);

/** @SWG\Get(
 *    path="/configurator/coatings/{itemId}",
 *    description="List of available coatings for specific product",
 *    summary="Get list of available coatings for specific product",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Response(
 *      response=200,
 *      description="coatings response",
 *      @SWG\Schema(ref="#/definitions/RadiatorCoatingsStruct_TER"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/coatings/{itemId}', ['\B2B\Controllers\ConfiguratorController', 'getCoatings']);

//heater
/** @SWG\Get(
 *    path="/configurator/radiator/heaters/{itemId}/{configId}",
 *    description="List of available heaters (itemId) for specific radiator configuration",
 *    summary="Get list of available heaters (itemId) for specific radiator configuration",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/configId"),
 *    @SWG\Response(
 *      response=200,
 *      description="coatings response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/HeaterModel")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/radiator/heaters/{itemId}/{configId}', ['\B2B\Controllers\ConfiguratorController', 'getRadiatorHeaters']);

/** @SWG\Get(
 *    path="/configurator/radiator/heaters/{itemId}/{configId}/{heaterItemId}",
 *    description="List of available heater configs (configId) for specific radiator configuration & heater",
 *    summary="Get list of available heater configs (configId) for specific radiator configuration & heater",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/configId"),
 *    @SWG\Parameter(ref="#/parameters/heaterItemId"),
 *    @SWG\Response(
 *      response=200,
 *      description="coatings response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/HeaterConfigs")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/radiator/heaters/{itemId}/{configId}/{heaterItemId}', ['\B2B\Controllers\ConfiguratorController', 'getRadiatorHeatersConfigs']);


//check config part

/** @SWG\Get(
 *    path="/configurator/check/model/{model}",
 *    description="Check if model exists",
 *    summary="Check if model exists",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/model"),
 *    @SWG\Response(
 *      response=200,
 *      description="models response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/RadiatorModel")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/check/model/{model}', ['\B2B\Controllers\ConfiguratorController', 'checkModel']);

/** @SWG\Get(
 *    path="/configurator/check/size/{itemId}",
 *    description="Check if size exists for given model",
 *    summary="Check if size exists for given model (check if item id is valid)",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Response(
 *      response=200,
 *      description="sizes response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/RadiatorSize")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/check/size/{itemId}', ['\B2B\Controllers\ConfiguratorController', 'checkSize']);

/** @SWG\Get(
 *    path="/configurator/check/connection/{itemId}/{connectionId}",
 *    description="Check if connection is possible for given product",
 *    summary="Check if connection is possible for given product",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/connectionId"),
 *    @SWG\Response(
 *      response=200,
 *      description="connections response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/RadiatorConnections")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/check/connection/{itemId}/{connectionId}', ['\B2B\Controllers\ConfiguratorController', 'checkConnection']);

/** @SWG\Get(
 *    path="/configurator/check/color/{itemId}/{colorId}",
 *    description="Check if color is possible for given product",
 *    summary="Check if color is possible for given product",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/colorId"),
 *    @SWG\Response(
 *      response=200,
 *      description="radiator colors response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/RadiatorColors")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/check/color/{itemId}/{colorId}', ['\B2B\Controllers\ConfiguratorController', 'checkColor']);

//prices

/** @SWG\Get(
 *    path="/configurator/price/get/{sku}",
 *    description="Prices for specific product",
 *    summary="Total price amount for configuration of product",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/sku"),
 *    @SWG\Response(
 *      response=200,
 *      description="prices response",
 *      @SWG\Schema(ref="#/definitions/PriceCustomerResponse_TER"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/price/get/{sku}', ['\B2B\Controllers\ConfiguratorController', 'getPriceBySku']);

/** @SWG\Get(
 *    path="/configurator/price/get/{itemId}/{configId}",
 *    description="Prices for specific product",
 *    summary="Total price amount for configuration of product",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/configId"),
 *    @SWG\Response(
 *      response=200,
 *      description="prices response",
 *      @SWG\Schema(ref="#/definitions/PriceCustomerResponse_TER"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/price/get/{itemId}/{configId}', ['\B2B\Controllers\ConfiguratorController', 'getPriceByParts']);

/** @SWG\Post(
 *    path="/configurator/prices/get",
 *    description="Prices for list of products",
 *    summary="Total price amount for list of products",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/skus"),
 *    @SWG\Response(
 *      response=200,
 *      description="prices response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/PriceCustomerResponse_TER")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/configurator/prices/get', ['\B2B\Controllers\ConfiguratorController', 'getPrices']);

//configs

/** @SWG\Get(
 *    path="/configurator/config/get/{sku}",
 *    description="Get configuration data from database",
 *    summary="Get configuration data",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/sku"),
 *    @SWG\Response(
 *      response=200,
 *      description="config response",
 *      @SWG\Schema(
 *        ref="#/definitions/JsonObj"
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/config/get/{sku}', ['\B2B\Controllers\ConfiguratorController', 'getConfigBySku']);

/** @SWG\Get(
 *    path="/configurator/config/get/{itemId}/{configId}",
 *    description="Get configuration data from database",
 *    summary="Get configuration data",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/configId"),
 *    @SWG\Response(
 *      response=200,
 *      description="config response",
 *      @SWG\Schema(
 *        ref="#/definitions/JsonObj"
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/configurator/config/get/{itemId}/{configId}', ['\B2B\Controllers\ConfiguratorController', 'getConfigByParts']);

/** @SWG\Post(
 *    path="/configurator/configs/get",
 *    description="Configs for list of products",
 *    summary="Products' configurations for list of products",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/skus"),
 *    @SWG\Response(
 *      response=200,
 *      description="prices response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/JsonObj")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/configurator/configs/get', ['\B2B\Controllers\ConfiguratorController', 'getConfigs']);

/** @SWG\Post(
 *    path="/configurator/config/set/{sku}",
 *    description="Save configuration data in database",
 *    summary="Save configuration data",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/sku"),
 *    @SWG\Parameter(ref="#/parameters/jsonObj"),
 *    @SWG\Response(
 *      response=200,
 *      description="config response",
 *      @SWG\Schema(
 *        ref="#/definitions/JsonObj"
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/configurator/config/set/{sku}', ['\B2B\Controllers\ConfiguratorController', 'setConfig']);