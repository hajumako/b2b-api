<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 30.08.2017
 * Time: 12:23
 */

namespace B2B\Classes\Constants;

/**
 * @SWG\Definition(
 *   definition="OrderStatusInterface",
 *   type="string",
 *   enum={"b2b_cart","b2b_checkout_shipping","b2b_checkout_payment","b2b_checkout_review","b2b_saved","pending","payment_processing","ax_processing","invoiced","completed"},
 *   default="b2b_cart"
 * )
 */
interface OrderStatusInterface
{
  const CART = 'b2b_cart';
  const CHECKOUT_SHIPPING = 'b2b_checkout_shipping';
  const CHECKOUT_PAYMENT = 'b2b_checkout_payment';
  const CHECKOUT_REVIEW = 'b2b_checkout_review';
  const SAVED = 'b2b_saved';
  const PENDING = 'pending'; // Order was created in payment gateway.
  const PAYMENT_PROCESSING = 'payment_processing'; // Payment not pass.
  const AX_PROCESSING = 'ax_processing'; // Something went wrong during saving order to AX.
  const INVOICED = 'invoiced'; // Payment was passed and order was saved to AX with successfully.
  const COMPLETED = 'completed'; // Order delivered to the customer.
}
