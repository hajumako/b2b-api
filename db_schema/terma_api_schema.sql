-- --------------------------------------------------------
-- Host:                         127.0.0.13
-- Server version:               5.6.39 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for view db.api_accessories
DROP VIEW IF EXISTS `api_accessories`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_accessories` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`modelNameLoc` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`modelAX` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`language` VARCHAR(32) NULL COMMENT 'The language for this data item.' COLLATE 'utf8_general_ci',
	`lead` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`created` INT(11) NOT NULL COMMENT 'The Unix timestamp when the node was created.',
	`changed` INT(11) NOT NULL COMMENT 'The Unix timestamp when the node was most recently saved.',
	`gallery` TEXT NULL COLLATE 'utf8_bin'
) ENGINE=MyISAM;

-- Dumping structure for view db.api_accessories_stock
DROP VIEW IF EXISTS `api_accessories_stock`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_accessories_stock` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`productId` INT(10) UNSIGNED NULL,
	`sku` VARCHAR(255) NULL COMMENT 'The unique, human-readable identifier for a product.' COLLATE 'utf8_general_ci',
	`configurationName` VARCHAR(255) NULL COMMENT 'The title of this product, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`quantity` DECIMAL(10,2) NULL,
	`price` INT(11) NULL COMMENT 'The price amount.',
	`priceRrp` INT(11) NULL COMMENT 'The price amount.',
	`sale` INT(11) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db.api_accessories_stock_models
DROP VIEW IF EXISTS `api_accessories_stock_models`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_accessories_stock_models` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view db.api_all_stock
DROP VIEW IF EXISTS `api_all_stock`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_all_stock` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`productId` INT(10) UNSIGNED NULL,
	`sku` VARCHAR(255) NULL COMMENT 'The unique, human-readable identifier for a product.' COLLATE 'utf8_general_ci',
	`configurationName` VARCHAR(255) NULL COMMENT 'The title of this product, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`quantity` DECIMAL(10,2) NULL,
	`price` INT(11) NULL COMMENT 'The price amount.',
	`priceRrp` INT(11) NULL COMMENT 'The price amount.',
	`sale` INT(11) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db.api_all_stock_models
DROP VIEW IF EXISTS `api_all_stock_models`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_all_stock_models` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view db.api_colors
DROP VIEW IF EXISTS `api_colors`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_colors` (
	`id` INT(10) UNSIGNED NOT NULL COMMENT 'The entity id this data is attached to',
	`colorId` VARCHAR(6) NULL COLLATE 'utf8_general_ci',
	`name` VARCHAR(255) NULL COMMENT 'The term name.' COLLATE 'utf8_general_ci',
	`type` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`hex` VARCHAR(6) NULL COLLATE 'utf8_general_ci',
	`rgb` VARCHAR(11) NULL COLLATE 'utf8_general_ci',
	`img` VARCHAR(255) NULL COMMENT 'The URI to access the file (either local or remote).' COLLATE 'utf8_bin',
	`height` INT(10) UNSIGNED NULL COMMENT 'The height of the image in pixels.',
	`width` INT(10) UNSIGNED NULL COMMENT 'The width of the image in pixels.'
) ENGINE=MyISAM;

-- Dumping structure for procedure db.api_create_order
DROP PROCEDURE IF EXISTS `api_create_order`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_create_order`(
	IN `uid` INT

,
	IN `status` VARCHAR(255),
	OUT `orderId` INT
)
BEGIN
DECLARE _mail VARCHAR(255);
DECLARE _date, _revision_id, _order_id INT DEFAULT 0;
SET _date = UNIX_TIMESTAMP();
SELECT U.`mail` into _mail FROM shared_users AS U WHERE U.`uid` = uid;
INSERT INTO shared_commerce_commerce_order_revision (`mail`, `status`, `log`, `revision_timestamp`) VALUES (_mail, status, 'Utworzono jako zamówienie koszyka.', _date);
SET _revision_id = LAST_INSERT_ID();
INSERT INTO shared_commerce_commerce_order (`revision_id`, `type`, `uid`, `mail`, `status`, `created`, `changed`) VALUES (_revision_id, 'commerce_order', uid, _mail, status, _date, _date);
SET _order_id = LAST_INSERT_ID();
UPDATE shared_commerce_commerce_order_revision SET `order_id` = _order_id, `order_number` = _order_id WHERE `revision_id` = _revision_id;
UPDATE shared_commerce_commerce_order SET `order_number` = _order_id WHERE `order_id` = _order_id;

SET orderId = _order_id;
END//
DELIMITER ;

-- Dumping structure for table db.api_field_data_field_ax_address_id
DROP TABLE IF EXISTS `api_field_data_field_ax_address_id`;
CREATE TABLE IF NOT EXISTS `api_field_data_field_ax_address_id` (
  `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
  `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
  `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
  `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
  `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
  `field_ax_address_id_value` varchar(255) DEFAULT NULL,
  `field_ax_address_id_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_ax_address_id_format` (`field_ax_address_id_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Data storage for field 327 (field_ax_address_id)';

-- Data exporting was unselected.
-- Dumping structure for table db.api_field_data_field_order_additional_data
DROP TABLE IF EXISTS `api_field_data_field_order_additional_data`;
CREATE TABLE IF NOT EXISTS `api_field_data_field_order_additional_data` (
  `entity_type` varchar(128) NOT NULL DEFAULT '',
  `bundle` varchar(128) NOT NULL DEFAULT '',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `entity_id` int(10) unsigned NOT NULL,
  `revision_id` int(10) unsigned DEFAULT NULL,
  `language` varchar(32) NOT NULL DEFAULT '',
  `delta` int(10) unsigned NOT NULL,
  `field_order_additional_data_value` varchar(255) DEFAULT NULL,
  `field_order_additional_data_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_type`,`entity_id`,`deleted`,`delta`,`language`),
  KEY `entity_type` (`entity_type`),
  KEY `bundle` (`bundle`),
  KEY `deleted` (`deleted`),
  KEY `entity_id` (`entity_id`),
  KEY `revision_id` (`revision_id`),
  KEY `language` (`language`),
  KEY `field_order_additional_data_format` (`field_order_additional_data_format`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for procedure db.api_get_accessory_details
DROP PROCEDURE IF EXISTS `api_get_accessory_details`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_get_accessory_details`(
	IN `nid` INT,
	IN `language` CHAR(50)



)
BEGIN
SELECT
N.nid, 
N.title AS 'modelName', 
F_TITL.title_field_value AS 'modelNameLoc',
T_GROU.name AS 'group',
F_LEAD.`language`,
F_LEAD.field_lead_value AS 'lead',
N.created,
N.changed,
F_DESC1.field_description_value AS 'description',
F_DESC2.field_description2_value AS 'description2',
GROUP_CONCAT(DISTINCT FI_GALL.uri SEPARATOR ';') AS 'gallery',
GROUP_CONCAT(DISTINCT FI_SKET.uri SEPARATOR ';') AS 'sketches',
F_VERS.field_versiontable_value AS 'versions'
FROM theat_node AS N 
LEFT JOIN theat_field_data_field_lead AS F_LEAD ON F_LEAD.entity_id = N.nid

LEFT JOIN theat_field_data_title_field AS F_TITL ON (F_TITL.entity_id = N.nid AND F_TITL.`language` = F_LEAD.`language`)

LEFT JOIN theat_field_data_field_description AS F_DESC1 ON (F_DESC1.entity_id = N.nid AND F_DESC1.`language` = F_LEAD.`language`)

LEFT JOIN theat_field_data_field_description2 AS F_DESC2 ON (F_DESC2.entity_id = N.nid AND F_DESC2.`language` = F_LEAD.`language`)

LEFT JOIN theat_field_data_field_gallery AS F_GALL ON F_GALL.entity_id = N.nid
LEFT JOIN theat_file_managed AS FI_GALL ON FI_GALL.fid = F_GALL.field_gallery_fid

LEFT JOIN theat_field_data_field_sketch AS F_SKET ON F_SKET.entity_id = N.nid
LEFT JOIN theat_file_managed AS FI_SKET ON FI_SKET.fid = F_SKET.field_sketch_fid

LEFT JOIN theat_field_data_field_versiontable AS F_VERS ON F_VERS.entity_id = N.nid

LEFT JOIN theat_field_data_field_group AS F_GROU ON F_GROU.entity_id = N.nid
LEFT JOIN shared_taxonomy_term_data AS T_GROU ON T_GROU.tid = F_GROU.field_group_tid

WHERE N.nid = nid 
AND F_LEAD.`language` = language;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_heater_details
DROP PROCEDURE IF EXISTS `api_get_heater_details`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_get_heater_details`(
	IN `nid` INT,
	IN `language` CHAR(50)





)
BEGIN
SELECT
N.nid, 
N.title AS 'modelName', 
F_TITL.title_field_value AS 'modelNameLoc',
F_LEAD.`language`,
F_LEAD.field_lead_value AS 'lead',
N.created,
N.changed,
F_DESC1.field_description_value AS 'description',
F_DESC2.field_description2_value AS 'description2',
GROUP_CONCAT(DISTINCT FI_GALL.uri SEPARATOR ';') AS 'gallery',
F_SPEC.field_heaterspecification_field_heaterpower_value AS 'heaterPower',
F_SPEC.field_heaterspecification_field_heaterpowerclass_value AS 'heaterPowerClass',
F_SPEC.field_heaterspecification_field_heaterpowers_value AS 'heaterPowers',
T_SPEC1.name AS 'connect',
T_SPEC2.name AS 'ip'
FROM theat_node AS N 
LEFT JOIN theat_field_data_field_lead AS F_LEAD ON F_LEAD.entity_id = N.nid

LEFT JOIN theat_field_data_title_field AS F_TITL ON (F_TITL.entity_id = N.nid AND F_TITL.`language` = F_LEAD.`language`)

LEFT JOIN theat_field_data_field_description AS F_DESC1 ON (F_DESC1.entity_id = N.nid AND F_DESC1.`language` = F_LEAD.`language`)

LEFT JOIN theat_field_data_field_description2 AS F_DESC2 ON (F_DESC2.entity_id = N.nid AND F_DESC2.`language` = F_LEAD.`language`)

LEFT JOIN theat_field_data_field_gallery AS F_GALL ON F_GALL.entity_id = N.nid
LEFT JOIN theat_file_managed AS FI_GALL ON FI_GALL.fid = F_GALL.field_gallery_fid

LEFT JOIN theat_field_data_field_heaterspecification AS F_SPEC ON F_SPEC.entity_id = N.nid
LEFT JOIN shared_taxonomy_term_data AS T_SPEC1 ON T_SPEC1.tid = F_SPEC.field_heaterspecification_field_connect_tid
LEFT JOIN shared_taxonomy_term_data AS T_SPEC2 ON T_SPEC2.tid = F_SPEC.field_heaterspecification_field_ip_tid

WHERE N.nid = nid 
AND F_LEAD.`language` = language;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_order_additional_data
DROP PROCEDURE IF EXISTS `api_get_order_additional_data`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_get_order_additional_data`(
	IN `uid` INT
,
	IN `orderId` INT


)
BEGIN
DECLARE _cnt INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	SELECT 
	O.`field_order_additional_data_value`  AS 'additionalData'
	FROM api_field_data_field_order_additional_data AS O
	WHERE `entity_id` = orderId;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_order_address
DROP PROCEDURE IF EXISTS `api_get_order_address`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_get_order_address`(
	IN `uid` INT,
	IN `orderId` INT









)
BEGIN
DECLARE _cnt INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	(
		SELECT
		SHIP.`entity_id` AS 'orderId',
		PROF.`profile_id` AS 'profileId',
		F_AXAD.`field_ax_address_id_value` AS 'addressIdAX',
		PROF.`type`,
		PROF.`uid`,
		PROF.`status`,
		PROF.`created`,
		PROF.`changed`,
		ADDR.commerce_customer_address_country AS 'country',
		ADDR.commerce_customer_address_locality AS 'locality',
		ADDR.commerce_customer_address_postal_code AS 'postalCode',
		ADDR.commerce_customer_address_thoroughfare AS 'thoroughfare',
		ADDR.commerce_customer_address_premise AS 'premise',
		ADDR.commerce_customer_address_organisation_name AS 'organisationName',
		ADDR.commerce_customer_address_name_line AS 'nameLine',
		ADDR.commerce_customer_address_first_name AS 'firstName',
		ADDR.commerce_customer_address_last_name AS 'lastName'
		from shared_commerce_field_data_commerce_customer_shipping AS SHIP
		LEFT JOIN shared_commerce_commerce_customer_profile AS PROF ON PROF.`profile_id` = SHIP.`commerce_customer_shipping_profile_id`
		LEFT JOIN shared_commerce_field_data_commerce_customer_address AS ADDR ON ADDR.`entity_id` = SHIP.`commerce_customer_shipping_profile_id`
		LEFT JOIN api_field_data_field_ax_address_id AS F_AXAD ON F_AXAD.`entity_id` = SHIP.`commerce_customer_shipping_profile_id`
		WHERE SHIP.`entity_id` = orderId
	) UNION ALL (
		SELECT
		BILL.`entity_id` AS 'orderId',
		PROF.`profile_id` AS 'profileId',
		F_AXAD.`field_ax_address_id_value` AS 'addressIdAX',
		PROF.`type`,
		PROF.`uid`,
		PROF.`status`,
		PROF.`created`,
		PROF.`changed`,
		ADDR.commerce_customer_address_country AS 'country',
		ADDR.commerce_customer_address_locality AS 'locality',
		ADDR.commerce_customer_address_postal_code AS 'postalCode',
		ADDR.commerce_customer_address_thoroughfare AS 'thoroughfare',
		ADDR.commerce_customer_address_premise AS 'premise',
		ADDR.commerce_customer_address_organisation_name AS 'organisationName',
		ADDR.commerce_customer_address_name_line AS 'nameLine',
		ADDR.commerce_customer_address_first_name AS 'firstName',
		ADDR.commerce_customer_address_last_name AS 'lastName'
		from shared_commerce_field_data_commerce_customer_billing AS BILL
		LEFT JOIN shared_commerce_commerce_customer_profile AS PROF ON PROF.`profile_id` = BILL.`commerce_customer_billing_profile_id`
		LEFT JOIN shared_commerce_field_data_commerce_customer_address AS ADDR ON ADDR.`entity_id` = BILL.`commerce_customer_billing_profile_id`
		LEFT JOIN api_field_data_field_ax_address_id AS F_AXAD ON F_AXAD.`entity_id` = BILL.`commerce_customer_billing_profile_id`
		WHERE BILL.`entity_id` = orderId
	);
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_order_delivery_type
DROP PROCEDURE IF EXISTS `api_get_order_delivery_type`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_get_order_delivery_type`(
	IN `uid` INT,
	IN `orderId` INT

)
BEGIN
DECLARE _cnt INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	SELECT
	
	I.`line_item_label` AS 'deliveryType'
	
	FROM shared_commerce_commerce_line_item AS I
	LEFT JOIN shared_commerce_field_data_commerce_total AS F_TOTA ON F_TOTA.`entity_id` = I.`line_item_id`
	LEFT JOIN shared_commerce_field_data_commerce_line_items AS F_ITEM ON F_ITEM.`commerce_line_items_line_item_id` = I.`line_item_id`
	WHERE I.`order_id` = orderId AND I.`type`='shipping';
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_order_line_items
DROP PROCEDURE IF EXISTS `api_get_order_line_items`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_get_order_line_items`(
	IN `uid` INT,
	IN `orderId` INT












)
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
DECLARE _cnt INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	SELECT
	I.`line_item_id` AS 'itemId',
	I.`order_id` AS 'orderId',
	I.`line_item_label` AS 'sku',
	I.`quantity`,
	I.`created`,
	I.`changed`,
	F_TOTA.`commerce_total_amount` AS 'amount',
	F_TOTA.`commerce_total_currency_code` AS 'currencyCode',
	F_TOTA.`commerce_total_data` AS 'data',
	F_ITEM.`deleted`
	FROM shared_commerce_commerce_line_item AS I
	LEFT JOIN shared_commerce_field_data_commerce_total AS F_TOTA ON F_TOTA.`entity_id` = I.`line_item_id`
	LEFT JOIN shared_commerce_field_data_commerce_line_items AS F_ITEM ON F_ITEM.`commerce_line_items_line_item_id` = I.`line_item_id`
	WHERE I.`order_id` = orderId AND I.`type` = 'product'
	ORDER BY I.`changed` ASC;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_order_payment
DROP PROCEDURE IF EXISTS `api_get_order_payment`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_get_order_payment`(
	IN `uid` INT,
	IN `orderId` INT



)
BEGIN
DECLARE _cnt INTEGER;

SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;
IF (_cnt > 0) THEN
	SELECT 
	P.`transaction_id` AS transactionId,
	P.`uid`,
	P.`order_id` AS orderId,
	P.`remote_id` AS remoteId,
	P.`payment_method` AS paymentMethod,
	P.`amount`,
	P.`currency_code` AS currencyCode,
	P.`status`,
	P.`remote_status` AS remoteStatus,
	P.`created`,
	P.`changed`,
	P.`data`
	FROM shared_commerce_commerce_payment_transaction AS P WHERE P.`uid` = uid AND P.`order_id` = orderId;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_order_status
DROP PROCEDURE IF EXISTS `api_get_order_status`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_get_order_status`(
	IN `uid` INT,
	IN `orderId` INT
)
BEGIN
DECLARE _cnt INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	SELECT 
	O.`status` 
	FROM shared_commerce_commerce_order AS O
	WHERE `uid` = uid AND `order_id` = orderId;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_radiator_details
DROP PROCEDURE IF EXISTS `api_get_radiator_details`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_get_radiator_details`(
	IN `nid` INT
,
	IN `language` CHAR(50)






)
BEGIN
SELECT
N.nid, 
N.title AS 'modelName', 
F_TITL.title_field_value AS 'modelNameLoc',
F_LEAD.`language`,
F_LEAD.field_lead_value AS 'lead',
N.created,
N.changed,
F_DESC.field_description_value AS 'description',
GROUP_CONCAT(DISTINCT T_CONN1.name SEPARATOR ';') AS 'connector',
GROUP_CONCAT(DISTINCT T_CONN2.name SEPARATOR ';') AS 'customConnector',
T_TEMP.name AS 'maxTemperature',
T_PRES.name AS 'pressure',
T_WARR.name AS 'warranty',
GROUP_CONCAT(DISTINCT FI_GALL.uri SEPARATOR ';') AS 'gallery',
GROUP_CONCAT(DISTINCT T_ZALE.name SEPARATOR ';') AS 'adv',
GROUP_CONCAT(DISTINCT T_EQUI.name SEPARATOR ';') AS 'equipment'
FROM theat_node AS N 
LEFT JOIN theat_field_data_field_lead AS F_LEAD ON F_LEAD.entity_id = N.nid

LEFT JOIN theat_field_data_title_field AS F_TITL ON (F_TITL.entity_id = N.nid AND F_TITL.`language` = F_LEAD.`language`)

LEFT JOIN theat_field_data_field_description AS F_DESC ON (F_DESC.entity_id = N.nid AND F_DESC.`language` = F_LEAD.`language`)

LEFT JOIN theat_field_data_field_connector AS F_CONN1 ON F_CONN1.entity_id = N.nid
LEFT JOIN shared_taxonomy_term_data AS T_CONN1 ON T_CONN1.tid = F_CONN1.field_connector_tid

LEFT JOIN theat_field_data_field_pod_czenie_niestandardowe AS F_CONN2 ON F_CONN2.entity_id = N.nid
LEFT JOIN shared_taxonomy_term_data AS T_CONN2 ON T_CONN2.tid = F_CONN2.field_pod_czenie_niestandardowe_tid

LEFT JOIN theat_field_data_field_max_temperature AS F_TEMP ON F_TEMP.entity_id = N.nid
LEFT JOIN shared_taxonomy_term_data AS T_TEMP ON T_TEMP.tid = F_TEMP.field_max_temperature_tid

LEFT JOIN theat_field_data_field_pressure_radiator AS F_PRES ON F_PRES.entity_id = N.nid
LEFT JOIN shared_taxonomy_term_data AS T_PRES ON T_PRES.tid = F_PRES.field_pressure_radiator_tid

LEFT JOIN theat_field_data_field_warranty AS F_WARR ON F_WARR.entity_id = N.nid
LEFT JOIN shared_taxonomy_term_data AS T_WARR ON T_WARR.tid = F_WARR.field_warranty_tid

LEFT JOIN theat_field_data_field_gallery AS F_GALL ON F_GALL.entity_id = N.nid
LEFT JOIN theat_file_managed AS FI_GALL ON FI_GALL.fid = F_GALL.field_gallery_fid

LEFT JOIN theat_field_data_field_zalety AS F_ZALE ON F_ZALE.entity_id = N.nid
LEFT JOIN shared_taxonomy_term_data AS T_ZALE ON T_ZALE.tid = F_ZALE.field_zalety_tid

LEFT JOIN theat_field_data_field_equipment AS F_EQUI ON F_EQUI.entity_id = N.nid
LEFT JOIN shared_taxonomy_term_data AS T_EQUI ON T_EQUI.tid = F_EQUI.field_equipment_tid

WHERE N.nid = nid 
AND F_LEAD.`language` = language;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_valid_colors
DROP PROCEDURE IF EXISTS `api_get_valid_colors`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `api_get_valid_colors`(
	IN `regex` VARCHAR(50)
)
BEGIN
SELECT substring(sku, 14, 3) as colors FROM theat_commerce_product WHERE sku REGEXP CONCAT('^', regex) AND type='grzejnik' group by colors;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_valid_connections
DROP PROCEDURE IF EXISTS `api_get_valid_connections`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `api_get_valid_connections`(
	IN `itemId` VARCHAR(11)
)
BEGIN
SELECT substring(sku, 17, 2) as connections FROM theat_commerce_product WHERE sku REGEXP CONCAT('^', itemId) AND type='grzejnik' group by connections;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_valid_models
DROP PROCEDURE IF EXISTS `api_get_valid_models`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `api_get_valid_models`(
	IN `groupId` VARCHAR(2)

)
BEGIN
SELECT substring(sku, 1, 5) as model FROM theat_commerce_product WHERE sku REGEXP CONCAT('^', groupId) AND type='grzejnik' group by model;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_get_valid_sizes
DROP PROCEDURE IF EXISTS `api_get_valid_sizes`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `api_get_valid_sizes`(
	IN `model` VARCHAR(5)
)
BEGIN
SELECT substring(sku, 1, 11) as size FROM theat_commerce_product WHERE sku REGEXP CONCAT('^', model) AND type='grzejnik' group by size;
END//
DELIMITER ;

-- Dumping structure for view db.api_heaters
DROP VIEW IF EXISTS `api_heaters`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_heaters` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`modelNameLoc` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`modelAX` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`language` VARCHAR(32) NULL COMMENT 'The language for this data item.' COLLATE 'utf8_general_ci',
	`lead` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`created` INT(11) NOT NULL COMMENT 'The Unix timestamp when the node was created.',
	`changed` INT(11) NOT NULL COMMENT 'The Unix timestamp when the node was most recently saved.',
	`gallery` TEXT NULL COLLATE 'utf8_bin'
) ENGINE=MyISAM;

-- Dumping structure for view db.api_heaters_stock
DROP VIEW IF EXISTS `api_heaters_stock`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_heaters_stock` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`productId` INT(10) UNSIGNED NULL,
	`sku` VARCHAR(255) NULL COMMENT 'The unique, human-readable identifier for a product.' COLLATE 'utf8_general_ci',
	`configurationName` VARCHAR(255) NULL COMMENT 'The title of this product, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`quantity` DECIMAL(10,2) NULL,
	`price` INT(11) NULL COMMENT 'The price amount.',
	`priceRrp` INT(11) NULL COMMENT 'The price amount.',
	`sale` INT(11) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db.api_heaters_stock_models
DROP VIEW IF EXISTS `api_heaters_stock_models`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_heaters_stock_models` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view db.api_orders
DROP VIEW IF EXISTS `api_orders`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_orders` (
	`orderId` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for an order.',
	`orderNumber` VARCHAR(255) NULL COMMENT 'The order number displayed to the customer.' COLLATE 'utf8_general_ci',
	`orderIdAX` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`uid` INT(11) NOT NULL COMMENT 'The THEAT_users.uid that owns this order.',
	`status` VARCHAR(255) NOT NULL COMMENT 'The status name of this order.' COLLATE 'utf8_general_ci',
	`amount` INT(11) NULL COMMENT 'The price amount.',
	`currencyCode` VARCHAR(32) NULL COMMENT 'The currency code for the price.' COLLATE 'utf8_general_ci',
	`data` LONGTEXT NULL COMMENT 'A serialized array of additional price data.' COLLATE 'utf8_general_ci',
	`created` INT(11) NOT NULL COMMENT 'The Unix timestamp when the order was created.',
	`changed` INT(11) NOT NULL COMMENT 'The Unix timestamp when the order was most recently saved.'
) ENGINE=MyISAM;

-- Dumping structure for view db.api_product_model_image
DROP VIEW IF EXISTS `api_product_model_image`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_product_model_image` (
	`entity_id` INT(10) UNSIGNED NOT NULL COMMENT 'The entity id this data is attached to',
	`modelAX` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`type` VARCHAR(128) NOT NULL COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance' COLLATE 'utf8_general_ci',
	`uri` VARCHAR(255) NULL COMMENT 'The URI to access the file (either local or remote).' COLLATE 'utf8_bin'
) ENGINE=MyISAM;

-- Dumping structure for view db.api_radiators
DROP VIEW IF EXISTS `api_radiators`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_radiators` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`modelNameLoc` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`modelAX` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`language` VARCHAR(32) NULL COMMENT 'The language for this data item.' COLLATE 'utf8_general_ci',
	`lead` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`created` INT(11) NOT NULL COMMENT 'The Unix timestamp when the node was created.',
	`changed` INT(11) NOT NULL COMMENT 'The Unix timestamp when the node was most recently saved.',
	`gallery` TEXT NULL COLLATE 'utf8_bin'
) ENGINE=MyISAM;

-- Dumping structure for view db.api_radiators_stock
DROP VIEW IF EXISTS `api_radiators_stock`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_radiators_stock` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`productId` INT(10) UNSIGNED NULL,
	`sku` VARCHAR(255) NULL COMMENT 'The unique, human-readable identifier for a product.' COLLATE 'utf8_general_ci',
	`configurationName` VARCHAR(255) NULL COMMENT 'The title of this product, always treated as non-markup plain text.' COLLATE 'utf8_general_ci',
	`quantity` DECIMAL(10,2) NULL,
	`price` INT(11) NULL COMMENT 'The price amount.',
	`priceRrp` INT(11) NULL COMMENT 'The price amount.',
	`sale` INT(11) NULL
) ENGINE=MyISAM;

-- Dumping structure for view db.api_radiators_stock_models
DROP VIEW IF EXISTS `api_radiators_stock_models`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_radiators_stock_models` (
	`nid` INT(10) UNSIGNED NOT NULL COMMENT 'The primary identifier for a node.',
	`modelName` VARCHAR(255) NOT NULL COMMENT 'The title of this node, always treated as non-markup plain text.' COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for procedure db.api_set_order_additional_data
DROP PROCEDURE IF EXISTS `api_set_order_additional_data`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_set_order_additional_data`(
	IN `uid` INT,
	IN `orderId` INT,
	IN `orderData` VARCHAR(255)


)
BEGIN
DECLARE _cnt INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	INSERT INTO api_field_data_field_order_additional_data VALUES ('commerce_order', 'commerce_order', 0, orderId, 0, 'und', 0, orderData, '')
	ON DUPLICATE KEY UPDATE `field_order_additional_data_value` = orderData;
END IF;	
END//
DELIMITER ;

-- Dumping structure for procedure db.api_set_order_address
DROP PROCEDURE IF EXISTS `api_set_order_address`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_set_order_address`(
	IN `uid` INT,
	IN `orderId` INT,
	IN `profileId` INT,
	IN `addressIdAX` BIGINT,
	IN `aType` VARCHAR(255) CHARACTER SET utf8,
	IN `country` VARCHAR(255) CHARACTER SET utf8,
	IN `locality` VARCHAR(255) CHARACTER SET utf8,
	IN `postalCode` VARCHAR(255) CHARACTER SET utf8,
	IN `thoroughfare` VARCHAR(255) CHARACTER SET utf8,
	IN `premise` VARCHAR(255) CHARACTER SET utf8,
	IN `organisationName` VARCHAR(255) CHARACTER SET utf8,
	IN `nameLine` VARCHAR(255) CHARACTER SET utf8,
	IN `firstName` VARCHAR(255) CHARACTER SET utf8,
	IN `lastName` VARCHAR(255) CHARACTER SET utf8

)
BEGIN
DECLARE _cnt, _date, _profile_id, _revision_id INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	IF (profileId = 0) THEN
		SET _date = UNIX_TIMESTAMP();
		INSERT INTO shared_commerce_commerce_customer_profile_revision (`profile_id`, `revision_uid`, `status`, `log`, `revision_timestamp`) VALUES (0, uid, 1, '', _date);
		SET _revision_id = LAST_INSERT_ID();
		INSERT INTO shared_commerce_commerce_customer_profile (`revision_id`, `type`, `uid`, `status`, `created`, `changed`) VALUES (_revision_id, aType, uid, 1, _date, _date);
		SET _profile_id = LAST_INSERT_ID(); 
		INSERT INTO shared_commerce_field_data_commerce_customer_address (
		`entity_type`,
		`bundle`,
		`deleted`,
		`entity_id`,
		`revision_id`,
		`language`,
		`delta`,
		`commerce_customer_address_country`,
		`commerce_customer_address_locality`,
		`commerce_customer_address_postal_code`,
		`commerce_customer_address_thoroughfare`,
		`commerce_customer_address_premise`,
		`commerce_customer_address_organisation_name`,
		`commerce_customer_address_name_line`,
		`commerce_customer_address_first_name`,
		`commerce_customer_address_last_name`
		) VALUES (
		'commerce_customer_profile', 
		aType, 
		0, 
		_profile_id, 
		_profile_id, 
		'und', 
		0, 
		country,
		locality,
		postalCode,
		thoroughfare,
		premise,
		organisationName,
		nameLine,
		firstName,
		lastName
		);		
	ELSE
		UPDATE shared_commerce_field_data_commerce_customer_address SET 
		`commerce_customer_address_country` = country,
		`commerce_customer_address_locality` = locality,
		`commerce_customer_address_postal_code` = postalCode,
		`commerce_customer_address_thoroughfare` = thoroughfare,
		`commerce_customer_address_premise` = premise,
		`commerce_customer_address_organisation_name` = organisationName,
		`commerce_customer_address_name_line` = nameLine,
		`commerce_customer_address_first_name` = firstName,
		`commerce_customer_address_last_name` = lastName
		WHERE `entity_id` = profileId;
		SET _profile_id = profileId; 
	END IF;
	
	IF (aType = 'billing') THEN
		INSERT INTO shared_commerce_field_data_commerce_customer_billing VALUES ('commerce_order','commerce_order', 0, orderId, 0, 'und', 0, _profile_id)
		ON DUPLICATE KEY UPDATE `commerce_customer_billing_profile_id` = _profile_id;
	ELSEIF (aType = 'shipping') THEN
		INSERT INTO shared_commerce_field_data_commerce_customer_shipping VALUES ('commerce_order','commerce_order', 0, orderId, 0, 'und', 0, _profile_id)
		ON DUPLICATE KEY UPDATE `commerce_customer_shipping_profile_id` = _profile_id;
	END IF;
	
	INSERT INTO api_field_data_field_ax_address_id VALUES ('commerce_customer_profile', 'commerce_customer_profile', 0, _profile_id, 0, 'und', 0, addressIdAX, NULL) 
	ON DUPLICATE KEY UPDATE `field_ax_address_id_value` = addressIdAX;
END IF;	
END//
DELIMITER ;

-- Dumping structure for procedure db.api_set_order_axid
DROP PROCEDURE IF EXISTS `api_set_order_axid`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `api_set_order_axid`(
	IN `uid` INT,
	IN `orderId` INT,
	IN `orderIdAX` VARCHAR(255)

)
BEGIN
DECLARE _cnt, _date INTEGER;

SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;
IF (_cnt > 0) THEN
	SET _date = UNIX_TIMESTAMP();
	INSERT INTO tstore_field_data_field_ax_zs
	(`entity_type`, `bundle`, `deleted`, `entity_id`, `language`, `delta`, `field_ax_zs_value`)
	VALUES
	('commerce_order', 'commerce_order', 0, orderId, 'und', 0, orderIdAX);
	UPDATE shared_commerce_commerce_order SET `changed` = _date WHERE `order_id` = orderId;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_set_order_delivery_type
DROP PROCEDURE IF EXISTS `api_set_order_delivery_type`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_set_order_delivery_type`(
	IN `uid` INT,
	IN `orderId` INT,
	IN `deliveryType` VARCHAR(255) CHARACTER SET utf8

)
BEGIN
DECLARE _cnt, _delta, _date, _line_item_id INT DEFAULT 0;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = order_id;

IF (_cnt > 0) THEN
	SELECT line_item_id INTO _line_item_id FROM shared_commerce_commerce_line_item WHERE `order_id` = orderId AND `type` = 'shipping';
	SET _date = UNIX_TIMESTAMP();
	IF(_line_item_id = 0) THEN
		INSERT INTO shared_commerce_commerce_line_item (`order_id`, `type`, `line_item_label`, `quantity`, `created`, `changed`) VALUES (orderId, 'shipping', deliveryType, 1, _date, _date);
		SET _line_item_id = LAST_INSERT_ID();
		INSERT INTO shared_commerce_field_data_commerce_total VALUES ('commerce_line_item', 'shipping', 0, _line_item_id, _line_item_id, 'und', 0, 0, 'PLN', NULL);
		SELECT MAX(delta) INTO _delta FROM shared_commerce_field_data_commerce_line_items WHERE `entity_id` = orderId;
		IF (_delta is NULL) THEN
			SET _delta = 0;
		ELSE
			SET _delta = _delta + 1;
		END IF;
		INSERT INTO shared_commerce_field_data_commerce_line_items VALUES ('commerce_order', 'commerce_order', 0, orderId, 0, 'und', _delta, _line_item_id);
	ELSE
		UPDATE shared_commerce_commerce_line_item SET `line_item_label` = deliveryType, `changed` = _date WHERE `order_id` = orderId AND `line_item_id` = _line_item_id;
	END IF;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_set_order_line_items
DROP PROCEDURE IF EXISTS `api_set_order_line_items`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_set_order_line_items`(
	IN `uid` INT,
	IN `orderId` INT,
	IN `itemId` INT,
	IN `sku` VARCHAR(255),
	IN `quantity` DECIMAL(10,2),
	IN `amount` INT,
	IN `currencyCode` VARCHAR(32),
	IN `itemData` LONGTEXT





,
	IN `deleted` INT




)
BEGIN
DECLARE _cnt, _delta, _date, _line_item_id INT DEFAULT 0;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = order_id;

IF (_cnt > 0) THEN
	SET _date = UNIX_TIMESTAMP();
	IF (itemId = 0) THEN
		INSERT INTO shared_commerce_commerce_line_item (`order_id`, `type`, `line_item_label`, `quantity`, `created`, `changed`) VALUES (orderId, 'product', sku, quantity, _date, _date); 
		SET _line_item_id = LAST_INSERT_ID();
		INSERT INTO shared_commerce_field_data_commerce_total VALUES ('commerce_line_item', 'product', 0, _line_item_id, _line_item_id, 'und', 0, amount, currencyCode, itemData);
		SELECT MAX(delta) INTO _delta FROM shared_commerce_field_data_commerce_line_items WHERE `entity_id` = orderId;
		IF (_delta is NULL) THEN
			SET _delta = 0;
		ELSE
			SET _delta = _delta + 1;
		END IF;
		INSERT INTO shared_commerce_field_data_commerce_line_items VALUES ('commerce_order', 'commerce_order', 0, orderId, 0, 'und', _delta, _line_item_id);
	ELSE 
		UPDATE shared_commerce_commerce_line_item SET `order_id` = orderId, `line_item_label` = sku, `quantity` = quantity, `changed` = _date WHERE `line_item_id` = itemId;
		SET _line_item_id = itemId;
		UPDATE shared_commerce_field_data_commerce_total SET `commerce_total_amount` = amount, `commerce_total_currency_code` = currencyCode, `commerce_total_data` = itemData WHERE `entity_id` = itemId;
		UPDATE shared_commerce_field_data_commerce_line_items SET `deleted` = deleted WHERE `commerce_line_items_line_item_id` = itemId;
	END IF;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_set_order_payment
DROP PROCEDURE IF EXISTS `api_set_order_payment`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_set_order_payment`(
	IN `uid` INT,
	IN `orderId` INT,
	IN `transactionId` INT,
	IN `remoteId` VARCHAR(255),
	IN `paymentMethod` VARCHAR(128),
	IN `amount` INT,
	IN `currencyCode` VARCHAR(32),
	IN `status` VARCHAR(128)


,
	IN `remoteStatus` VARCHAR(128)


,
	IN `data` LONGBLOB
)
BEGIN
DECLARE _cnt, _date, _transaction_id INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	SET _date = UNIX_TIMESTAMP();
	IF (transactionId = 0) THEN
		INSERT INTO shared_commerce_commerce_payment_transaction (
		`uid`,
		`order_id`,
		`payment_method`,
		`amount`,
		`currency_code`,
		`status`,
		`remote_status`,
		`created`,
		`changed`,
		`data`
		) VALUES (
		uid,
		orderId,
		paymentMethod,
		amount,
		currencyCode,
		status,
		remoteStatus,
		_date,
		_date,
		data
		);
		SET _transaction_id = LAST_INSERT_ID();
		UPDATE shared_commerce_commerce_payment_transaction SET `revision_id` = _transaction_id WHERE `transaction_id` = _transaction_id;
	ELSE
		UPDATE shared_commerce_commerce_payment_transaction SET
		`remote_id` = remoteId,
		`payment_method` = paymentMethod,
		`amount` = amount,
		`currency_code` = currencyCode,
		`status` = status,
		`remote_status` = remoteStatus,
		`changed` = _date,
		`data` = data
		WHERE `transaction_id` = transactionId AND `uid` = uid AND `order_id` = orderId;
	END IF;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_set_order_payment_method
DROP PROCEDURE IF EXISTS `api_set_order_payment_method`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_set_order_payment_method`(
	IN `uid` INT,
	IN `orderId` INT,
	IN `transactionId` INT,
	IN `paymentMethod` VARCHAR(128)


)
BEGIN
DECLARE _cnt, _date, _transaction_id INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	SET _date = UNIX_TIMESTAMP();
	IF (transactionId = 0) THEN
		INSERT INTO shared_commerce_commerce_payment_transaction (
		`uid`,
		`order_id`,
		`payment_method`,
		`amount`,
		`currency_code`,
		`status`,
		`created`,
		`changed`
		) VALUES (
		uid,
		orderId,
		paymentMethod,
		0,
		'PLN',
		'pending',
		_date,
		_date
		);
		SET _transaction_id = LAST_INSERT_ID();
		UPDATE shared_commerce_commerce_payment_transaction SET `revision_id` = _transaction_id WHERE `transaction_id` = _transaction_id;
	ELSE
		UPDATE shared_commerce_commerce_payment_transaction SET
		`payment_method` = paymentMethod,
		`changed` = _date
		WHERE `transaction_id` = transactionId AND `uid` = uid AND `order_id` = orderId;
	END IF;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_set_order_status
DROP PROCEDURE IF EXISTS `api_set_order_status`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `api_set_order_status`(
	IN `uid` INT,
	IN `orderId` INT,
	IN `status` VARCHAR(255)

)
BEGIN
DECLARE _cnt, _date INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	SET _date = UNIX_TIMESTAMP();
	UPDATE shared_commerce_commerce_order SET `status` = status, `changed` = _date WHERE `uid` = uid AND `order_id` = orderId;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db.api_set_order_total_amount
DROP PROCEDURE IF EXISTS `api_set_order_total_amount`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `api_set_order_total_amount`(
	IN `uid` INT,
	IN `orderId` INT,
	IN `amount` INT

)
BEGIN
DECLARE _cnt, _date INTEGER;
SELECT COUNT(*) INTO _cnt FROM shared_commerce_commerce_order AS O WHERE O.`uid` = uid AND O.`order_id` = orderId;

IF (_cnt > 0) THEN
	SET _date = UNIX_TIMESTAMP();
	INSERT INTO shared_commerce_field_data_commerce_order_total VALUES ('commerce_order', 'commerce_order', 0, orderId, 0, 'und', 0, amount, 'PLN', '')
	ON DUPLICATE KEY UPDATE `commerce_order_total_amount` = amount;
	UPDATE shared_commerce_commerce_order SET `changed` = _date WHERE `order_id` = orderId;
END IF;	
END//
DELIMITER ;

-- Dumping structure for view db.api_users
DROP VIEW IF EXISTS `api_users`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `api_users` (
	`uid` INT(10) UNSIGNED NOT NULL COMMENT 'Primary Key: Unique user ID.',
	`name` VARCHAR(60) NOT NULL COMMENT 'Unique user name.' COLLATE 'utf8_general_ci',
	`pass` VARCHAR(128) NOT NULL COMMENT 'User’s password (hashed).' COLLATE 'utf8_general_ci',
	`mail` VARCHAR(254) NULL COMMENT 'User’s e-mail address.' COLLATE 'utf8_general_ci',
	`theme` VARCHAR(255) NOT NULL COMMENT 'User’s default theme.' COLLATE 'utf8_general_ci',
	`signature` VARCHAR(255) NOT NULL COMMENT 'User’s signature.' COLLATE 'utf8_general_ci',
	`signature_format` VARCHAR(255) NULL COMMENT 'The TMAX_filter_format.format of the signature.' COLLATE 'utf8_general_ci',
	`created` INT(11) NOT NULL COMMENT 'Timestamp for when user was created.',
	`access` INT(11) NOT NULL COMMENT 'Timestamp for previous time user accessed the site.',
	`login` INT(11) NOT NULL COMMENT 'Timestamp for user’s last login.',
	`status` TINYINT(4) NOT NULL COMMENT 'Whether the user is active(1) or blocked(0).',
	`timezone` VARCHAR(32) NULL COMMENT 'User’s time zone.' COLLATE 'utf8_general_ci',
	`language` VARCHAR(12) NOT NULL COMMENT 'User’s default language.' COLLATE 'utf8_general_ci',
	`picture` INT(11) NOT NULL COMMENT 'Foreign key: TMAX_file_managed.fid of user’s picture.',
	`init` VARCHAR(254) NULL COMMENT 'E-mail address used for initial account creation.' COLLATE 'utf8_general_ci',
	`data` LONGBLOB NULL COMMENT 'A serialized array of name value pairs that are related to the user. Any form values posted during user edit are stored and are loaded into the $user object during user_load(). Use of this field is discouraged and it will likely disappear in a future...',
	`uuid` CHAR(36) NOT NULL COMMENT 'The Universally Unique Identifier.' COLLATE 'utf8_general_ci',
	`axId` VARCHAR(10) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for procedure db.api_validate_config
DROP PROCEDURE IF EXISTS `api_validate_config`;
DELIMITER //
CREATE DEFINER=`root`@`%` PROCEDURE `api_validate_config`(
	IN `regex` VARCHAR(250)
)
BEGIN
SELECT sku FROM theat_commerce_product WHERE sku REGEXP regex AND type='grzejnik';
END//
DELIMITER ;

-- Dumping structure for view db.api_accessories
DROP VIEW IF EXISTS `api_accessories`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_accessories`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_accessories` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName`,`F_TITL`.`title_field_value` AS `modelNameLoc`,`F_MODE`.`field_modelax_value` AS `modelAX`,`F_LEAD`.`language` AS `language`,`F_LEAD`.`field_lead_value` AS `lead`,`N`.`created` AS `created`,`N`.`changed` AS `changed`,group_concat(distinct `FI_GALL`.`uri` separator ';') AS `gallery` from (((((`theat_node` `N` left join `theat_field_data_field_lead` `F_LEAD` on((`F_LEAD`.`entity_id` = `N`.`nid`))) left join `theat_field_data_title_field` `F_TITL` on(((`F_TITL`.`entity_id` = `N`.`nid`) and (`F_TITL`.`language` = `F_LEAD`.`language`)))) left join `theat_field_data_field_gallery` `F_GALL` on((`F_GALL`.`entity_id` = `N`.`nid`))) left join `theat_file_managed` `FI_GALL` on((`FI_GALL`.`fid` = `F_GALL`.`field_gallery_fid`))) left join `theat_field_data_field_modelax` `F_MODE` on(((`F_MODE`.`entity_id` = `N`.`nid`) and (`F_MODE`.`entity_type` = 'node')))) where ((`N`.`type` = 'akcesorium_grzewcze') and (`N`.`status` = 1)) group by `N`.`nid`,`F_LEAD`.`language`;

-- Dumping structure for view db.api_accessories_stock
DROP VIEW IF EXISTS `api_accessories_stock`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_accessories_stock`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_accessories_stock` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName`,`F_PROD`.`field_product_product_id` AS `productId`,`C_PROD`.`sku` AS `sku`,`C_PROD`.`title` AS `configurationName`,`C_STOC`.`commerce_stock_value` AS `quantity`,`C_PRIC`.`commerce_price_amount` AS `price`,`F_PRRP`.`field_price_rrp_amount` AS `priceRrp`,`F_SALE`.`field_sale_value` AS `sale` from ((((((`theat_node` `N` left join `theat_field_data_field_product` `F_PROD` on((`F_PROD`.`entity_id` = `N`.`nid`))) left join `theat_commerce_product` `C_PROD` on((`C_PROD`.`product_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_commerce_stock` `C_STOC` on((`C_STOC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `shared_commerce_field_data_commerce_price` `C_PRIC` on((`C_PRIC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_field_price_rrp` `F_PRRP` on((`F_PRRP`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_field_sale` `F_SALE` on((`F_SALE`.`entity_id` = `F_PROD`.`field_product_product_id`))) where ((`N`.`type` = 'akcesorium_grzewcze') and (`N`.`status` = 1) and (`C_STOC`.`commerce_stock_value` > 0) and (`C_PROD`.`status` = 1) and (`C_PRIC`.`commerce_price_amount` > 0)) order by `N`.`nid`;

-- Dumping structure for view db.api_accessories_stock_models
DROP VIEW IF EXISTS `api_accessories_stock_models`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_accessories_stock_models`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_accessories_stock_models` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName` from ((((`theat_node` `N` left join `theat_field_data_field_product` `F_PROD` on((`F_PROD`.`entity_id` = `N`.`nid`))) left join `theat_commerce_product` `C_PROD` on((`C_PROD`.`product_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_commerce_stock` `C_STOC` on((`C_STOC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `shared_commerce_field_data_commerce_price` `C_PRIC` on((`C_PRIC`.`entity_id` = `F_PROD`.`field_product_product_id`))) where ((`N`.`type` = 'akcesorium_grzewcze') and (`N`.`status` = 1) and (`C_STOC`.`commerce_stock_value` > 0) and (`C_PROD`.`status` = 1) and (`C_PRIC`.`commerce_price_amount` > 0)) group by `N`.`nid` order by `N`.`nid`;

-- Dumping structure for view db.api_all_stock
DROP VIEW IF EXISTS `api_all_stock`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_all_stock`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_all_stock` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName`,`F_PROD`.`field_product_product_id` AS `productId`,`C_PROD`.`sku` AS `sku`,`C_PROD`.`title` AS `configurationName`,`C_STOC`.`commerce_stock_value` AS `quantity`,`C_PRIC`.`commerce_price_amount` AS `price`,`F_PRRP`.`field_price_rrp_amount` AS `priceRrp`,`F_SALE`.`field_sale_value` AS `sale` from ((((((`theat_node` `N` left join `theat_field_data_field_product` `F_PROD` on((`F_PROD`.`entity_id` = `N`.`nid`))) left join `theat_commerce_product` `C_PROD` on((`C_PROD`.`product_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_commerce_stock` `C_STOC` on((`C_STOC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `shared_commerce_field_data_commerce_price` `C_PRIC` on((`C_PRIC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_field_price_rrp` `F_PRRP` on((`F_PRRP`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_field_sale` `F_SALE` on((`F_SALE`.`entity_id` = `F_PROD`.`field_product_product_id`))) where (((`N`.`type` = 'grzalka') or (`N`.`type` = 'grzejnik') or (`N`.`type` = 'akcesorium_grzewcze')) and (`N`.`status` = 1) and (`C_STOC`.`commerce_stock_value` > 0) and (`C_PROD`.`status` = 1) and (`C_PRIC`.`commerce_price_amount` > 0)) order by `N`.`nid`;

-- Dumping structure for view db.api_all_stock_models
DROP VIEW IF EXISTS `api_all_stock_models`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_all_stock_models`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_all_stock_models` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName` from ((((`theat_node` `N` left join `theat_field_data_field_product` `F_PROD` on((`F_PROD`.`entity_id` = `N`.`nid`))) left join `theat_commerce_product` `C_PROD` on((`C_PROD`.`product_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_commerce_stock` `C_STOC` on((`C_STOC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `shared_commerce_field_data_commerce_price` `C_PRIC` on((`C_PRIC`.`entity_id` = `F_PROD`.`field_product_product_id`))) where (((`N`.`type` = 'grzalka') or (`N`.`type` = 'grzejnik') or (`N`.`type` = 'akcesorium_grzewcze')) and (`N`.`status` = 1) and (`C_STOC`.`commerce_stock_value` > 0) and (`C_PROD`.`status` = 1) and (`C_PRIC`.`commerce_price_amount` > 0)) group by `N`.`nid` order by `N`.`nid`;

-- Dumping structure for view db.api_colors
DROP VIEW IF EXISTS `api_colors`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_colors`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `api_colors` AS select `COL_ID`.`entity_id` AS `id`,`COL_ID`.`field_color_id_value` AS `colorId`,`TERMS`.`name` AS `name`,`COL_TP`.`field_color_type_value` AS `type`,`COL_HEX`.`field_hex_color_value` AS `hex`,`COL_RGB`.`field_rgb_color_value` AS `rgb`,`FILES`.`uri` AS `img`,`IMG`.`field_color_img_height` AS `height`,`IMG`.`field_color_img_width` AS `width` from ((((((`theat_field_data_field_color_id` `COL_ID` left join `theat_field_data_field_color_type` `COL_TP` on((`COL_TP`.`entity_id` = `COL_ID`.`entity_id`))) left join `shared_taxonomy_term_data` `TERMS` on((`TERMS`.`tid` = `COL_ID`.`entity_id`))) left join `theat_field_data_field_hex_color` `COL_HEX` on((`COL_HEX`.`entity_id` = `COL_ID`.`entity_id`))) left join `theat_field_data_field_rgb_color` `COL_RGB` on((`COL_RGB`.`entity_id` = `COL_ID`.`entity_id`))) left join `theat_field_data_field_color_img` `IMG` on((`IMG`.`entity_id` = `COL_ID`.`entity_id`))) left join `theat_file_managed` `FILES` on((`FILES`.`fid` = `IMG`.`field_color_img_fid`)));

-- Dumping structure for view db.api_heaters
DROP VIEW IF EXISTS `api_heaters`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_heaters`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_heaters` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName`,`F_TITL`.`title_field_value` AS `modelNameLoc`,`F_MODE`.`field_modelax_value` AS `modelAX`,`F_LEAD`.`language` AS `language`,`F_LEAD`.`field_lead_value` AS `lead`,`N`.`created` AS `created`,`N`.`changed` AS `changed`,group_concat(distinct `FI_GALL`.`uri` separator ';') AS `gallery` from (((((`theat_node` `N` left join `theat_field_data_field_lead` `F_LEAD` on((`F_LEAD`.`entity_id` = `N`.`nid`))) left join `theat_field_data_title_field` `F_TITL` on(((`F_TITL`.`entity_id` = `N`.`nid`) and (`F_TITL`.`language` = `F_LEAD`.`language`)))) left join `theat_field_data_field_gallery` `F_GALL` on((`F_GALL`.`entity_id` = `N`.`nid`))) left join `theat_file_managed` `FI_GALL` on((`FI_GALL`.`fid` = `F_GALL`.`field_gallery_fid`))) left join `theat_field_data_field_modelax` `F_MODE` on(((`F_MODE`.`entity_id` = `N`.`nid`) and (`F_MODE`.`entity_type` = 'node')))) where ((`N`.`type` = 'grzalka') and (`N`.`status` = 1)) group by `N`.`nid`,`F_LEAD`.`language`;

-- Dumping structure for view db.api_heaters_stock
DROP VIEW IF EXISTS `api_heaters_stock`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_heaters_stock`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_heaters_stock` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName`,`F_PROD`.`field_product_product_id` AS `productId`,`C_PROD`.`sku` AS `sku`,`C_PROD`.`title` AS `configurationName`,`C_STOC`.`commerce_stock_value` AS `quantity`,`C_PRIC`.`commerce_price_amount` AS `price`,`F_PRRP`.`field_price_rrp_amount` AS `priceRrp`,`F_SALE`.`field_sale_value` AS `sale` from ((((((`theat_node` `N` left join `theat_field_data_field_product` `F_PROD` on((`F_PROD`.`entity_id` = `N`.`nid`))) left join `theat_commerce_product` `C_PROD` on((`C_PROD`.`product_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_commerce_stock` `C_STOC` on((`C_STOC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `shared_commerce_field_data_commerce_price` `C_PRIC` on((`C_PRIC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_field_price_rrp` `F_PRRP` on((`F_PRRP`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_field_sale` `F_SALE` on((`F_SALE`.`entity_id` = `F_PROD`.`field_product_product_id`))) where ((`N`.`type` = 'grzalka') and (`N`.`status` = 1) and (`C_STOC`.`commerce_stock_value` > 0) and (`C_PROD`.`status` = 1) and (`C_PRIC`.`commerce_price_amount` > 0)) order by `N`.`nid`;

-- Dumping structure for view db.api_heaters_stock_models
DROP VIEW IF EXISTS `api_heaters_stock_models`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_heaters_stock_models`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_heaters_stock_models` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName` from ((((`theat_node` `N` left join `theat_field_data_field_product` `F_PROD` on((`F_PROD`.`entity_id` = `N`.`nid`))) left join `theat_commerce_product` `C_PROD` on((`C_PROD`.`product_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_commerce_stock` `C_STOC` on((`C_STOC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `shared_commerce_field_data_commerce_price` `C_PRIC` on((`C_PRIC`.`entity_id` = `F_PROD`.`field_product_product_id`))) where ((`N`.`type` = 'grzalka') and (`N`.`status` = 1) and (`C_STOC`.`commerce_stock_value` > 0) and (`C_PROD`.`status` = 1) and (`C_PRIC`.`commerce_price_amount` > 0)) group by `N`.`nid` order by `N`.`nid`;

-- Dumping structure for view db.api_orders
DROP VIEW IF EXISTS `api_orders`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_orders`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_orders` AS select `O`.`order_id` AS `orderId`,`O`.`order_number` AS `orderNumber`,`F_AXZS`.`field_ax_zs_value` AS `orderIdAX`,`O`.`uid` AS `uid`,`O`.`status` AS `status`,`F_TOTA`.`commerce_order_total_amount` AS `amount`,`F_TOTA`.`commerce_order_total_currency_code` AS `currencyCode`,`F_TOTA`.`commerce_order_total_data` AS `data`,`O`.`created` AS `created`,`O`.`changed` AS `changed` from ((`shared_commerce_commerce_order` `O` left join `tstore_field_data_field_ax_zs` `F_AXZS` on((`F_AXZS`.`entity_id` = `O`.`order_id`))) left join `shared_commerce_field_data_commerce_order_total` `F_TOTA` on((`F_TOTA`.`entity_id` = `O`.`order_id`)));

-- Dumping structure for view db.api_product_model_image
DROP VIEW IF EXISTS `api_product_model_image`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_product_model_image`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `api_product_model_image` AS select `model`.`entity_id` AS `entity_id`,`model`.`field_modelax_value` AS `modelAX`,`model`.`bundle` AS `type`,`files`.`uri` AS `uri` from ((`theat_field_data_field_modelax` `model` left join `theat_field_data_field_gallery` `gallery` on((`gallery`.`entity_id` = `model`.`entity_id`))) left join `theat_file_managed` `files` on((`files`.`fid` = `gallery`.`field_gallery_fid`))) where ((`model`.`entity_type` = 'node') and (`gallery`.`delta` = 0));

-- Dumping structure for view db.api_radiators
DROP VIEW IF EXISTS `api_radiators`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_radiators`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_radiators` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName`,`F_TITL`.`title_field_value` AS `modelNameLoc`,`F_MODE`.`field_modelax_value` AS `modelAX`,`F_LEAD`.`language` AS `language`,`F_LEAD`.`field_lead_value` AS `lead`,`N`.`created` AS `created`,`N`.`changed` AS `changed`,group_concat(distinct `FI_GALL`.`uri` separator ';') AS `gallery` from (((((`theat_node` `N` left join `theat_field_data_field_lead` `F_LEAD` on((`F_LEAD`.`entity_id` = `N`.`nid`))) left join `theat_field_data_title_field` `F_TITL` on(((`F_TITL`.`entity_id` = `N`.`nid`) and (`F_TITL`.`language` = `F_LEAD`.`language`)))) left join `theat_field_data_field_modelax` `F_MODE` on(((`F_MODE`.`entity_id` = `N`.`nid`) and (`F_MODE`.`entity_type` = 'node')))) left join `theat_field_data_field_gallery` `F_GALL` on((`F_GALL`.`entity_id` = `N`.`nid`))) left join `theat_file_managed` `FI_GALL` on((`FI_GALL`.`fid` = `F_GALL`.`field_gallery_fid`))) where ((`N`.`type` = 'grzejnik') and (`N`.`status` = 1)) group by `N`.`nid`,`F_LEAD`.`language`;

-- Dumping structure for view db.api_radiators_stock
DROP VIEW IF EXISTS `api_radiators_stock`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_radiators_stock`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_radiators_stock` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName`,`F_PROD`.`field_product_product_id` AS `productId`,`C_PROD`.`sku` AS `sku`,`C_PROD`.`title` AS `configurationName`,`C_STOC`.`commerce_stock_value` AS `quantity`,`C_PRIC`.`commerce_price_amount` AS `price`,`F_PRRP`.`field_price_rrp_amount` AS `priceRrp`,`F_SALE`.`field_sale_value` AS `sale` from ((((((`theat_node` `N` left join `theat_field_data_field_product` `F_PROD` on((`F_PROD`.`entity_id` = `N`.`nid`))) left join `theat_commerce_product` `C_PROD` on((`C_PROD`.`product_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_commerce_stock` `C_STOC` on((`C_STOC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `shared_commerce_field_data_commerce_price` `C_PRIC` on((`C_PRIC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_field_price_rrp` `F_PRRP` on((`F_PRRP`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_field_sale` `F_SALE` on((`F_SALE`.`entity_id` = `F_PROD`.`field_product_product_id`))) where ((`N`.`type` = 'grzejnik') and (`C_STOC`.`commerce_stock_value` > 0) and (`C_PRIC`.`commerce_price_amount` > 0)) order by `N`.`nid`;

-- Dumping structure for view db.api_radiators_stock_models
DROP VIEW IF EXISTS `api_radiators_stock_models`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_radiators_stock_models`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_radiators_stock_models` AS select `N`.`nid` AS `nid`,`N`.`title` AS `modelName` from ((((`theat_node` `N` left join `theat_field_data_field_product` `F_PROD` on((`F_PROD`.`entity_id` = `N`.`nid`))) left join `theat_commerce_product` `C_PROD` on((`C_PROD`.`product_id` = `F_PROD`.`field_product_product_id`))) left join `theat_field_data_commerce_stock` `C_STOC` on((`C_STOC`.`entity_id` = `F_PROD`.`field_product_product_id`))) left join `shared_commerce_field_data_commerce_price` `C_PRIC` on((`C_PRIC`.`entity_id` = `F_PROD`.`field_product_product_id`))) where ((`N`.`type` = 'grzejnik') and (`N`.`status` = 1) and (`C_STOC`.`commerce_stock_value` > 0) and (`C_PROD`.`status` = 1) and (`C_PRIC`.`commerce_price_amount` > 0)) group by `N`.`nid` order by `N`.`nid`;

-- Dumping structure for view db.api_users
DROP VIEW IF EXISTS `api_users`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `api_users`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `api_users` AS select `U`.`uid` AS `uid`,`U`.`name` AS `name`,`U`.`pass` AS `pass`,`U`.`mail` AS `mail`,`U`.`theme` AS `theme`,`U`.`signature` AS `signature`,`U`.`signature_format` AS `signature_format`,`U`.`created` AS `created`,`U`.`access` AS `access`,`U`.`login` AS `login`,`U`.`status` AS `status`,`U`.`timezone` AS `timezone`,`U`.`language` AS `language`,`U`.`picture` AS `picture`,`U`.`init` AS `init`,`U`.`data` AS `data`,`U`.`uuid` AS `uuid`,`F_AXID`.`field_ax_id_value` AS `axId` from (`shared_users` `U` left join `tstore_field_data_field_ax_id` `F_AXID` on((`F_AXID`.`entity_id` = `U`.`uid`)));

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
