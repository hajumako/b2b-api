<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 12.01.2018
 * Time: 12:03
 */

namespace B2B\Models;


use B2B\Classes\Helpers\DrupalProductBundle;
use B2B\Classes\Helpers\ProductGroupId;

class Product extends Model
{
  protected $preparedImages;

  /**
   * Method to get images from database by bundle.
   *
   * @param string $groupId
   *   Product group identifier in AX.
   * @param string|null $modelAX
   *
   * @return Image[]
   */
  public function getImages(string $groupId = null, string $modelAX = null)
  {
    $statement = "SELECT * FROM api_product_model_image";
    //TODO missing product groups handling needed (WYRE, WYRL, TOWG)
    switch ($groupId) {
      case ProductGroupId::AX_PRODUCT_GROUP_HEATERS:
        $statement .= " WHERE type = '".DrupalProductBundle::HEATER."'";
        break;
      case ProductGroupId::AX_PRODUCT_GROUP_NORMAL_RADIATORS:
        $statement .= " WHERE type = '".DrupalProductBundle::RADIATOR."'";
        break;
      case ProductGroupId::AX_PRODUCT_GROUP_VALVES:
        $statement .= " WHERE type = '".DrupalProductBundle::HEATING_APPLIANCE."'";
        break;
    }

    if ($modelAX) {
      $statement .= " AND field_modelax_value = $modelAX";
    }

    return parent::getItems($statement, null, Image::class);
  }
}