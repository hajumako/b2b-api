<?php

namespace B2B\Models;

use B2B\Classes\Constants\AddressTypeInterface;
use B2B\Classes\Constants\DeliveryTypeInterface;
use B2B\Classes\Constants\OrderStatusInterface;
use B2B\Classes\Constants\PaymentMethodInterface;
use B2B\Classes\Helpers\MongoDb;
use B2B\Wsdl\DtOutletServicesGroup\ArrayOfDtOutletSalesOrderLineData;
use B2B\Wsdl\DtOutletServicesGroup\DTOutletAddress;
use B2B\Wsdl\DtOutletServicesGroup\DTOutletAddressRole;
use B2B\Wsdl\DtOutletServicesGroup\DtOutletCustomerServiceCreateOrUpdateAddressTERRequest;
use B2B\Wsdl\DtOutletServicesGroup\DtOutletCustomerServiceCreateRequest;
use B2B\Wsdl\DtOutletServicesGroup\DtOutletSalesOrderData;
use B2B\Wsdl\DtOutletServicesGroup\DtOutletSalesOrderLineData;
use B2B\Wsdl\DtOutletServicesGroup\DtOutletSalesOrderServiceCreateSalesOrderRequest;
use B2B\Wsdl\DtOutletServicesGroup\RoutingService;
use B2B\Wsdl\DtOutletServicesGroup\SalesStatus;
use League\ISO3166\ISO3166;
use Psr\Http\Message\ServerRequestInterface as Request;
use stdClass;

/** @SWG\Definition(
 *   definition="NewOrder",
 *   type="object",
 *   required={"uid"}
 *  )
 */
class Order extends Model
{
  const TABLE = "api_orders";

  public $orderId;

  public $orderNumber;

  public $orderIdAX;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var number
   */
  public $uid;

  /**
   * @SWG\Property(
   *   ref="#/definitions/OrderStatusInterface"
   * )
   * @var OrderStatusInterface
   */
  public $status;

  public $currencyCode;

  public $data;

  public $created;

  public $changed;

  public $amount;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/LineItem")
   * )
   * @var LineItem[]
   */
  public $lineItems;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/Address")
   * )
   * @var Address[]
   */
  public $addresses;

  /**
   * @SWG\Property(
   *   ref="#/definitions/DeliveryTypeInterface"
   * )
   * @var DeliveryTypeInterface
   */
  public $deliveryType;

  /**
   * @SWG\Property(
   *   ref="#/definitions/PaymentMethodInterface"
   * )
   * @var PaymentMethodInterface
   */
  public $paymentMethod;

  /**
   * Function for object serialization (for JSON)
   *
   * @return $this
   */
  public function jsonSerialize()
  {
    return $this;
  }

  /**
   * @param int $uid
   *  User id
   * @param Request $request
   *
   * @return Order[]
   */
  public function listItems(int $uid, Request $request): array
  {
    $statement = "SELECT * FROM ".self::TABLE." WHERE uid = $uid";

    return parent::getItems($statement, $request);
  }

  /**
   * Method to create new order.
   *
   * @param int $uid
   *   User id
   *
   * @return Order
   */
  public function createOrder(int $uid): Order
  {
    $statement = "CALL api_create_order($uid, '".OrderStatusInterface::CART."', @orderId)";
    $this->db->query($statement);
    $query = $this->db->query("SELECT @orderId");
    $result = $query->fetchAll();
    $query->closeCursor();
    if ($result) {
      return $this->getOrder($uid, $result[0]['@orderId']);
    }

    return null;
  }

  /**
   * Method to get order.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   *
   * @return Order
   */
  public function getOrder(int $uid, int $orderId): Order
  {
    $statement = "SELECT * FROM ".self::TABLE." WHERE uid = $uid AND orderId = $orderId";

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return parent::getItem($statement, self::class);
  }

  /**
   * Method to get order by AX id.
   *
   * @param int $uid
   *   User id
   * @param string $orderIdAX
   *   Order AX identifier.
   *
   * @return Order
   */
  public function getOrderByIdAX(int $uid, string $orderIdAX): Order
  {
    $statement = "SELECT * FROM ".self::TABLE." WHERE uid = $uid AND orderIdAX = '$orderIdAX'";

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return parent::getItem($statement, self::class);
  }

  /**
   * Method to get cart.
   *
   * @param int $uid
   *   User id
   *
   * @return Order
   */
  public function getCart(int $uid): Order
  {
    $statement = "SELECT * FROM ".self::TABLE." WHERE uid = $uid AND (
      status = '".OrderStatusInterface::CART."' OR
      status = '".OrderStatusInterface::CHECKOUT_SHIPPING."' OR
      status = '".OrderStatusInterface::CHECKOUT_PAYMENT."' OR
      status = '".OrderStatusInterface::CHECKOUT_REVIEW."'
    ) ORDER BY orderId DESC";

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return parent::getItem($statement, self::class);
  }

  /**
   * Method to get order status.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   *
   * @return OrderStatus
   */
  public function getOrderStatus(int $uid, int $orderId): OrderStatus
  {
    $statement = "CALL api_get_order_status($uid, $orderId)";

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return parent::getItem($statement, OrderStatus::class);
  }

  /**
   * Method to set order status.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   * @param string $status
   *   Status name o set.
   */
  public function setOrderStatus(int $uid, int $orderId, string $status)
  {
    $this->setCollectionDoc(
      MongoDb::COLLECTION_ORDER_STATUS,
      [
        'uid' => $uid,
        'orderId' => $orderId,
        'status' => $status,
        'created' => time(),
      ]
    );
    $statement = "CALL api_set_order_status($uid, $orderId, '$status')";
    $query = $this->db->query($statement);
    $query->closeCursor();
  }

  /**
   * Method to get line items.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   *
   * @return LineItem[]
   *   Array of line items.
   */
  public function getLineItems(int $uid, int $orderId): array
  {
    $statement = "CALL api_get_order_line_items($uid, $orderId)";

    return parent::getItems($statement, null, LineItem::class);
  }

  /**
   * Method to set line items.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   * @param array $lineItems
   *   Array of line items.
   *
   * @return LineItem[]
   *   Array of line items.
   */
  public function setLineItems(int $uid, int $orderId, array $lineItems): array
  {
    //TODO can be optimized with prepared query
    foreach ($lineItems as $lineItem) {
      $lineItem = (object)$lineItem;
      if (empty($lineItem->deleted)) {
        // Create mongodb entry.
        $this->setCollectionDoc(
          MongoDb::COLLECTION_LINE_ITEMS,
          [
            'uid' => $uid,
            'orderId' => $orderId,
            'lineItem' => $lineItem,
            'created' => time(),
          ]
        );
      }

      $statement = "CALL api_set_order_line_items($uid, $orderId, $lineItem->itemId, '$lineItem->sku', $lineItem->quantity, $lineItem->amount, '$lineItem->currencyCode', '$lineItem->data', $lineItem->deleted)";
      $query = $this->db->query($statement);
      $query->closeCursor();
    }

    $amount = $this->calculateTotalAmount($lineItems);
    $this->setTotalAmount($uid, $orderId, $amount);

    return $this->getLineItems($uid, $orderId);
  }

  /**
   * Get total amount of order
   *
   * @param int $uid
   *   User id.
   * @param int $orderId
   *   Order identifier.
   *
   * @return TotalAmount
   */
  public function getTotalAmount(int $uid, int $orderId): TotalAmount
  {
    $statement = "SELECT amount FROM ".self::TABLE." WHERE uid = $uid AND orderId = $orderId";

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return parent::getItem($statement, TotalAmount::class);
  }

  /**
   * Set total amount of order
   *
   * @param int $uid
   *   User id.
   * @param int $orderId
   *   Order identifier.
   * @param int $amount
   *   Total amount.
   *
   * @return TotalAmount
   */
  public function setTotalAmount(int $uid, int $orderId, int $amount): TotalAmount
  {
    $statement = "CALL api_set_order_total_amount($uid, $orderId, $amount)";
    $query = $this->db->query($statement);
    $query->closeCursor();

    return $this->getTotalAmount($uid, $orderId);
  }

  public function calculateTotalAmount(array $lineItems): int
  {
    $amount = 0;
    foreach ($lineItems as $i => $lineItem) {
      $lineItem = (object)$lineItem;
      if ($lineItem->deleted === 0) {
        $amount += $lineItem->amount * $lineItem->quantity;
      }
    }
    return $amount;
  }

  /**
   * Method to get address.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   *
   * @return Address[]
   */
  public function getAddresses(int $uid, int $orderId): array
  {
    $statement = "CALL api_get_order_address($uid, $orderId)";
    $addresses = array();
    foreach (parent::getItems($statement) as $address) {
      $address->country = (new ISO3166)->alpha2($address->country)['alpha3'];
      array_push($addresses, $address);
    }

    return $addresses;
  }

  /**
   * Method to set addresses.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   * @param Address[] $addresses
   *   Array of addresses to set.
   *
   * @return Address[]
   */
  public function setAddresses(int $uid, int $orderId, array $addresses): array
  {
    foreach ($addresses as $a) {
      $a = (object)$a;
      $pid = $a->profileId + 0;
      //check if address should be saved in AX
      if ($a->addressIdAX === -1) {
        $userModel = new User($this->container);
        $account = $userModel->get($uid);
        $idAX = $this->axSaveAddress($a, $account->axId);
        $a->addressIdAX = $idAX;
      }
      $a->firstName = empty($a->firstName) ? '' : $a->firstName;
      $a->lastName = empty($a->lastName) ? '' : $a->lastName;
      $c = (new ISO3166)->alpha3($a->country)['alpha2'];
      $statement = "CALL api_set_order_address($uid, $orderId, $pid, $a->addressIdAX, '$a->type', '$c', '$a->locality', '$a->postalCode', '$a->thoroughfare', '$a->premise', '$a->organisationName', '$a->nameLine', '$a->firstName', '$a->lastName')";
      $query = $this->db->query($statement);
      $query->closeCursor();
    }

    return $this->getAddresses($uid, $orderId);
  }

  /**
   * Method to get delivery type.
   *
   * @param int $uid
   * @param int $orderId
   *   Order identifier.
   *
   * @return DeliveryType
   */
  public function getDeliveryType(int $uid, int $orderId): DeliveryType
  {
    $statement = "CALL api_get_order_delivery_type($uid, $orderId)";

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return parent::getItem($statement, DeliveryType::class);
  }

  /**
   * Method to set delivery type.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   * @param string $deliveryType
   *   The delivery type.
   *
   * @return DeliveryType
   */
  public function setDeliveryType(int $uid, int $orderId, string $deliveryType): DeliveryType
  {
    $statement = "CALL api_set_order_delivery_type($uid, $orderId, '$deliveryType')";
    $query = $this->db->query($statement);
    $query->closeCursor();

    return $this->getDeliveryType($uid, $orderId);
  }

  /**
   * Method to get payment method.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   *
   * @return Payment
   */
  public function getPaymentMethod(int $uid, int $orderId): Payment
  {
    $statement = "CALL api_get_order_payment($uid, $orderId)";

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return parent::getItem($statement, Payment::class);
  }

  /**
   * Method to set payment method.
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   * @param string $paymentMethod
   *   Payment method.
   * @param int $transactionId
   *   Transaction identifier.
   *
   * @return Payment
   */
  public function setPaymentMethod(int $uid, int $orderId, string $paymentMethod, int $transactionId = 0): Payment
  {
    $this->setCollectionDoc(
      MongoDb::COLLECTION_PAYMENTS,
      [
        'uid' => $uid,
        'orderId' => $orderId,
        'transactionId' => $transactionId,
        'paymentMethod' => $paymentMethod,
        'created' => time(),
      ]
    );
    $tid = $transactionId + 0;
    $statement = "CALL api_set_order_payment_method($uid, $orderId, $tid, '$paymentMethod')";
    $query = $this->db->query($statement);
    $query->closeCursor();

    return $this->getPaymentMethod($uid, $orderId);
  }

  /**
   * Get transaction payment for order
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   *
   * @return Payment
   */
  public function getPayment(int $uid, int $orderId): Payment
  {
    $statement = "CALL api_get_order_payment($uid, $orderId)";

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return parent::getItem($statement, Payment::class);
  }

  /**
   * Method to update payment transaction.
   *
   * @param int $uid
   *   User identifier.
   * @param int $orderId
   *   Order identifier.
   * @param Payment $payment
   *   Payment object.
   *
   * @return Payment
   */
  public function updatePayment(int $uid, int $orderId, Payment $payment): Payment
  {
    if (!empty($payment->transactionId)) {
      $this->setCollectionDoc(
        MongoDb::COLLECTION_PAYMENTS,
        [
          'uid' => $uid,
          'orderId' => $orderId,
          'payment' => $payment,
          'created' => time(),
        ]
      );
      $remoteId = empty($payment->remoteId) ? '' : $payment->remoteId;
      $remoteStatus = empty($payment->remoteStatus) ? '' : $payment->remoteStatus;
      $data = empty($payment->data) ? '' : $payment->data;
      $statement = "CALL api_set_order_payment($uid, $orderId, $payment->transactionId, '$remoteId', '$payment->paymentMethod', $payment->amount, '$payment->currencyCode', '$payment->status', '$remoteStatus', '$data')";
      $query = $this->db->query($statement);
      $query->closeCursor();
    }

    return $this->getPaymentMethod($uid, $orderId);
  }

  /**
   * Get order's additional data
   *
   * @param int $uid
   *   User identifier.
   * @param int $orderId
   *   Order identifier.
   *
   * @return mixed
   */
  public function getAdditionalData(int $uid, int $orderId)
  {
    $statement = "CALL api_get_order_additional_data($uid, $orderId)";
    $data = parent::getItem($statement, AdditionalData::class)->additionalData;

    return $data ? unserialize($data) : new stdClass();
  }

  /**
   * Set order's additional data
   *
   * @param int $uid
   *   User identifier.
   * @param int $orderId
   *   Order identifier.
   * @param array $data
   *   Order's additional data array
   *
   * @return mixed
   */
  public function setAdditionalData(int $uid, int $orderId, array $data)
  {
    $sdata = serialize($data);
    $statement = "CALL api_set_order_additional_data($uid, $orderId, '$sdata')";
    $query = $this->db->query($statement);
    $query->closeCursor();

    return $this->getAdditionalData($uid, $orderId);
  }

  /**
   * Set ZS number (AX order id) returned from AX
   *
   * @param int $uid
   *   User identifier.
   * @param int $orderId
   *   Order identifier.
   * @param string $orderIdAX
   *   ZS number from AX
   *
   * @return Order
   */
  public function setOrderIdAX(int $uid, int $orderId, string $orderIdAX): Order
  {
    $this->setCollectionDoc(
      MongoDb::COLLECTION_ORDERS,
      [
        'uid' => $uid,
        'orderId' => $orderId,
        'orderIdAX' => $orderIdAX,
        'created' => time(),
      ]
    );
    $statement = "CALL api_set_order_axid($uid, $orderId, '$orderIdAX')";
    $query = $this->db->query($statement);
    $query->closeCursor();

    return $this->getOrder($uid, $orderId);
  }

  /**
   * Save order to AX
   *
   * @param int $uid
   *   User id
   * @param int $orderId
   *   Order identifier.
   *
   * @return bool
   */
  public function axSaveOrder(int $uid, int $orderId)
  {
    $statement = "SELECT * FROM api_users WHERE uid = $uid";
    $user = parent::getItem($statement, User::class);

    if (!empty($user) && !empty($user->axId)) {
      if (function_exists('xdebug_disable')) {
        xdebug_disable();
      };
      $s = new RoutingService();
      $salesOrderData = new DtOutletSalesOrderData();
      $salesOrderData->setCustomerId($user->axId);
      $salesOrderData->setPurchOrderFormNum($orderId);
      //TODO testing dates - real values need to be added
      $date = new \DateTime();
      $salesOrderData->setReceiptDateRequested($date->add(new \DateInterval('P7D')));
      $salesOrderData->setShippingDateRequested($date);
      $arrayOfDtOutletSalesOrderLineData = new ArrayOfDtOutletSalesOrderLineData();
      $salesOrderLineDatas = array();
      foreach ($this->getLineItems($uid, $orderId) as $lineItem) {
        if ($lineItem->deleted === 0) {
          $salesOrderLineData = new DtOutletSalesOrderLineData();
          $salesOrderLineData->setItemId($lineItem->getItemIdAX());
          $salesOrderLineData->setConfigId($lineItem->getConfigIdAX());
          $salesOrderLineData->setQty($lineItem->quantity);
          $salesOrderLineData->setSalesUnit('szt.');
          array_push($salesOrderLineDatas, $salesOrderLineData);
        }
      }
      $arrayOfDtOutletSalesOrderLineData->setDtOutletSalesOrderLineData($salesOrderLineDatas);
      $salesOrderData->setOrderLines($arrayOfDtOutletSalesOrderLineData);
      $salesOrderServiceCreateSalesOrderRequest = new DtOutletSalesOrderServiceCreateSalesOrderRequest($salesOrderData);

      //TODO transaction type (payu/on delivery) should be transfered to AX
      $s_response = $s->createSalesOrder($salesOrderServiceCreateSalesOrderRequest)->getResponse();

      if (function_exists('xdebug_enable')) {
        xdebug_enable();
      };

      //save the result of communication with AX
      $payment = $this->getPayment($uid, $orderId);
      $payment->remoteStatus = $s_response->getSalesStatus();
      $payment->data = addslashes(serialize($s_response));
      $this->updatePayment($uid, $orderId, $payment);

      if ($s_response->getSalesStatus() === SalesStatus::Backorder) {
        //success
        $this->setOrderIdAX($uid, $orderId, $s_response->getSalesId());

        return true;
      } elseif ($s_response->getSalesStatus() === SalesStatus::None) {
        //failure
      }
    }

    return false;
  }

  /**
   * Save address to AX.
   *
   * @param Address $address
   *   Address to save
   * @param string $axId
   *   AX user identifier.
   *
   * @return string
   *   Address identifier in AX.
   */
  private function axSaveAddress($address, $axId): string
  {
    $service = new RoutingService();
    $axAddress = new DTOutletAddress();
    $axAddress->setDirPartyLocationRecId(0);
    $axAddress->setAddressContactName($address->nameLine);
    $axAddress->setAddressDescription($address->nameLine);
    $axAddress->setAddressCity($address->locality);
    $axAddress->setAddressStreet($address->thoroughfare);
    $axAddress->setAddressStreetNo($address->premise);
    $axAddress->setAddressZipCode($address->postalCode);
    $axAddress->setAddressCountryRegionId($address->country);
    $type = new DTOutletAddressRole();
    switch ($address->type) {
      case AddressTypeInterface::SHIPPING:
        $type = $type::Delivery;
        break;
      case AddressTypeInterface::BILLING:
        $type = $type::Invoice;
        break;
    }
    $axAddress->setAddressRole($type);

    //TODO needs changing - AX method dissapeared
    $params = new DtOutletCustomerServiceCreateOrUpdateAddressTERRequest($axAddress, $axId);
    try {
      $axRequest = $service->CreateOrUpdateAddressTER($params);

      return $axRequest->getResponse();
    } catch (\Exception $e) {
      $this->logger->addInfo(
        'Error during adding new address to AX: '.$e->getMessage()
      );
    }

    return null;
  }
}

/**
 * @SWG\Definition(
 *   definition="Order",
 *   type="object",
 *   allOf={
 *       @SWG\Schema(ref="#/definitions/NewOrder"),
 *       @SWG\Schema(
 *           required={"orderId"},
 *           @SWG\Property(property="orderId", format="int64", type="integer"),
 *           @SWG\Property(property="orderNumber", type="string"),
 *           @SWG\Property(property="orderIdAX", type="string"),
 *           @SWG\Property(property="created", format="int64", type="integer"),
 *           @SWG\Property(property="changed", format="int64", type="integer"),
 *           @SWG\Property(property="amount", format="int32", type="integer"),
 *           @SWG\Property(property="currencyCode", type="string"),
 *           @SWG\Property(property="data", type="string")
 *       )
 *   }
 * )
 */

