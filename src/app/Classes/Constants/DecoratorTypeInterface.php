<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 23.02.2018
 * Time: 11:53
 */

namespace B2B\Classes\Constants;

/**
 * @SWG\Definition(
 *   definition="DecoratorTypeInterface",
 *   type="string",
 *   enum={"color","image","gradient"},
 *   default="color"
 * )
 */
interface DecoratorTypeInterface
{
  const COLOR = 'color';
  const IMAGE = 'image';
  const GRADIENT = 'gradient';
}