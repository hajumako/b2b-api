<?php

namespace B2B\Models\ExtendedAX;

use B2B\Classes\Traits\CastFromParent;
use B2B\Models\Image;
use B2B\Wsdl\B2BHelperTER\HeaterContract_TER;

/** @SWG\Definition(
 *  definition="HeaterConfigs",
 *  type="object"
 * )
 */
class HeaterConfigs
{
  /**
   * @SWG\Property()
   * @var string[]
   */
  public $packages;

  /**
   * @SWG\Property()
   * @var string[]
   */
  public $colors;

  /**
   * @SWG\Property()
   * @var string[]
   */
  public $cables;


  /**
   * @SWG\Property()
   * @var Image[]
   */
  public $images;


  /**
   * Set image
   *
   * @param string $index
   * @param Image $image
   */
  public function setImage(string $index, Image $image)
  {
    $this->images[$index] = $image;
  }

  /**
   * Get image
   *
   * @param string $index
   * @return Image
   */
  public function getImage(string $index): Image
  {
    return $this->images[$index];
  }
}
