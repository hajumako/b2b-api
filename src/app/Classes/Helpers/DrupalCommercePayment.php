<?php
/**
 * Class DrupalCommercePayment
 */
namespace B2B\Classes\Helpers;


abstract class DrupalCommercePayment
{
  /**
   * The commerce_payment.module local payment transaction status definitions.
   */
  const PAYMENT_TRANSACTION_PENDING = 'pending';
  const PAYMENT_TRANSACTION_SUCCESS = 'success';
  const PAYMENT_TRANSACTION_FAILURE = 'failure';
}
