<?php
/**
 * Created by PhpStorm.
 * User: konrad_o
 * Date: 19.04.18
 * Time: 12:30
 */

namespace B2B\Models;

use B2B\Classes\ProductCodeParser;
use B2B\Classes\Traits\CastFromParent;
use B2B\Models\ExtendedAX\RadiatorColors;
use B2B\Wsdl\B2BHelperTER\InventConfigurationState_TER;

/**
 * @SWG\Definition(
 *     definition="ProductConfiguration",
 *     type="object"
 *  )
 */
class ProductConfiguration
{
  use CastFromParent;

  /**
   * @SWG\Property()
   * @var string
   */
  public $productGroup;

  /**
   * @SWG\Property()
   * @var string
   */
  public $model;

  /**
   * @SWG\Property()
   * @var string
   */
  public $label;

  /**
   * @SWG\Property()
   * @var string
   */
  public $name;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $height;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $width;

  /**
   * @SWG\Property()
   * @var string
   */
  public $itemId;

  /**
   * @SWG\Property()
   * @var string
   */
  public $connection;

  /**
   * @SWG\Property(
   *     ref="#/definitions/RadiatorColors"
   * )
   * @var RadiatorColors
   */
  public $color;

  /**
   * @SWG\Property()
   * @var float $Price
   */
  public $price;

  /**
   * @SWG\Property(
   *     ref="#/definitions/Image"
   * )
   * @var Image
   */
  public $image;

  /**
   * @SWG\Property(
   *   type="array",
   *   @SWG\Items(ref="#/definitions/Image")
   * )
   * @var Image[]
   */
  public $images;

  /**
   * @SWG\Property()
   * @var string
   */
  public $configId;

  /**
   * @SWG\Property()
   * @var string
   */
  public $sku;

  /**
   * @SWG\Property(
   *   ref="#/definitions/InventConfigurationState_TER"
   * )
   * @var InventConfigurationState_TER
   */
  public $configurationState;

  protected $productCodeParser;
  protected $radiatorColors;
  protected $imageModel;
  protected $cache;
  protected $_id;

  /**
   * ProductConfiguration constructor.
   *
   * @param ProductCodeParser $productCodeParser
   * @param Color $colorModel
   * @param Image $imageModel
   */
  public function __construct(
    ProductCodeParser $productCodeParser,
    RadiatorColors $radiatorColors,
    Image $imageModel,
    Cache $cache
  ) {
    $this->productCodeParser = $productCodeParser;
    $this->radiatorColors = $radiatorColors;
    $this->imageModel = $imageModel;
    $this->cache = $cache;
  }

  /**
   * @param string $sku
   *   Product code (itemId & configId)
   *
   * @return ProductConfiguration
   */
  public function getConfig(string $sku): ProductConfiguration
  {
    //TODO for now only radiators are implied
    if ($this->productCodeParser->isRadiator($sku)) {
      $document = $this->cache->getRadiatorConfig($sku);

      if($document) {
        $this->castFromParent($document);
        $this->price = 0;
      } else {
        $this->productCodeParser->parseRadiator($sku);
        $this->sku = $this->productCodeParser->getCode();
        $this->itemId = $this->productCodeParser->getItemId();
        $this->configId = $this->productCodeParser->getConfigId();
        $this->color = $this->radiatorColors->loadColorData($this->productCodeParser->getAXcolor())->jsonSerialize();
        $this->connection = $this->productCodeParser->getConnectionId();
        $this->height = $this->productCodeParser->getHeight();
        $this->width = $this->productCodeParser->getWidth();
        $this->imageModel->setSKU($sku);
        $this->imageModel->prepareDefaultUrl();
        $this->imageModel->prepareSrcset();
        $this->image = $this->imageModel;
        $this->label = $this->productCodeParser->getLabel();
        $this->model = $this->productCodeParser->getModel();
        //TODO temp solution; GetProductData does not return 'name'
        $this->name = "Grzejnik TERMA ".mb_strtoupper($this->label)." ".$this->height."/".$this->width;
        //TODO price shouldn't be stored, because it depends on customer id
        $this->price = null;
        $this->productGroup = $this->productCodeParser->getGroupId();
        //config state shouldn't be stored in cache because it changes often
        $this->configurationState = null;
        $this->_id = null;

        $this->cache->setRadiatorConfig($sku, $this);
      }
    }

    return $this;
  }

  public function setConfig(string $sku, $productConfiguration): ProductConfiguration {
    //TODO price shouldn't be stored, because it depends on customer
    //TODO needs refactoring so it's not send to API or is stored in another place
    $this->castFromParent($productConfiguration);
    $this->price = 0;
    $this->cache->setRadiatorConfig($sku, $this);

    return $this;
  }

  /**
   * Function for object serialization (for JSON)
   *
   * @return $this
   */
  public function jsonSerialize()
  {
    return $this;
  }
}
