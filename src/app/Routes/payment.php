<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 08.02.2018
 * Time: 14:54
 */

/** @SWG\Get(
 *    path="/payment/orders/{orderId}",
 *    description="Method to get order's payment details.",
 *    summary="Method to get order's payment details.",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="Payment response",
 *      @SWG\Schema(ref="#/definitions/Payment"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/payment/orders/{orderId}', ['\B2B\Controllers\PaymentController', 'getOrder']);

/** @SWG\Post(
 *    path="/payment/orders/{orderId}",
 *    description="Method to add order to payment gateway.",
 *    summary="Method to add order to payment gateway to get redirect url to do payment.",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/order_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="Payment gateway response",
 *      @SWG\Schema(ref="#/definitions/PaymentGateway"),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->post('/payment/orders/{orderId}', ['\B2B\Controllers\PaymentController', 'setOrder']);

$app->post('/payment/notify', ['\B2B\Controllers\PaymentController', 'consumeNotify']);