<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 08.02.2018
 * Time: 14:54
 */

/** @SWG\Get(
 *    path="/radiators/{lang}/list",
 *    description="List all radiators",
 *    summary="Get list of all radiators",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/filter"),
 *    @SWG\Parameter(ref="#/parameters/order"),
 *    @SWG\Parameter(ref="#/parameters/limit"),
 *    @SWG\Parameter(ref="#/parameters/offset"),
 *    @SWG\Response(
 *      response=200,
 *      description="radiator response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Radiator")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/radiators/{lang}/list', ['\B2B\Controllers\RadiatorController', 'listItems']);

/** @SWG\Get(
 *    path="/radiators/{lang}/item/{nid}",
 *    description="Get radiator details",
 *    summary="Get details of the radiator with nid",
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/node_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="radiator response",
 *      @SWG\Schema(ref="#/definitions/Radiator")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/radiators/{lang}/item/{nid}', ['\B2B\Controllers\RadiatorController', 'itemDetails']);

/** @SWG\Get(
 *    path="/radiators/{lang}/specs/get/{sku}",
 *    description="Get radiator specs from AX",
 *    summary="Get radiator specs from AX ny sku",
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/sku"),
 *    @SWG\Response(
 *      response=200,
 *      description="radiator response",
 *      @SWG\Schema(ref="#/definitions/RadiatorSpecs")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/radiators/{lang}/specs/get/{sku}', ['\B2B\Controllers\RadiatorController', 'getRadiatorAXSpecsBySku']);

/** @SWG\Get(
 *    path="/radiators/{lang}/specs/get/{itemId}/{configId}",
 *    description="Get radiator specs from AX",
 *    summary="Get radiator specs from AX by itemId & configId",
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/itemId"),
 *    @SWG\Parameter(ref="#/parameters/configId"),
 *    @SWG\Response(
 *      response=200,
 *      description="radiator response",
 *      @SWG\Schema(ref="#/definitions/RadiatorSpecs")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/radiators/{lang}/specs/get/{itemId}/{configId}', ['\B2B\Controllers\RadiatorController', 'getRadiatorAXSpecsByParts']);


/** @SWG\Get(
 *    path="/heaters/{lang}/list",
 *    description="List all heaters",
 *    summary="Get list of all heaters",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/filter"),
 *    @SWG\Parameter(ref="#/parameters/order"),
 *    @SWG\Parameter(ref="#/parameters/limit"),
 *    @SWG\Parameter(ref="#/parameters/offset"),
 *    @SWG\Response(
 *      response=200,
 *      description="heater response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Heater")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
*      )
 *    )
 *  )
 */
$app->get('/heaters/{lang}/list', ['\B2B\Controllers\HeaterController', 'listItems']);

/** @SWG\Get(
 *    path="/heaters/{lang}/item/{nid}",
 *    description="Get heater details",
 *    summary="Get details of the heater with nid",
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/node_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="heater response",
 *      @SWG\Schema(ref="#/definitions/Heater")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/heaters/{lang}/item/{nid}', ['\B2B\Controllers\HeaterController', 'itemDetails']);


/** @SWG\Get(
 *    path="/accessories/{lang}/list",
 *    description="List all accessories",
 *    summary="Get list of all accessories",
 *    produces={"application/json"},
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/filter"),
 *    @SWG\Parameter(ref="#/parameters/order"),
 *    @SWG\Parameter(ref="#/parameters/limit"),
 *    @SWG\Parameter(ref="#/parameters/offset"),
 *    @SWG\Response(
 *      response=200,
 *      description="accessory response",
 *      @SWG\Schema(
 *        type="array",
 *        @SWG\Items(ref="#/definitions/Accessory")
 *      ),
 *      @SWG\Header(header="X-Token", type="string", description="Token with expiration date"),
 *      @SWG\Header(header="X-Next", type="string", description="A link to the next page of responses"),
 *      @SWG\Header(header="X-Total", type="string", description="Total number of records in database")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/accessories/{lang}/list', ['\B2B\Controllers\AccessoryController', 'listItems']);

/** @SWG\Get(
 *    path="/accessories/{lang}/item/{nid}",
 *    description="Get accessory details",
 *    summary="Get details of the accessory with nid",
 *    @SWG\Parameter(ref="#/parameters/language"),
 *    @SWG\Parameter(ref="#/parameters/node_id"),
 *    @SWG\Response(
 *      response=200,
 *      description="heater response",
 *      @SWG\Schema(ref="#/definitions/Accessory")
 *    ),
 *    @SWG\Response(
 *      response=500,
 *      description="internal error"
 *    ),
 *    @SWG\Response(
 *      response="default",
 *      description="an ""unexpected"" error",
 *      @SWG\Schema(
 *        ref="#/definitions/ErrorModel"
 *      )
 *    )
 *  )
 */
$app->get('/accessories/{lang}/item/{nid}', ['\B2B\Controllers\AccessoryController', 'itemDetails']);
