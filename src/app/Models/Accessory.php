<?php
/**
 * @file
 * Contains Accessory model code implementation.
 */

namespace B2B\Models;

use Psr\Http\Message\ServerRequestInterface as Request;

/** @SWG\Definition(
 *   definition="Accessory",
 *   type="object"
 *  )
 */
class Accessory extends Product
{
  const TABLE = "api_accessories";

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $nid;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelName;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelNameLoc;

  /**
   * @SWG\Property()
   * @var string
   */
  public $modelAX;

  /**
   * @SWG\Property()
   * @var string
   */
  public $language;

  /**
   * @SWG\Property()
   * @var string
   */
  public $lead;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $created;

  /**
   * @SWG\Property(
   *   format="int64"
   * )
   * @var integer
   */
  public $changed;

  /**
   * @SWG\Property(
   *   property="gallery"
   * )
   * @var string
   */

  public function __set($name, $value)
  {
    if ($name === 'gallery') {
      //TODO $this->container->get('img_host') throws error - container is null when __set is being called
      $this->gallery = str_replace('public://', 'https://www.termaheat.pl/sites/default/files/', $value);
    }
  }

  /**
   * @param string $lang
   *   Language.
   * @param Request $request
   *   Request object.
   *
   * @return Accessory[]
   */
  public function getList(string $lang, Request $request): array
  {
    $statement = "SELECT * FROM " . self::TABLE . " WHERE language='$lang'";

    return parent::getItems($statement, $request, self::class);
  }

  /**
   * Get accessory details
   *
   * @param int $nid
   *   Node identifier
   * @param string $lang
   *   Language
   *
   * @return Accessory
   */
  public function get(int $nid, string $lang): Accessory
  {
    $statement = "CALL api_get_accessory_details($nid, '$lang')";

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    return parent::getItem($statement, self::class);
  }

}
