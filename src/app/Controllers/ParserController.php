<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 06.02.2018
 * Time: 15:02
 */

namespace B2B\Controllers;


use B2B\Classes\Exceptions\UnauthorizedException;
use B2B\Classes\ProductCodeParser;
use B2B\Models\ConfigValidator;
use B2B\Wsdl\B2BHelperTER\ArrayOfDTItemInfoReference;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetConfigurationStateRequest;
use B2B\Wsdl\B2BHelperTER\DTItemInfoReference;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use B2B\Wsdl\B2BHelperTER\RoutingService as B2BRoutingService;

class ParserController extends Controller
{
  protected $B2BroutingService;
  protected $tokenController;
  protected $parser;
  protected $configValidator;

  /**
   * ParserController constructor.
   *
   * @param Container $container
   * @param B2BroutingService $B2BroutingService
   * @param TokenController $tokenController
   * @param ProductCodeParser $parser
   * @param ConfigValidator $configValidator
   */
  public function __construct(
    Container $container,
    B2BRoutingService $B2BroutingService,
    TokenController $tokenController,
    ProductCodeParser $parser,
    ConfigValidator $configValidator
  ) {
    $this->B2BroutingService = $B2BroutingService;
    $this->tokenController = $tokenController;
    $this->parser = $parser;
    $this->configValidator = $configValidator;
    parent::__construct($container);
  }

  /**
   * Validate product codes againts AX
   *
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   * @throws UnauthorizedException
   */
  public function validateCodes(Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    if ($uid != null) {
      $codes = $request->getParsedBody();
      $DTItemInfoReferenceArr = [];
      foreach ($codes as $code) {
        $code = strtoupper($code);
        $sku = explode('-', strtoupper($code));
        $this->parser->reset();
        if ($this->parser->isAccessory($code)) {
          $this->parser->parseAccessory($code);
        } elseif ($this->parser->isCommodity($code)) {
          $this->parser->parseCommodity($code);
        } elseif ($this->parser->isHeater($code)) {
          $this->parser->parseHeater($code);
        } elseif ($this->parser->isRadiator($code)) {
          $this->parser->parseRadiator($code);
        }
        $DTItemInfoReference = new DTItemInfoReference();
        $DTItemInfoReference->setProductId($this->parser->getItemId() ?: $sku[0]);
        if ($this->parser->isConfigIdValid()) {
          $DTItemInfoReference->setVariant($this->parser->getConfigId());
        } elseif (count($sku) === 2) {
          $DTItemInfoReference->setVariant($sku[1]);
        }
        $DTItemInfoReferenceArr[] = $DTItemInfoReference;
      }
      $arrayOfDTItemInfoReference = new ArrayOfDTItemInfoReference();
      $arrayOfDTItemInfoReference->setDTItemInfoReference($DTItemInfoReferenceArr);
      $params = new B2BHelper_TERGetConfigurationStateRequest($arrayOfDTItemInfoReference);
      $axResponse = $this->B2BroutingService->GetConfigurationState($params)->getResponse();

      $response
        ->getBody()
        ->write(json_encode($axResponse->jsonSerialize(), $this->isDebugMode ? JSON_PRETTY_PRINT : null));

      return $response;
    }
    throw new UnauthorizedException('null $uid in ' . __METHOD__);
  }
}