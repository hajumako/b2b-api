<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 19.07.2017
 * Time: 11:06
 */

namespace B2B\Models;

/** @SWG\Definition(
 *   definition="Credentials",
 *   type="object",
 *   required={"user", "password"}
 *  )
 */
class Credentials extends Model
{
  /**
   * @SWG\Property()
   * @var string
   */
  public $user;

  /**
   * @SWG\Property()
   * @var string
   */
  public $password;
}
