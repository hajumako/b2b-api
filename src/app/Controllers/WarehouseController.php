<?php
/**
 * @file
 * Contains class WarehouseController code implementation.
 */

namespace B2B\Controllers;


use B2B\Classes\ProductCodeParser;
use B2B\Models\Cache;
use B2B\Models\ConfigValidator;
use B2B\Models\ExtendedAX\InventAvailability;
use B2B\Models\ProductConfiguration;
use B2B\Models\User;
use B2B\Models\WarehouseItem;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetAvailabilityByGroupRequest;
use B2B\Wsdl\B2BHelperTER\NoYesCombo;
use B2B\Wsdl\B2BHelperTER\RoutingService as B2BRoutingService;
use B2B\Wsdl\DtOutletServicesGroup\RoutingService as OutletRoutingService;
use B2B\Wsdl\PriceService\ArrayOfPriceCustomerRequest_TER;
use B2B\Wsdl\PriceService\PriceCustomerRequest_TER;
use B2B\Wsdl\PriceService\PriceService_TERGetCustomerPricesRequest;
use B2B\Wsdl\PriceService\RoutingService as PriceRoutingService;
use B2B\Models\Image;
use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class WarehouseController extends Controller
{
  protected $B2BroutingService;
  protected $outletRoutingService;
  protected $priceRoutingService;
  protected $tokenController;
  protected $warehouseModel;
  protected $imageModel;
  protected $userModel;
  protected $parser;
  protected $configValidator;
  protected $productConfiguration;
  protected $cache;

  public function __construct(
    Container $container,
    B2BRoutingService $B2BroutingService,
    OutletRoutingService $outletRoutingService,
    PriceRoutingService $priceRoutingService,
    TokenController $tokenController,
    WarehouseItem $warehouseModel,
    Image $imageModel,
    User $userModel,
    ProductCodeParser $parser,
    ConfigValidator $configValidator,
    ProductConfiguration $productConfiguration,
    Cache $cache
  ) {
    $this->B2BroutingService = $B2BroutingService;
    $this->outletRoutingService = $outletRoutingService;
    $this->priceRoutingService = $priceRoutingService;
    $this->tokenController = $tokenController;
    $this->warehouseModel = $warehouseModel;
    $this->imageModel = $imageModel;
    $this->userModel = $userModel;
    $this->parser = $parser;
    $this->configValidator = $configValidator;
    $this->productConfiguration = $productConfiguration;
    $this->cache = $cache;
    parent::__construct($container);
  }

  /**
   * Get models list
   *
   * @param string $lang
   *   Language
   * @param string $productType
   *   Warehouse item type
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function listModels(string $lang, string $productType, Request $request, Response $response): Response
  {
    $items = $this->warehouseModel->getModelsList($productType, $request);
    $response
      ->getBody()
      ->write(json_encode($items, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
    $this->total = $this->warehouseModel->getTotal();

    return parent::addHeaders('', $request, $response);
  }

  /**
   * Get models list from AX
   *
   * @param string $lang
   *   Language
   * @param string $groupId
   *   Warehouse item type
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function listAXModels($lang, string $groupId, Request $request, Response $response): Response
  {
    $params = new B2BHelper_TERGetAvailabilityByGroupRequest($groupId, NoYesCombo::No);
    $axWarehouseItems = $this->B2BroutingService->GetAvailabilityByGroup($params)
      ->getResponse()
      ->getInventAvailabilityStruct_TER();

    $AXmodels = [];
    $found = [];
    foreach ($axWarehouseItems as $axWarehouseItem) {
      $model = substr($axWarehouseItem->getItemId(), 0, 5);
      if (!in_array($model, $found)) {
        $productLabel = $this->cache->getProductLabel($model);
        $productLabel->label = $productLabel->label ?: '??? [' . $productLabel->model . ']';
        unset($productLabel->_id);
        $AXmodels[] = $productLabel;
        $found[] = $model;
      }
    }

    $response
      ->getBody()
      ->write(json_encode($AXmodels, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }

  /**
   * Get warehouse items list from Drupal
   *
   * @param string $lang
   *   Language
   * @param string $productType
   *   Warehouse item type
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function listItems(string $lang, string $productType, Request $request, Response $response): Response
  {
    $items = $this->warehouseModel->getList($productType, $request);
    foreach ($items as &$item) {
      /*$this->imageModel->setSKU($item->sku);
      $this->imageModel->prepareDefaultUrl();
      $this->imageModel->prepareSrcset();
      $item->setImage(clone $this->imageModel);*/
      $this->productConfiguration->getConfig($item->sku);
      $item->setConfig(clone $this->productConfiguration);
    }
    $response
      ->getBody()
      ->write(json_encode($items, $this->isDebugMode ? JSON_PRETTY_PRINT : null));
    $this->total = $this->warehouseModel->getTotal();

    return parent::addHeaders('', $request, $response);
  }

  /**
   * Get warehouse items list from AX
   *
   * @param string $groupId
   *   Warehouse item type
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function listAXItems(string $lang, string $groupId, Request $request, Response $response): Response
  {
    $uid = $this->tokenController->getUserId();
    $account = $this->userModel->get($uid);

    $params = new B2BHelper_TERGetAvailabilityByGroupRequest($groupId, NoYesCombo::No);
    $axWarehouseItems = $this->B2BroutingService->GetAvailabilityByGroup($params)
      ->getResponse()
      ->getInventAvailabilityStruct_TER();

    $limit = null;
    $offset = 0;
    //TODO simple filtering, only model name
    $filter = null;
    $params = $request->getQueryParams();
    if (count($params)) {
      foreach ($params as $name => $value) {
        if ($name === 'filter') {
          $filters = explode(',', $value);
          foreach ($filters as $i => $filter) {
            $f = explode('.', $filter);
            switch ($f[1]) {
              case 'eq':
                $filter = $f[2];
                break;
            }
          }
        } elseif ($name === 'limit') {
          $limit = $value;
        } elseif ($name === 'offset') {
          $offset = $value;
        }
      }
    }
    $wIs = [];
    if ($filter) {
      foreach ($axWarehouseItems as $axWarehouseItem) {
        if ($filter === substr($axWarehouseItem->getItemId(), 0, 5)) {
          $wIs[] = $axWarehouseItem;
        }
      }
    } else {
      $wIs = $axWarehouseItems;
    }
    $this->total = count($wIs);
    $wIs = array_slice($wIs, $offset, $limit);
    $arrayPriceCustomer = [];
    foreach ($wIs as $wI) {
      $priceCustomerRequest = new PriceCustomerRequest_TER();
      $priceCustomerRequest->setCalculateBasePrice(true);
      $priceCustomerRequest->setCalculateIndividualPrice(false);
      $priceCustomerRequest->setCalculateLineAmount(false);
      $priceCustomerRequest->setCalculateSalesPrice(true);
      $priceCustomerRequest->setCurrency('PLN');
      $priceCustomerRequest->setItemId($wI->getItemId());
      $priceCustomerRequest->setConfigId($wI->getConfigId());
      if ($wI->getIsOutlet() || empty($account->axId)) {
        $priceCustomerRequest->setCustAccount($this->container->get('outlet_user'));
      } else {
        $priceCustomerRequest->setCustAccount($account->axId);
      }
      $arrayPriceCustomer[] = $priceCustomerRequest;
    }
    $arrayOfPriceCustomerRequest_TER = new ArrayOfPriceCustomerRequest_TER();
    $arrayOfPriceCustomerRequest_TER->setPriceCustomerRequest_TER($arrayPriceCustomer);
    $params2 = new PriceService_TERGetCustomerPricesRequest($arrayOfPriceCustomerRequest_TER);
    $customerPrices = $this->priceRoutingService->GetCustomerPrices($params2)->getResponse();
    $warehouseItems = [];
    foreach ($customerPrices as $i => $customerPrice) {
      $warehouseItem = new InventAvailability();
      $warehouseItem->loadFromParentObj($wIs[$i]);
      $this->productConfiguration->getConfig($wIs[$i]->getItemId() . '-' . $wIs[$i]->getConfigId());
      $warehouseItem->setConfig(clone $this->productConfiguration);
      $warehouseItem->setListPrice(round($customerPrice->getBasePrice() * 100));
      $warehouseItem->setSalesPrice(round($customerPrice->getSalesPrice() * 100));
      $warehouseItems[] = $warehouseItem->jsonSerialize();
    }
    $response
      ->getBody()
      ->write(json_encode($warehouseItems, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return parent::addHeaders('', $request, $response);
  }

  /**
   * Get warehouse item details
   *
   * @param string $lang
   *   Language
   * @param int $nid
   *   Node id
   * @param Request $request
   * @param Response $response
   *
   * @return Response
   */
  public function itemDetails(string $lang, int $nid, Request $request, Response $response): Response
  {
    $items = $this->warehouseModel->get($nid);
    $response
      ->getBody()
      ->write(json_encode($items, $this->isDebugMode ? JSON_PRETTY_PRINT : null));

    return $response;
  }
}
