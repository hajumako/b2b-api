<?php

namespace B2B\Controllers;


use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Controller
{
  protected $total;
  protected $logger;
  protected $container;
  protected $isDebugMode;


  public function __construct(Container $container)
  {
    $this->container = $container;
    $this->logger = $container->get('logger');
    $this->isDebugMode = $container->get('debug_config')['debug_mode'] ?? false;
  }

  /**
   * Add limit, offset & total headers to Psr Response.
   *
   * @param string $lang
   *   Language of selected records, pass null for all languages or table is not language specific.
   * @param Request $request
   *   Psr request.
   * @param Response $response
   *   Psr response.
   *
   * @return Response
   *   Psr response with headers.
   */

  protected function addHeaders(string $lang, Request $request, Response $response): Response
  {
    //TODO $lang is not used
    $params = $request->getQueryParams();
    if (isset($params['limit']) && isset($params['offset'])) {
      $response = $response
        ->withHeader('X-Next', '?limit=' . $params['limit'] . '&offset=' . ($params['offset'] + $params['limit']))
        ->withHeader('X-Total', $this->total);
      if ($params['offset'] > 0) {
        $response = $response
          ->withHeader('X-Prev', '?limit=' . $params['limit'] . '&offset=' . ($params['offset'] - $params['limit']));
      }
    }

    return $response;
  }
}
