<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 01.02.2018
 * Time: 09:57
 */

namespace B2B\Classes;


use B2B\Classes\Helpers\MongoDb;
use B2B\Classes\Helpers\ProductGroupId;
use B2B\Wsdl\B2BHelperTER\RoutingService as B2BRoutingService;
use B2B\Wsdl\DTIntegrationItemInfoServiceGroup\ArrayOfDTRadiatorSurface_TER;
use B2B\Wsdl\DTIntegrationItemInfoServiceGroup\DTIntegrationItemInfoServiceGetRadiatorSpecBySurface_TERRequest;
use B2B\Wsdl\DTIntegrationItemInfoServiceGroup\DTRadiatorSurface_TER;
use B2B\Wsdl\DTIntegrationItemInfoServiceGroup\RoutingService AS DTRoutingService;
use B2B\Wsdl\B2BHelperTER\B2BHelper_TERGetAllRadiatorColorsRequest;
use DI\Container;
use MongoDB\BSON\ObjectID;
use stdClass;

class _ProductCodeParser
{
  protected $container;
  protected $logger;
  protected $mongoDb;
  protected $B2BroutingService;
  protected $DTroutingService;

  protected $groupId;
  protected $groupShort;
  protected $itemId;
  protected $configId;

  //AX codes
  protected $model;
  protected $modelShort;
  protected $size;
  protected $package;
  protected $colorId;
  protected $connectionId;

  //readable values
  protected $label;
  protected $height;
  protected $width;
  protected $color;

  //AX objects;
  protected $axSize;
  protected $axColor;

  protected $itemIdValid;
  protected $configIdValid;

  /**
   * ProductCodeParser constructor.
   *
   * @param Container $container
   * @param B2BRoutingService $B2BroutingService
   * @param DTRoutingService $DTroutingService
   */
  public function __construct(
    Container $container,
    B2BRoutingService $B2BroutingService,
    DTRoutingService $DTroutingService
  ) {
    $this->container = $container;
    $this->logger = $container->get('logger');
    $this->mongoDb = MongoDb::getConnection($this->container->get('mongodb'));
    $this->B2BroutingService = $B2BroutingService;
    $this->DTroutingService = $DTroutingService;
  }

  /**
   * Parse product configuration based on itemId and configId
   *
   * @param string $itemId
   *  Product itemId
   * @param string|null $configId
   *  Product configId
   */
  public function parse(string $itemId, string $configId = null)
  {
    $this->itemId = $itemId;
    $this->configId = $configId;

    $this->groupShort = substr($this->itemId, 0, 2);
    $this->model = substr($this->itemId, 0, 5);
    $this->modelShort = substr($this->itemId, 2, 3);
    $this->size = substr($this->itemId, 5, 6);

    $this->groupId = ProductGroupId::getGroupId($this->groupShort);

    $this->itemIdValid = false;

    $document = $this->getCollectionDoc(
      MongoDb::COLLECTION_RADIATOR_SPECS,
      ['itemId' => $itemId]
    );

    $specs = new stdClass();
    if (!$document) {
      $item = new DTRadiatorSurface_TER();
      $item->setProductId($itemId);
      $items = new ArrayOfDTRadiatorSurface_TER();
      $items->setDTRadiatorSurface_TER(array($item));
      $params = new  DTIntegrationItemInfoServiceGetRadiatorSpecBySurface_TERRequest($items);
      $axSpecs = $this->DTroutingService->GetRadiatorSpecBySurface_TER($params)->getResponse()
        ->getDTRadiatorSpecData_TER()[0];

      if ($axSpecs->getSpecifications()->getDTItemSpecification()) {
        $specs->itemId = $itemId;
        foreach ($axSpecs->getSpecifications()->getDTItemSpecification() as $axSpec) {
          $n = $axSpec->getPropatyName();
          $specs->fullSpecs[] = [
            'name' => $axSpec->getPropatyName(),
            'value' => $axSpec->getPropertyValue(),
          ];
          if ($n === 'Wysokość A [mm]') {
            $specs->height = $axSpec->getPropertyValue();
          } elseif ($n === 'Szerokość B [mm]') {
            $specs->width = $axSpec->getPropertyValue();
          } elseif ($n === 'Nazwa na etykietę') {
            $specs->label = str_replace('\'', '', $axSpec->getPropertyValue());
          }

        }

        $this->upsertCollectionDoc(
          MongoDB::COLLECTION_RADIATOR_SPECS,
          ['itemId' => $specs->itemId],
          [
            'height' => $specs->height,
            'width' => $specs->width,
            'label' => $specs->label,
            'fullSpecs' => $specs->fullSpecs,
          ]
        );
      }
    } else {
      $specs = $document->bsonSerialize();
    }

    if (
      isset($specs->height) &&
      isset($specs->width) &&
      isset($specs->label)
    ) {
      $this->itemIdValid = true;
      $this->height = $specs->height;
      $this->width = $specs->width;
      $this->label = $specs->label;
    }

    if ($this->configId) {
      $this->configIdValid = false;
      $this->package = substr($this->configId, 0, 1);
      $this->colorId = substr($this->configId, 1, 3);
      $this->connectionId = substr($this->configId, 4, 2);

      $document = $this->getCollectionDoc(
        MongoDb::COLLECTION_RADIATOR_COLORS,
        ['colorId' => $this->colorId]
      );

      $color = new stdClass();
      if (!$document) {
        $params = new B2BHelper_TERGetAllRadiatorColorsRequest();
        $axColors = $this->B2BroutingService->GetAllRadiatorColors($params)->getResponse()->getRadiatorColorsStruct_TER(
        );

        foreach ($axColors as $axColor) {
          $this->upsertCollectionDoc(
            MongoDb::COLLECTION_RADIATOR_COLORS,
            ['colorId' => strtoupper($axColor->getColorId())],
            [
              'colorName' => $axColor->getColorName(),
              'axColor' => $axColor->jsonSerialize(),
            ]
          );

          if ($this->colorId === $axColor->getColorId()) {
            $color->colorId = strtoupper($axColor->getColorId());
            $color->colorName = $axColor->getColorName();
            $color->axColor = $axColor->jsonSerialize();
          }
        }
      } else {
        $color = $document->bsonSerialize();
      }

      if (
        isset($color->colorName) &&
        isset($color->axColor)
      ) {
        $this->color = $color->colorName;
        $this->axColor = $color->axColor;
        $this->configIdValid = true;
      }
    } else {
      $this->configIdValid = true;
    }
  }

  /**
   * Parse product configuration based on SKU (itemId-configId)
   *
   * @param string $sku
   *   Product SKU (itemId-configId)
   */
  public function parseSKU(string $sku)
  {
    $skuArr = explode('-', $sku);
    $this->parse($skuArr[0], $skuArr[1]);
  }

  public function isValid(): bool
  {
    return $this->itemIdValid && $this->configIdValid;
  }

  public function getGroupId(): string
  {
    return $this->groupId ?: '';
  }

  public function getGroupIdShort(): string
  {
    return $this->groupShort ?: '';
  }

  public function getItemId(): string
  {
    return $this->itemId ?: '';
  }

  public function getConfigId(): string
  {
    return $this->configId ?: '';
  }

  public function getModel(): string
  {
    return $this->model ?: '';
  }

  public function getModelShort(): string
  {
    return $this->modelShort ?: '';
  }

  public function getSize(): string
  {
    return $this->size ?: '';
  }

  public function getPackage(): string
  {
    return $this->package ?: '';
  }

  public function getColorId(): string
  {
    return $this->colorId ?: '';
  }

  public function getConnectionId(): string
  {
    return $this->connectionId ?: '';
  }

  public function getLabel(): string
  {
    return $this->label ?: '';
  }

  public function getHeight(): string
  {
    return $this->height ?: '';
  }

  public function getWidth(): string
  {
    return $this->width ?: '';
  }

  public function getColor(): string
  {
    return $this->color ?: '';
  }

  public function getValues(): array
  {
    return array(
      'groupId' => $this->groupId,
      'groupShort' => $this->groupShort,
      'itemId' => $this->itemId,
      'configId' => $this->configId,

      //AX codes
      'model' => $this->model,
      'modelShort' => $this->modelShort,
      'size' => $this->size,
      'package' => $this->package,
      'colorId' => $this->colorId,
      'connectionId' => $this->connectionId,

      //readable values
      'label' => $this->label,
      'height' => $this->height,
      'width' => $this->width,
      'color' => $this->color,

      //AX objects;
      'axSize' => $this->axSize,
      'axColor' => $this->axColor,

      'itemIdValid' => $this->itemIdValid,
      'configIdValid' => $this->configIdValid,
    );
  }

  /**
   * Method to save document in specific mongoDb collection.
   *
   * @param string $collection
   *   The mongodb collection name.
   * @param mixed $document
   *   Document to record.
   *
   * @return \MongoDB\InsertOneResult
   */
  public function setCollectionDoc(string $collection, $document)
  {
    $dbCollection = $this->mongoDb->selectCollection($collection);

    return $dbCollection->insertOne($document);
  }

  /**
   * Method to remove document from specific mongoDb collection.
   *
   * @param string $collection
   *   The mongodb collection name.
   * @param string $oid
   *   Document identifier.
   *
   * @return \MongoDB\DeleteResult
   */
  protected function removeCollectionDoc(string $collection, string $oid)
  {
    $dbCollection = $this->mongoDb->selectCollection($collection);

    return $dbCollection->deleteOne(['_id' => new ObjectID($oid)]);
  }

  /**
   * Method to find one document in specific collection.
   *
   * @param string $collection
   *   The mongodb collection name.
   * @param array $conditions
   *   Array of searching conditions.
   *
   * @return array|null|object
   */
  protected function getCollectionDoc(string $collection, array $conditions)
  {
    $dbCollection = $this->mongoDb->selectCollection($collection);

    return $dbCollection->findOne($conditions);
  }

  /**
   * Method to update existing document or insert new if conditions not met
   *
   * @param string $collection
   *   The mongodb collection name.
   * @param array $conditions
   *   Array of searching conditions.
   * @param $document
   *   Data to save/update
   *
   * @return \MongoDB\UpdateResult
   */
  protected function upsertCollectionDoc(string $collection, array $conditions, $document)
  {
    $dbCollection = $this->mongoDb->selectCollection($collection);

    return $dbCollection->updateOne(
      $conditions,
      ['$set' => $document],
      ['upsert' => true]
    );
  }
}
